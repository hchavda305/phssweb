
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';


import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import Loader from '../Loader';
import LoaderSecond from '../LoaderSecond';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';
import FileUploadHelper from '../Helpers/FileUploadHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import moment from 'moment';

import Datetime from "react-datetime";

import SelectDropdown from 'react-dropdown-select';

//import Select from 'react-select-me';
import Select from 'react-select';

import FileUploadPreview from '../FileUpload/FileUploadPreviewNotes'
import DocxImg from '../../assets/img/doc/docx.png'
import ExcelImg from '../../assets/img/doc/excel.png'
import PdfImg from '../../assets/img/doc/pdf.png'

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

class BacthPayrollReports extends Component {
  constructor(props) {
    super(props);
    this.state = {

        // Pagination
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : 'StaffNumberId',
        SortType : false,
        IsSortingEnabled : true,
        display_Inactive_flage:"false",
        // Pagination

        locationID:localStorage.getItem("primaryLocationId"),
        staffContactID:localStorage.getItem("contactId"),
        fullName:localStorage.getItem("fullName"),

        role_reports_can : {},
        role_employees_can : {},
        role_hierarchy_can : {},
        role_payroll_adjustment_menu_can : {},

        errormsg :  '',
        isDelete : false,
        ListGrid : [],

        header_data : [],

        // FT PT 
        payPeriodListFilter : [],
        locationListFilter : [],
        
        employmentTypeListFilter :[],
        
        EmployeeNameListFilter : [],

        FilterPayPeriod : '',
        FilterLocation : '',
        
        FilterEmploymentType : '',
        
        FilterEmployee : '',

        ExportFilter : {},
       
        GetCRMUserList:[],
        selectedOptionLocation: null,
        selectedOptionEmployees: null,
        // FT PT
        

        // ============ TimeSheet ============ //
        // Drop down list
        serviceView:[],
        locationViews:[],
        AddPreference:[],
        // Drop down list

        // Add
        AddDate:'',
        AddTimeIn:'',
        AddTimeOut:'',
        Addlocation:'',
        AddService:'',
        AddNotes:'',
        AddNoOfHours:'',
        timeSheetPeriodId:'',
        IsNoOfHoursvalid: false,
        AddTimeOutDisabled : true,
        // Add

        // Edit
        timeSheetTransactionId: '',
        EditService: '',
        Editlocation: '',
        EditTimeIn: '',
        EditTimeOut: '',
        EditNoOfHours: '',
        EditDate: '',
        IsNoOfHoursvalidEdit:true,
        DeletetimesheetTransactionId:'',
        // Edit

        ListGridPastDetails:[],
        role_permission: {},
        // ============ TimeSheet ============ //

        // ============ Timesheet Summary of Entries & Overtime Entitlement and Payroll Adjustment ============ //
        SummaryOfEntriesDisplaytotalRegularHours : '00.00',  
        SummaryOfEntriesDisplaytotalAsleepHours : '00.00',
        SummaryOfEntriesDisplaytotalTripHours : '00.00',
        SummaryOfEntriesDisplaytotalVacationHours : '00.00',

        SummaryOfEntriesDisplaytotalSickHours : '00.00',
        SummaryOfEntriesDisplaytotalCOVID19Hours : '00.00',
        SummaryOfEntriesDisplaytotalBRVHours : '00.00',
        SummaryOfEntriesDisplaytotalLeaveHours : '00.00',
        
        SummaryOfEntriesDisplaytotalOTHours : '00.00',
        SummaryOfEntriesDisplaytotalOTBankedUsedHours : '00.00',
        SummaryOfEntriesDisplaytotalMiscBankedUsedHours : '00.00',

        SummaryOfEntriesDisplaytotalOnCallHours : '00.00',
        SummaryOfEntriesDisplaytotalStatHours : '00.00',
        SummaryOfEntriesDisplaytotalStatDayOffHours : '00.00',
        // ============ Timesheet Summary of Entries & Overtime Entitlement and Payroll Adjustment ============ //

        // ============ Payroll Adjustment ============ //
        header_data_PayRollADJListGrid : [],
        ListGrid_PayRollADJ : [],

        // Drop down list
        PayrollADJAdjustmentCodeList : [],
        PayrollADJLocationList : [],
        PayrollADJEntitlementCategoryList : [],
        PayrollADJEntitlementSubCategoryList : [],
        // Drop down list

        // Add
        AddPayrollADJadjustmentCode: '',
        AddPayrollADJlocation: '',
        AddPayrollADJDate: '',
        AddPayrollADJQtyhours: '',
        AddPayrollADJQtyminutes: '',
        //AddPayrollADJamount: '',
        AddPayrollADJrate: '',
        AddPayrollADJdescription: '',
        AddPayrollADJsubstractFrom: false,
        AddPayrollADJEntCategory: '',
        AddPayrollADJEntSubCategory: '',
        AddPayrollADJEntQtyhours: '',
        AddPayrollADJEntQtyminutes: '',
        AddPayrollADJEntQtyshifts : '',
        AddPayrollADJEntQtydays : '',

        AddPayrollADJEntQtyhoursDivIsShow: true,
        AddPayrollADJEntQtyminutesDivIsShow: true,
        AddPayrollADJEntQtyshiftsDivIsShow: false,
        AddPayrollADJEntQtydaysDivIsShow: false,

        AddPayrollADJsubstractFromIsShow: false,
        AddPayrollADJRateFieldIsShow: false,
        // Add

        // Edit
        EditstaffContactID: '',
        EditPayrollADJId: '',
        EditPayrollADJadjustmentCode: '',
        EditPayrollADJlocation: '',
        EditPayrollADJDate: '',
        EditPayrollADJQtyhours: '',
        EditPayrollADJQtyminutes: '',
        EditPayrollADJrate: '',
        EditPayrollADJdescription: '',
        EditPayrollADJsubstractFrom: false,
        EditPayrollADJEntCategory: '',
        EditPayrollADJEntSubCategory: '',
        EditPayrollADJEntQtyhours: '',
        EditPayrollADJEntQtyminutes: '',

        EditPayrollADJEntQtyshifts : '',
        EditPayrollADJEntQtydays : '',

        EditPayrollADJEntQtyhoursDivIsShow: true,
        EditPayrollADJEntQtyminutesDivIsShow: true,
        EditPayrollADJEntQtyshiftsDivIsShow: false,
        EditPayrollADJEntQtydaysDivIsShow: false,

        EditPayrollADJsubstractFromIsShow: false,
        EditPayrollADJRateFieldIsShow: false,
        // Edit
        // ============ Payroll Adjustment ============ //

        // ============ Notes ============ //
        AddNotesResetflag:false,
        AddMsg:'',
        filePreviewsFinal:[],
        ListNotes:[],
        allow_emp: false,
        isDelete : false,
        EditNoteId: '',
        IsEditBtn: false,

        AddPreference:[],
        EditPreference:[],
        AddPreferenceService:'',
        EditPreferenceService:'',
        filePreviewsFinalEdit:[],
        filePreviewsFinalEditActive:0,
        isPayrollAdminSession: false,
        approverDueDateForAmendmend :'',
        // ============ Notes ============ //
        UserHoursType : '',


        IsAddInDateHoliday: false,
        IsAddOutDateHoliday: false,
        IsEditInDateHoliday: false,
        IsEditOutDateHoliday: false,
        BatchesList:[],
        BatchRecord:''

        /* ************************ Pk Dev Timesheet ************************* */
    };

    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
    this.onChangeSearch = this.onChangeSearch.bind(this);

    this.handleAddPayrollADJDate = this.handleAddPayrollADJDate.bind(this);
    this.handleEditPayrollADJDate = this.handleEditPayrollADJDate.bind(this);

    this.handleDateAddIn = this.handleDateAddIn.bind(this);
    this.handleDateAddOut = this.handleDateAddOut.bind(this);
    this.handleDateEditIn = this.handleDateEditIn.bind(this);
    this.handleDateEditOut = this.handleDateEditOut.bind(this);

    this.handleKeypress = this.handleKeypress.bind(this);
  }

  onChangeSearch(value) {
    console.log(value);
    this.setState({ value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }

  hideLoader2 = () => {
    this.setState({ loading2: false });
  }
  showLoader2 = () => {
    this.setState({ loading2: true });
  }
  // Loader hide show method

  // Handle Payroll Adjustment
  handleAddPayrollADJDate = (date) =>{
    console.log('AddPayrollADJDate => '+ date);
    this.setState({ AddPayrollADJDate : date });
  };
  handleEditPayrollADJDate = (date) =>{
    console.log('EditPayrollADJDate => '+ date);
    this.setState({ EditPayrollADJDate : date });
  };

  // Hours Day and Shift Only Number in Add Payroll Adjustment
  handleKeypress (e) {
    const characterCode = e.key
    if (characterCode === 'Backspace') return

    const characterNumber = Number(characterCode)
    if (characterNumber >= 0 && characterNumber <= 9) {
      if (e.currentTarget.value && e.currentTarget.value.length) {
        return
      } else if (characterNumber === 0) {
        e.preventDefault()
      }
    } else {
      e.preventDefault()
    }
  }
  // Hours Day and Shift Only Number in Add Payroll Adjustment
  // Handle Payroll Adjustment

  // Handle TimeSheet
  handleChangeSelectLocation = (selectedOptionLocation) => {
    this.setState({ selectedOptionLocation });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ FilterLocation: selectedOptionLocation.value });
    //LocationFilterID
  };

  handleChangeSelectEmployees = (selectedOptionEmployees) => {
    this.setState({ selectedOptionEmployees });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ FilterEmployee: selectedOptionEmployees.value });
    //LocationFilterID
  };

  handleDateAddIn = (date) =>{
    this.setState({ AddTimeOut_Min: moment(date).subtract(1, 'day')});
    this.setState({ AddTimeOut_Max: moment(date).add(2, 'day')});
    this.setState({ AddTimeIn: date});
    this.setState({ AddTimeOutDisabled: false});
    $('.readonly-cls-AddTimeOut').css("background-color", "#fff");

    var AddTimeOut = this.state.AddTimeOut;

    if(this.state.AddService == process.env.API_24HOURS_SERVICE ){
       this.setState({ AddTimeOutDisabled: true});
      
      var AddTimeOut = moment(moment(date).add(24, 'hours'));
    }

    if(this.state.AddService != process.env.API_24HOURS_SERVICE ){
      this.setState({ AddTimeOutDisabled: false});
    }
    this.setState({ AddTimeOut: AddTimeOut});

    if(date !='' && AddTimeOut != ''){
      var Get_Hour = this.Get_Hour(date,AddTimeOut);
      this.setState({ AddNoOfHours: Get_Hour });
    }
  };

  handleDateAddOut = (date) =>{
    this.setState({ AddTimeOut: date});

    var StarTime = this.state.AddTimeIn;
    var EndTime = date;

    if(StarTime != '' && EndTime !=''){
      var Get_Hour = this.Get_Hour(StarTime,EndTime);
      this.setState({ AddNoOfHours: Get_Hour });
    }
  };

  handleDateEditIn = (date) =>{
    this.setState({ EditTimeOut_Min: moment(date).subtract(1, 'day')});
    this.setState({ EditTimeOut_Max: moment(date).add(2, 'day')});
    this.setState({ EditTimeIn: date});

    var EditTimeOut = this.state.EditTimeOut;
    if(this.state.EditService == process.env.API_24HOURS_SERVICE ){
      var EditTimeOut = moment(moment(date).add(24, 'hours'));
    }
    
    this.setState({ EditTimeOut: EditTimeOut});
    

    if(date !='' && EditTimeOut != ''){
      var Get_Hour = this.Get_Hour_Edit(date,EditTimeOut);
      this.setState({ EditNoOfHours: Get_Hour });
    }
  };

  handleDateEditOut = (date) =>{
    this.setState({ EditTimeOut: date});

    var StarTime = this.state.EditTimeIn;
    var EndTime = date;
    
    if(StarTime != '' && EndTime !=''){
      var Get_Hour = this.Get_Hour_Edit(StarTime,EndTime);
      this.setState({ EditNoOfHours: Get_Hour });
    }
  };

  validationDateAddIn = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    if(this.state.AddService == process.env.API_ON_CALL){
      return currentDate.isBefore(moment(this.state.AddTimeIn_Max).add(7, 'day')) && currentDate.isAfter(moment(this.state.AddTimeIn_Min).subtract(7, 'day'));
    }else{
      return currentDate.isBefore(moment(this.state.AddTimeIn_Max)) && currentDate.isAfter(moment(this.state.AddTimeIn_Min).subtract(1, 'day'));
    }
    
  };

  validationDateAddOut = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    if(this.state.AddService == process.env.API_ON_CALL || this.state.AddService == process.env.API_ZERO_HOURS){
      if(this.state.AddService == process.env.API_ON_CALL){
        return currentDate.isBefore(moment(this.state.AddTempTimeOut_Max).add(7, 'day')) && currentDate.isAfter(moment(this.state.AddTimeOut_Min));
      }else{
        return currentDate.isBefore(moment(this.state.AddTempTimeOut_Max)) && currentDate.isAfter(moment(this.state.AddTimeOut_Min));
      }
    }else{
      return currentDate.isBefore(moment(this.state.AddTimeOut_Max)) && currentDate.isAfter(moment(this.state.AddTimeOut_Min));
    }
  };

  AddTimeOutonClose = (date) =>{
    /* Payout Preference Start */

    if(this.state.AddService !="" && this.state.Addlocation !="" && this.state.AddTimeIn !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(this.state.AddService,this.state.AddTimeIn,service,this.state.Addlocation,'Add','In');
      this.GetPayOffDetailsForUser(this.state.AddService,date,service,this.state.Addlocation,'Add','Out');

    }

    /* Payout Preference End */
  }

  AddTimeInonClose = (date) =>{
    /* Payout Preference Start */

    if(this.state.AddService !="" && this.state.Addlocation !="" && this.state.AddTimeOut !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(this.state.AddService,date,service,this.state.Addlocation,'Add','In');
      this.GetPayOffDetailsForUser(this.state.AddService,this.state.AddTimeOut,service,this.state.Addlocation,'Add','Out');
    }

    /* Payout Preference End */
  }

  EditTimeOutonClose = (date) =>{
    /* Payout Preference Start */
    console.log("EditTimeOutonClose");
    this.setState({ EditPreferenceService: "" });
    if(this.state.EditService !="" && this.state.Editlocation !="" && this.state.EditTimeIn !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(this.state.EditService,this.state.EditTimeIn,service,this.state.Editlocation,'Edit','In');
      this.GetPayOffDetailsForUser(this.state.EditService,this.state.EditTimeOut,service,this.state.Editlocation,'Edit','Out');
      
    }

    /* Payout Preference End */
  }

  EditTimeInonClose = (date) =>{
    /* Payout Preference Start */

    console.log("EditTimeInonClose date");
    this.setState({ EditPreferenceService: "" });
    //console.log(date);
    if(this.state.EditService !="" && this.state.Editlocation !="" && this.state.EditTimeOut !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(this.state.EditService,this.state.EditTimeIn,service,this.state.Editlocation,'Edit','In');
      this.GetPayOffDetailsForUser(this.state.EditService,this.state.EditTimeOut,service,this.state.Editlocation,'Edit','Out');
      
    }

    /* Payout Preference End */
  }

  validationDateEditIn = (currentDate) => {
    return currentDate.isBefore(moment(this.state.EditTimeIn_Max)) && currentDate.isAfter(moment(this.state.EditTimeIn_Min));
  };

  validationDateEditOut = (currentDate) => {
    if(this.state.EditService == process.env.API_ON_CALL || this.state.EditService == process.env.API_ZERO_HOURS){
      return currentDate.isBefore(moment(this.state.EditTempTimeOut_Max)) && currentDate.isAfter(moment(this.state.EditTimeOut_Min));
    }else{
      return currentDate.isBefore(moment(this.state.EditTimeOut_Max)) && currentDate.isAfter(moment(this.state.EditTimeOut_Min));
    }
  };
  // Handle TimeSheet
  

  handleChange = input => e => {

    if( [input] != 'display_Inactive'){
      this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    }
    
    //console.log('handleChange');
    //console.log(input);

    if (this.state[input] != '') {
      delete this.state.errormsg[input];
    }

    // Pagination
    if([input]=="pageSize")
    {
      this.setState({ current_page: 1 });
      this.GetReportListGrid(1,e.target.value,this.state.searchText)
    }
    // Pagination

    // Active - Inactive employee Checkbox Display
    if( [input] == 'display_Inactive'){
      //console.log("**************************************** Start");
      //console.log([input]);
      //console.log(this.state.display_Inactive_flage);
      //console.log("**************************************** End");
      if(this.state.display_Inactive_flage == "true"){
        this.setState({ display_Inactive_flage: "false" });
      }else{
        this.setState({ display_Inactive_flage: "true" });
      }
    }
    // Active - Inactive employee Checkbox Display

    // ========================= Add time Payroll Adjustment ========================= //
    /*if([input]=="AddPayrollADJEntCategory")
    {
      this.setState({ PayrollADJEntitlementSubCategoryList: [] });
      this.GetEntitlementSubCategoryView(e.target.value);
      this.setState({ AddPayrollADJEntSubCategory: '' });
    }*/
    if([input]=="AddPayrollADJQtyhours")
    {
      this.setState({ AddPayrollADJEntQtyhours: e.target.value });
    }
    if([input]=="AddPayrollADJQtyminutes")
    {
      this.setState({ AddPayrollADJEntQtyminutes: e.target.value });
    }

    if([input] == "AddPayrollADJsubstractFrom")
    {
      if(this.state.AddPayrollADJsubstractFrom == false){
        this.setState({ AddPayrollADJsubstractFrom: true });
      }else{
        this.setState({ AddPayrollADJsubstractFrom: false });
      }
    }
    

    // Subtract From show or Hide
    if([input] == "AddPayrollADJadjustmentCode")
    {
      var substractFrom = event.target[event.target.selectedIndex].getAttribute('data-addpayrolladjsubstractfromishow');
      //console.log("123= "+substractFrom + "="+e.target.value);
      this.setState({ AddPayrollADJsubstractFromIsShow: substractFrom });

      this.setState({ PayrollADJEntitlementSubCategoryList: [] });
      this.setState({ AddPayrollADJEntSubCategory: '' });

      this.setState({ PayrollADJEntitlementCategoryList: [] });
      this.setState({ AddPayrollADJEntCategory: '' });

      this.setState({ AddentitlementQtyhours: '' });
      this.setState({ AddentitlementQtyminutes: '' });
      
      this.setState({ AddentitlementQtydays: '' });
      this.setState({ AddentitlementQtyshifts: '' });

      if(e.target.value==process.env.API_PAYROLL_ADJUSTMENTS_STAT_HOLIDAY_GUID){
        //console.log("1111")
        this.setState({ AddPayrollADJRateFieldIsShow: true });
        this.setState({ AddPayrollADJrate: '' });
      }else{
        //console.log("2222")
        this.setState({ AddPayrollADJRateFieldIsShow: false });
        this.setState({ AddPayrollADJrate: '' });
      }
      
      this.setState({ AddPayrollADJsubstractFrom: false });

      this.GetPayrollAdjustmentEntitlementReason(e.target.value);
    }
    // Subtract From show or Hide
    
    // Adjustment Code ("Sick Time" (Shift) And User (Part-Time)) and ("Sick Time COVID-19" (days)) Hide or Show 
    if([input] == "AddPayrollADJEntSubCategory")
    {

      this.setState({ PayrollADJEntitlementCategoryList: [] });
      this.setState({ AddPayrollADJEntCategory: '' });
      this.GetPayrollAdjustmentEntitlementCategory(e.target.value);
      

      if(e.target.value == process.env.API_ENTITLEMENT_IDEL_COVID19)
      {
        //alert("1");
        this.setState({ AddPayrollADJEntQtydaysDivIsShow: true });

        this.setState({ AddPayrollADJEntQtyshiftsDivIsShow: false });
        this.setState({ AddPayrollADJEntQtyhoursDivIsShow: false });
        this.setState({ AddPayrollADJEntQtyminutesDivIsShow: false });
        
        
        this.setState({ AddentitlementQtyhours: '' });
        this.setState({ AddentitlementQtyminutes: '' });
        
        this.setState({ AddentitlementQtydays: '' });
        this.setState({ AddentitlementQtyshifts: '' });
      }
      else if(e.target.value== process.env.API_ENTITLEMENT_REASON_SICK_TIME_GUID && this.state.UserHoursType == "Part-Time")
      {
        //alert("2");
        this.setState({ AddPayrollADJEntQtyshiftsDivIsShow: true });

        this.setState({ AddPayrollADJEntQtydaysDivIsShow: false });
        this.setState({ AddPayrollADJEntQtyhoursDivIsShow: false });
        this.setState({ AddPayrollADJEntQtyminutesDivIsShow: false });

        this.setState({ AddentitlementQtyhours: '' });
        this.setState({ AddentitlementQtyminutes: '' });
        
        this.setState({ AddentitlementQtydays: '' });
        this.setState({ AddentitlementQtyshifts: '' });
      }
      else if(e.target.value != process.env.API_ENTITLEMENT_IDEL_COVID19)
      {
        //alert("3");
        this.setState({ AddPayrollADJEntQtyhoursDivIsShow: true });
        this.setState({ AddPayrollADJEntQtyminutesDivIsShow: true });

        this.setState({ AddPayrollADJEntQtydaysDivIsShow: false });
        this.setState({ AddPayrollADJEntQtyshiftsDivIsShow: false });
        
        this.setState({ AddentitlementQtyhours: '' });
        this.setState({ AddentitlementQtydays: '' });
        this.setState({ AddentitlementQtyshifts: '' });
      }
    }
    // Adjustment Code ("Sick Time" (Shift) And User (Part-Time)) and ("Sick Time COVID-19" (days)) Hide or Show 
    // ========================= Add time Payroll Adjustment ========================= //

    // ========================= Edit time Payroll Adjustment ========================= //
    if([input]=="EditPayrollADJEntSubCategory")
    {
      //this.setState({ PayrollADJEntitlementSubCategoryList: [] });
      //this.GetEntitlementSubCategoryView(e.target.value);
      //this.setState({ EditPayrollADJEntSubCategory: '' });

    }


    if([input]=="EditPayrollADJsubstractFrom")
    {
      if(this.state.EditPayrollADJsubstractFrom == false){
        
        this.setState({ EditPayrollADJsubstractFrom: true });
      }else{
        
        this.setState({ EditPayrollADJsubstractFrom: false });
      }
    }

    
    // ========================= Edit time Payroll Adjustment ========================= //

    // Time Sheet //
    /* ****************************************** Pk DEV ********************************** */
    if(input !="allow_emp" && input !="AddPayrollADJsubstractFrom" && input !="EditPayrollADJsubstractFrom"){
      this.setState({ [input]: e.target.value });
    }
    
    console.log(input);
    console.log(e.target);

    if(input=="allow_emp")
    {
      if(this.state.allow_emp == true){
        this.setState({ allow_emp: false });
      }else{
       this.setState({ allow_emp: true }); 
      }
    }

    if (e.target.value != '') {
        delete this.state.errormsg[input];
    }

    

    if([input]=="FilterEmploymentType" && e.target.value != ''){
      this.GetPhssBatches(e.target.value);
    }

    /* Payout Preference Start */

    if([input]=="AddService" && e.target.value !='' && e.target.value == process.env.API_ON_CALL){
      this.setState({ AddTimeOut : '' });
      this.setState({ AddPreferenceService : '' });
      this.setState({ AddPreference: [] });
      this.setState({ AddNoOfHours : '' });
      return false;
    }

    if([input]=="AddService" && e.target.value !='' && this.state.AddService ==  process.env.API_ON_CALL && e.target.value != process.env.API_ON_CALL){
        this.setState({ AddTimeIn : '' });
        this.setState({ AddTimeOut : '' });
        this.setState({ AddPreferenceService : '' });
        this.setState({ AddPreference: [] });
        this.setState({ AddNoOfHours : '' });
        $('.readonly-cls-AddTimeOut').css("background-color", "lightgray");
        return false;
    }

    if(([input]=="AddService" && e.target.value !='') &&  this.state.Addlocation !="" && this.state.AddTimeIn !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      this.GetPayOffDetailsForUser(e.target.value,this.state.AddTimeIn,service,this.state.Addlocation,'Add','In');
      this.GetPayOffDetailsForUser(e.target.value,this.state.AddTimeOut,service,this.state.Addlocation,'Add','Out');
    }

    if(([input]=="Addlocation" && e.target.value !='') &&  this.state.AddService !="" && this.state.AddTimeIn !=""){
      var service = $("#AddService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(this.state.AddService,this.state.AddTimeInt,service,e.target.value,'Add','In');
      this.GetPayOffDetailsForUser(this.state.AddService,this.state.AddTimeOut,service,e.target.value,'Add','Out');
      
    }

    if(([input]=="EditService" && e.target.value !='') &&  this.state.Editlocation !="" && this.state.EditTimeIn !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(e.target.value,this.state.EditTimeIn,service,this.state.Editlocation,'Edit','In');
      this.GetPayOffDetailsForUser(e.target.value,this.state.EditTimeOut,service,this.state.Editlocation,'Edit','Out');
      
    }

    if([input]=="AddService" && this.state.AddTimeIn !='' && this.state.AddTimeOut != ''){
        var Get_Hour = this.Get_Hour(this.state.AddTimeIn,this.state.AddTimeOut);
        this.setState({ AddNoOfHours: Get_Hour });
    }

    if([input]=="EditService" && this.state.EditTimeIn !='' && this.state.EditTimeOut != ''){
        var Get_Hour = this.Get_Hour_Edit(this.state.EditTimeIn,this.state.EditTimeOut);
        this.setState({ EditNoOfHours: Get_Hour });
    }

    if(([input]=="Editlocation" && e.target.value !='') &&  this.state.EditService !="" && this.state.EditTimeIn !=""){
      var service = $("#EditService").find(':selected').data('basserivce');
      
      this.GetPayOffDetailsForUser(this.state.EditService,this.state.EditTimeIn,service,e.target.value,'Edit','In');
      this.GetPayOffDetailsForUser(this.state.EditService,this.state.EditTimeOut,service,e.target.value,'Edit','Out');
      
    }

    /* Payout Preference End */

    /* Filter Search */
    if([input]=="LocationFilterID" && e.target.value != ''){
      this.setState({ employeeFilterID: ''});
      this.setState({ roleFilterID: ''});

      $("#employeeFilterID").prop( "disabled", true );
      $("#roleFilterID").prop( "disabled", true );
    }

    if(([input]=="employeeFilterID" && e.target.value != '') || ([input]=="roleFilterID" && e.target.value != '') ){
      this.setState({ LocationFilterID: ''});
      $("#LocationFilterID").prop( "disabled", true );
    }

    if([input]=="LocationFilterID" && e.target.value == ''){
      this.setState({ employeeFilterID: ''});
      this.setState({ roleFilterID: ''});

      $("#employeeFilterID").prop( "disabled", false );
      $("#roleFilterID").prop( "disabled", false );
    }

    if(([input]=="employeeFilterID" && e.target.value == '') && (this.state.roleFilterID == '') ){
      this.setState({ LocationFilterID: ''});
      $("#LocationFilterID").prop( "disabled", false );
    }

    if(([input]=="roleFilterID" && e.target.value == '') && (this.state.employeeFilterID == '') ){
      this.setState({ LocationFilterID: ''});
      $("#LocationFilterID").prop( "disabled", false );
    }



    /* Filter Search */

    if([input]=="AddService" && this.state.AddTimeIn !='' && (e.target.value == process.env.API_24HOURS_SERVICE || e.target.value == process.env.API_ZERO_HOURS)){
      this.setState({ AddTimeOutDisabled: true});
      //var AddTimeOut = moment(moment(this.state.AddTimeIn).add(24, 'hours'));
      if(e.target.value == process.env.API_24HOURS_SERVICE ){
        var AddTimeOut = moment(moment(this.state.AddTimeIn).add(24, 'hours'));
      }else if(e.target.value == process.env.API_ZERO_HOURS){
        var AddTimeOut = moment(moment(this.state.AddTimeIn).add(0, 'hours'));
      }
      
      this.setState({ AddTimeOut: AddTimeOut});
      
      var StarTime = this.state.AddTimeIn;
      var EndTime = AddTimeOut;

      if(StarTime != '' && EndTime !=''){
        var Get_Hour = this.Get_Hour_Service(StarTime,EndTime,e.target.value);
        this.setState({ AddNoOfHours: Get_Hour });
      }  
    }

    if([input]=="AddService" && e.target.value != process.env.API_24HOURS_SERVICE ){
      this.setState({ AddTimeOutDisabled: false});
    }


    if([input]=="EditService" && (e.target.value == process.env.API_24HOURS_SERVICE || e.target.value == process.env.API_ZERO_HOURS)  && this.state.EditTimeIn !=''){
      this.setState({ EditTimeOutDisabled: true});
      $("#EditTimeOut").prop( "disabled", true );

      //var NewDate = moment(moment(this.state.EditTimeIn).add(24, 'hours'));
      if(e.target.value == process.env.API_24HOURS_SERVICE ){
        var NewDate = moment(moment(this.state.EditTimeIn).add(24, 'hours'));
      }else if(e.target.value == process.env.API_ZERO_HOURS){
        var NewDate = moment(moment(this.state.EditTimeIn).add(0, 'hours'));
      }
      
      this.setState({ EditTimeOut: NewDate});
      var StarTime = this.state.EditTimeIn;
      var EndTime = NewDate;

      if(StarTime != '' && EndTime !=''){
        var Get_Hour = this.Get_Hour_Edit_Service(StarTime,EndTime,e.target.value);
        this.setState({ EditNoOfHours: Get_Hour });
      }  
    }else if([input]=="EditService" && e.target.value != process.env.API_24HOURS_SERVICE ){
      $("#EditTimeOut").prop( "disabled", false );
      this.setState({ EditTimeOutDisabled: false});
    }

    /* ****************************************** Pk DEV ********************************** */
    // Time Sheet //


    // ========== Filter conditions ========== //
    // 1) When pay period select then startdate and enddate disabled
    // 1) When pay period select then startdate and enddate disabled
    // 2) When start date select then pay period disabled
    // 2) When start date select then pay period disabled
    // ========== Filter conditions ========== //
  }

  componentDidMount() {
    console.log("Reports");

    /* Role Management */
    console.log('Role Store reports_can');
    var getrole = SystemHelpers.GetRole();
    let report_can = getrole.report_can;
    this.setState({ role_reports_can: report_can });
    console.log(report_can);

    console.log('Role Store hierarchy_can');
    var getrole_hierarchy = SystemHelpers.GetRole();
    let hierarchy_can = getrole_hierarchy.hierarchy_can;
    this.setState({ role_hierarchy_can: hierarchy_can });
    console.log(hierarchy_can);

    console.log('Role Store payroll_adjustment_menu_can');
    var getrole = SystemHelpers.GetRole();
    let payroll_adjustment_menu_can = getrole.payroll_adjustment_menu_can;
    this.setState({ role_payroll_adjustment_menu_can: payroll_adjustment_menu_can });
    console.log(payroll_adjustment_menu_can);

    /* Role Management */
    console.log(localStorage.getItem("token"));
    
    this.GetTimeSheetReportView();

    //this.GetPayrollAdjustmentCodeMaster();
    //this.GetEntitlementCategoryView();

    // Table header
    // Table header

    // PayRoll Adjustmenr
    this.setState({ AddPayrollADJEntQtydaysDivIsShow: false });
    this.setState({ AddPayrollADJEntQtyshiftsDivIsShow: false });
    // PayRoll Adjustmenr

    $('.readonly-cls-AddTimeOut').css("background-color", "lightgray");

  }

  // ============================================== FT PT wise Get Batches Drop down API ============================================== //
  GetPhssBatches(type){

    this.showLoader();
    this.setState({ BatchesList: []});
    var url=process.env.API_API_URL+'GetPhssBatches?employeementType='+type;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetPhssBatches");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        this.setState({ BatchesList: data.data});

        
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserWiseTimeSheetData error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }
  // ============================================== FT PT wise Get Batches Drop down API ============================================== //

  // ============================================== FT PT report Main grid & TimeSheet Pay Period Drop down API ============================================== //
  GetTimeSheetReportView(){

    /* Role Management */
      var getrole = SystemHelpers.GetRole();
      console.log('Location Get role');
      console.log(getrole.locations_can);
      //let canDelete = getrole.locations_can.locations_can_delete;
      //let locationscanViewall = getrole.locations_can.locations_can_viewall;
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */
    this.showLoader();
    var url=process.env.API_API_URL+'GetTimeSheetReportView?loggedInUserId='+this.state.staffContactID+'&rolePriorityId='+hierarchyId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //console.log("responseJson Report GetTimeSheetReportView");
      //console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        this.setState({ payPeriodListFilter: data.data.timeSheetPeriodViews});

        if(this.state.role_hierarchy_can.hierarchy_can_id == "1")
        {
          $('#FilterLocation').prop('disabled', true);

          $('#FilterEmployee').prop('disabled', true);
          this.setState({ FilterEmployee: this.state.staffContactID });

          $('#FilterEmploymentType').prop('disabled', true);
          this.setState({ FilterEmploymentType: localStorage.getItem("employmentHoursName") });
          
          
        }
        
        
        
        this.setState({ employmentTypeListFilter: data.data.employeementTypes });
        
        /* ************************************************************ */
        let locationViews_push = [];
        var locationViews = data.data.locationViews;

        let locationtemp = {};
        locationtemp["value"] = "";
        //locationtemp["label"] = "All";
        locationtemp["label"] = "-";
        locationViews_push.push(locationtemp);

        for (var zz = 0; zz < locationViews.length; zz++) {
            let locationtemp = {};
            locationtemp["value"] = locationViews[zz].locationId;
            locationtemp["label"] = locationViews[zz].locationName;
            locationViews_push.push(locationtemp);
        }

        this.setState({ LocationListFilter: locationViews_push });
        //this.setState({ locationListFilter: data.data.locationViews });
        /* ************************************************************ */

        /* ************************************************************ */
        let employeeViews_push = [];
        var employeeViews = data.data.employees;

        let employeetemp = {};
        employeetemp["value"] = "";
        //employeetemp["label"] = "All";
        employeetemp["label"] = "-";
        employeeViews_push.push(employeetemp);

        for (var zz = 0; zz < employeeViews.length; zz++) {
            let employeetemp = {};
            employeetemp["value"] = employeeViews[zz].contactId;
            employeetemp["label"] = employeeViews[zz].employeeName;
            employeeViews_push.push(employeetemp);
        }

        this.setState({ EmployeeNameListFilter: employeeViews_push });
        //this.setState({ EmployeeNameListFilter: data.data.employees });
        /* ************************************************************ */
        this.setState({ locationViews: data.data.locationViews });
        this.setState({ serviceView: data.data.serviceViews });
        
        this.Default_TimeSheet(data.data.timeSheetPeriodViews);
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserWiseTimeSheetData error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }
  
  Default_TimeSheet(timeSheetPeriod){
      var length = timeSheetPeriod.length;
      
      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {
          if(timeSheetPeriod[zz].isCurrentTimeSheet == true){
            this.setState({ FilterPayPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
          }
          i++;
        }
      }
  }
  // ============================================== FT PT report Main grid & TimeSheet Pay Period Drop down API ============================================== //

  // ============================================== FT PT report Main grid API ============================================== //
  TableHeaderDesign(){
      // PT false, FT true
    
      if(this.state.FilterEmploymentType == "true")
      {
        let columns = [];
        columns.push(<tr>
            <th>ID Employee</th>
            <th>Name Employee</th>
            <th>ID Location</th>
            <th>Name Location</th>
            <th>Reg Hours</th>
            <th>Sleep Hours</th>
            <th>FT OT Shifts</th>
            <th>Straight time</th>
            <th>Overtime</th>
            <th>TRIP</th>
            <th>FTV</th>
            <th>Misc. Bank Used</th>
            <th>BTT</th>
            <th>SCK</th>
            <th>COVID-19</th>
            <th>ESP</th>
            <th>UNP</th>
            <th>BRV</th>
            <th>On-Call</th>
            <th>On-Call STAT</th>
            <th>On Call -Bank</th>
            <th>On Call - Payout</th>
            <th>On Call - RRSP</th>
            <th>STAT Day off</th>
            <th>STAT 2.5</th>
            <th>STAT 1.5</th>
            <th>STAT 1.0</th>
            <th>STAT - Bank Hours</th>
            <th>STAT - Payout</th>
            <th>STAT - RRSP</th>
            <th>Rec Type</th>
            <th>Action</th>
          </tr>
        );
          var columns1 = [
            {
              label: 'ID Employee',
              field: 'idEmployee',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Name Employee',
              field: 'nameEmployee',
              sort: 'asc',
              width: 150
            },
            {
              label: 'ID Location',
              field: 'idLocation',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Name Location',
              field: 'nameLocation',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Reg Hours',
              field: 'regHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Sleep Hours',
              field: 'sleepHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'FT OT Shifts',
              field: 'FTOTShifts',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Straight time',
              field: 'straightTime',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Overtime',
              field: 'overtime',
              sort: 'asc',
              width: 150
            },
            {
              label: 'TRIP',
              field: 'trip',
              sort: 'asc',
              width: 150
            },
            {
              label: 'FTV',
              field: 'ftv',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Misc. Bank Used',
              field: 'miscBankUsed',
              sort: 'asc',
              width: 150
            },
            {
              label: 'BTT',
              field: 'btt',
              sort: 'asc',
              width: 150
            },
            {
              label: 'SCK',
              field: 'sck',
              sort: 'asc',
              width: 150
            },
            {
              label: 'COVID-19',
              field: 'covid19',
              sort: 'asc',
              width: 150
            },
            {
              label: 'ESP',
              field: 'esp',
              sort: 'asc',
              width: 150
            },
            {
              label: 'UNP',
              field: 'unp',
              sort: 'asc',
              width: 150
            },
            {
              label: 'BRV',
              field: 'brv',
              sort: 'asc',
              width: 150
            },
            {
              label: 'On-Call',
              field: 'onCall',
              sort: 'asc',
              width: 150
            },
            /*{
              label: 'On-Call Pay Pref.',
              field: 'onCallPayPref',
              sort: 'asc',
              width: 150
            },*/
            {
              label: 'On-Call STAT',
              field: 'onCallstat',
              sort: 'asc',
              width: 150
            },
            {
              label: 'On Call -Bank',
              field: 'onCallBankHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'On Call - Payout',
              field: 'onCallPayout',
              sort: 'asc',
              width: 150
            },
            {
              label: 'On Call - RRSP',
              field: 'onCallRRSP',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT Day off',
              field: 'statDayoff',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT 2.5',
              field: 'stat25',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT 1.5',
              field: 'stat15',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT 1.0',
              field: 'stat10',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - Bank Hours',
              field: 'statBankHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - Payout',
              field: 'statPayout',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - RRSP',
              field: 'statRRSP',
              sort: 'asc',
              width: 150
            },
            /*{
              label: 'STAT Pay Pref.',
              field: 'statPayPref',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Other Earnings',
              field: 'otherEarnings',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Other Earnings Value',
              field: 'otherEarningsValue',
              sort: 'asc',
              width: 150
            },*/
            {
              label: 'Rec Type',
              field: 'recType',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Action',
              field: 'action',
              width: 150
            }
          ];

        //this.setState({ header_data: columns });
        return columns;
      }
      else
      {
        let columns = [];
        columns.push(<tr>
            <th>ID Employee</th>
            <th>Name Employee</th>
            <th>ID Location</th>
            <th>Name Location</th>
            <th>Reg Hours</th>
            <th>Sleep Hours</th>
            <th>CRIT ILL PAY</th>
            <th>SCK</th>
            <th>PPE Date</th>
            <th>BRV</th>
            <th>FTV</th>
            <th>Misc. Bank</th>
            <th>PT-OT</th>
            <th>OT HOURS</th>
            <th>COVID-19</th>
            <th>STAT 2.5</th>
            <th>Public holiday pay</th>
            <th>Rec Type</th>
            <th>Total</th>
            <th>Action</th>
          </tr>
        );
          var columns_2 = [
            {
              label: 'ID Employee',
              field: 'idEmployee',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Name Employee',
              field: 'nameEmployee',
              sort: 'asc',
              width: 150
            },
            {
              label: 'ID Location',
              field: 'idLocation',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Name Location',
              field: 'nameLocation',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Reg Hours',
              field: 'regHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Sleep Hours',
              field: 'sleepHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'CRIT ILL PAY',
              field: 'CritIllPay',
              sort: 'asc',
              width: 150
            },
            {
              label: 'SCK',
              field: 'sck',
              sort: 'asc',
              width: 150
            },
            {
              label: 'PPE Date',
              field: 'PPTDate',
              sort: 'asc',
              width: 150
            },
            {
              label: 'BRV',
              field: 'brv',
              sort: 'asc',
              width: 150
            },
            {
              label: 'FTV',
              field: 'ftv',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Misc. Bank',
              field: 'miscBank',
              sort: 'asc',
              width: 150
            },
            {
              label: 'PT-OT',
              field: 'PTOT',
              sort: 'asc',
              width: 150
            },
            {
              label: 'OT HOURS',
              field: 'OTHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'COVID-19',
              field: 'covid19',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT 2.5',
              field: 'stat25',
              sort: 'asc',
              width: 150
            },
            /*{
              label: 'STAT - RRSP',
              field: 'statRRSP',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - Bank Hours',
              field: 'statBankHours',
              sort: 'asc',
              width: 150
            },
            {
              label: 'STAT - Payout',
              field: 'statPayout',
              sort: 'asc',
              width: 150
            },*/
            {
              label: 'Public holiday pay',
              field: 'publicHoliday',
              sort: 'asc',
              width: 150
            },
            /*{
              label: 'Other Earnings',
              field: 'otherEarnings',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Other Earnings Value',
              field: 'otherEarningsValue',
              sort: 'asc',
              width: 150
            },*/
            {
              label: 'Rec Type',
              field: 'recType',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Total',
              field: 'total',
              sort: 'asc',
              width: 150
            },
            {
              label: 'Action',
              field: 'action',
              width: 150
            }
            
            
          ];
        //this.setState({ header_data: columns }); 
        return columns; 
      }
    
  }

  GetReportListGrid_1 = () => e => {
    
      e.preventDefault();

      this.setState({ ListGrid : [] });
      this.setState({ ExportFilter: {} });
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */


      let step1Errors = {};
    
      if (this.state["FilterEmploymentType"] =='') {
        step1Errors["FilterEmploymentType"] = "Please select Report Type.";
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      var paraPayPeriod = 'periodProfileId='+this.state.FilterPayPeriod;
      var paraLocation = '&locationId='+this.state.FilterLocation;
      var paraEmploymentType = '&employeementType='+this.state.FilterEmploymentType;
      var paraContactId = '&contactId='+this.state.FilterEmployee;
      var paraneedToExport = '&needToExport=false';
      var paraDisplayOnlyActive = '&displayOnlyActive='+this.state.display_Inactive_flage;
      
      //ExportFilter
      let ExportFilterArray = {
        FilterPayPeriod: this.state.FilterPayPeriod,
        FilterLocation: this.state.FilterLocation,
        FilterEmploymentType: this.state.FilterEmploymentType,
        FilterEmployee: this.state.FilterEmployee,
        FilterDisplayOnlyActive: this.state.display_Inactive_flage,
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      if(this.state.FilterEmploymentType == "false"){
        //var ApiName= 'GetPTBatchPayrollReport?'
        var ApiName= 'GetPTBatchPayrollReportJson?'
      }else{
        //var ApiName= 'GetFTBatchPayrollReport?'
        var ApiName= 'GetFTBatchPayrollReportJson?'
      }

      var pass_url = ApiName+paraPayPeriod+paraLocation+paraEmploymentType+paraContactId+paraneedToExport+paraDisplayOnlyActive;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetPTBatchPayrollReport ");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                //console.log("TableHeaderDesign");
                //console.log(this.TableHeaderDesign());
                this.setState({ header_data: this.TableHeaderDesign() });
                this.setState({ ListGrid: this.rowData(data.data) });
              }
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        console.log('GetReportListGrid error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  GetReportListGrid(currentPage,pageSize,searchText){
      
      /*console.log("GetReportListGrid");
      console.log(this.state.FilterLocation);
      console.log(this.state.FilterLocation);
      return false;*/

      this.setState({ ListGrid : [] });
      this.setState({ ExportFilter: {} });
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */

      // Pagination
      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });

      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      
      var IsSortingEnabled = true;

      var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
      // Pagination


      let step1Errors = {};
    
      if (this.state["FilterEmploymentType"] =='') {
        step1Errors["FilterEmploymentType"] = "Please select Report Type.";
      }

      if(this.state.FilterLocation =="" &&  this.state.FilterEmployee ==""){
        step1Errors["FilterLocation"] = "Please select either Location or Employee.";
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      var paraPayPeriod = 'periodProfileId='+this.state.FilterPayPeriod;
      var BatchRecord = '&batchRecord='+this.state.BatchRecord;
      var paraLocation = '&locationId='+this.state.FilterLocation;
      var paraEmploymentType = '&employeementType='+this.state.FilterEmploymentType;
      var paraContactId = '&contactId='+this.state.FilterEmployee;
      var paraneedToExport = '&needToExport=false';
      var paraDisplayOnlyActive = '&displayOnlyActive='+this.state.display_Inactive_flage;
      var paraneedToExport = '&needToExport=false';
      
      //ExportFilter
      let ExportFilterArray = {
        FilterPayPeriod: this.state.FilterPayPeriod,
        FilterLocation: this.state.FilterLocation,
        FilterEmploymentType: this.state.FilterEmploymentType,
        FilterEmployee: this.state.FilterEmployee,
        FilterDisplayOnlyActive: this.state.display_Inactive_flage,
        FilterPagingPara: url_paging_para,
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      if(this.state.FilterEmploymentType == "false"){
        var ApiName= 'GetPTBatchPayrollReport?'
        //var ApiName= 'GetPTBatchPayrollReportJson?'
      }else{
        var ApiName= 'GetFTBatchPayrollReport?'
        //var ApiName= 'GetFTBatchPayrollReportJson?'
      }

      var pass_url = ApiName+paraPayPeriod+BatchRecord+paraLocation+paraEmploymentType+paraContactId+paraDisplayOnlyActive+paraneedToExport+url_paging_para;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetPTBatchPayrollReport ");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                //console.log("TableHeaderDesign");
                //console.log(this.TableHeaderDesign());
                //this.AddSaveFTPTprocesslogs();

                this.setState({ header_data: this.TableHeaderDesign() });
                this.setState({ ListGrid: this.rowData(data.data) });
                this.setState({ pagingData: data.pagingData });
              }
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        console.log('GetReportListGrid error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  GetReportListGridReload () {
    
      //e.preventDefault();

      this.setState({ ListGrid : [] });
      this.setState({ ExportFilter: {} });
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */


      let step1Errors = {};
    
      if (this.state["FilterEmploymentType"] =='') {
        step1Errors["FilterEmploymentType"] = "Please select Report Type.";
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      var paraPayPeriod = 'periodProfileId='+this.state.FilterPayPeriod;
      var paraLocation = '&locationId='+this.state.FilterLocation;
      var paraEmploymentType = '&employeementType='+this.state.FilterEmploymentType;
      var paraContactId = '&contactId='+this.state.FilterEmployee;
      var paraneedToExport = '&needToExport=false';
      var paraDisplayOnlyActive = '&displayOnlyActive='+this.state.display_Inactive_flage;
      
      //ExportFilter
      let ExportFilterArray = {
        FilterPayPeriod: this.state.FilterPayPeriod,
        FilterLocation: this.state.FilterLocation,
        FilterEmploymentType: this.state.FilterEmploymentType,
        FilterEmployee: this.state.FilterEmployee,
        FilterDisplayOnlyActive: this.state.display_Inactive_flage,
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      if(this.state.FilterEmploymentType == "false"){
        var ApiName= 'GetPTBatchPayrollReport?'
      }else{
        var ApiName= 'GetFTBatchPayrollReport?'
      }

      var pass_url = ApiName+paraPayPeriod+paraLocation+paraEmploymentType+paraContactId+paraneedToExport+paraDisplayOnlyActive;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetPTBatchPayrollReport GetReportListGridReload");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                //console.log("TableHeaderDesign");
                //console.log(this.TableHeaderDesign());
                this.setState({ header_data: this.TableHeaderDesign() });
                this.setState({ ListGrid: this.rowData(data.data) });

              }
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        console.log('GetReportListGridReload error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }
  // FT PT report Main grid API

  // Save FT PT Process Logs
  AddSaveFTPTprocesslogs () {
    
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */


      let step1Errors = {};
    
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      var paraEmploymentType = "FT";
      if(this.state.FilterEmploymentType == "false"){
        var paraEmploymentType = "PT";
      }

      let bodyarray = {};
      bodyarray["periodMasterId"] = this.state.FilterPayPeriod;
      bodyarray["reportType"] = paraEmploymentType;
      bodyarray["processBy"] = this.state.staffContactID;

      var ApiName= 'Saveftptprocesslogs'
      
      var pass_url = ApiName;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson Saveftptprocesslogs");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                
              }
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        console.log('Saveftptprocesslogs error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }
  // Save FT PT Process Logs

  // FT PT report Main grid "Export Excel" API
  ExportReportData = () => e => {

      e.preventDefault();
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      /* Role Management */

      var FilterPara = this.state.ExportFilter;
      console.log('payload GetTimesheetReportDataExport');
      console.log(FilterPara);
      //console.log(FilterPara.FilterPayPeriod);
      //return false;
      
      var paraPayPeriod = 'periodProfileId='+FilterPara.FilterPayPeriod;
      var BatchRecord = '&batchRecord='+this.state.BatchRecord;
      var paraLocation = '&locationId='+FilterPara.FilterLocation;
      var paraEmploymentType = '&employeementType='+FilterPara.FilterEmploymentType;
      var paraContactId = '&contactId='+FilterPara.FilterEmployee;
      var paraneedToExport = '&needToExport=true';
      var paraDisplayOnlyActive = '&displayOnlyActive='+FilterPara.FilterDisplayOnlyActive;
      var paraFilterPagingPara = FilterPara.FilterPagingPara;

      if(this.state.FilterEmploymentType == "false"){
        var ApiName= 'GetPTBatchPayrollReport?'
      }else{
        var ApiName= 'GetFTBatchPayrollReport?'
      }

      var pass_url = ApiName+paraPayPeriod+BatchRecord+paraLocation+paraEmploymentType+paraContactId+paraneedToExport+paraDisplayOnlyActive+paraFilterPagingPara;
      console.log(pass_url);
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson ExportReportData Batch payroll");
          console.log(data);
          if (data.responseType === "1") {

              if(data.data != null)
              {
                //alert();
                var today = new Date();
                var y = today.getFullYear();
                var m = today.getMonth() + 1;
                var d = today.getDate();
                var h = today.getHours();
                var mi = today.getMinutes();
                var s = today.getSeconds();
                var ms = today.getMilliseconds();
                var time = "PHSS000"+y  + m  + d  + h  + mi  + s + ms+FileUploadHelper.randomFun();

                // File name
                var payperiodname = $('#FilterPayPeriod').find(':selected').data('payperiodname');
                
                let firstChar = $.trim(payperiodname.split(" ")[0]);
                let lastChar = $.trim(payperiodname.split(" ").pop());
                
                if(this.state.FilterEmploymentType == "false"){
                  var File_Name= 'PT Payroll Batch '+payperiodname;
                }else{
                  var name_ptfile = moment(firstChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT)+ " to " +moment(lastChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT);
                  var File_Name= 'FT Payroll Batch '+name_ptfile;
                }
                // File name
                
                var createBase64 = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+data.data;
                var ext=File_Name+'.xlsx';
                
                const linkSource = createBase64;
                const downloadLink = document.createElement("a");
                const FileName = ext;

                downloadLink.href = linkSource;
                downloadLink.download = FileName;
                downloadLink.click();

              }
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        console.log('ExportReportData error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }
  // FT PT report Main grid "Export Excel" API

  // FT PT report Main grid "Export Excel" API
  ProcessReportData = () => e => {

      e.preventDefault();
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      /* Role Management */

      var FilterPayPeriod = this.state.FilterPayPeriod;
      console.log('payload GetTimesheetReportDataExport');
      console.log(FilterPayPeriod);

     

      if(this.state.FilterEmploymentType == ""){
        SystemHelpers.ToastError("Please Select Report Type.");
        return false;
      }

      if(this.state.FilterPayPeriod == ""){
        SystemHelpers.ToastError("Please Select Pay Period.");
        return false;
      }
      
      var paraEmploymentType = "FT";
      if(this.state.FilterEmploymentType == "false"){
        var paraEmploymentType = "PT";
      }

    
      //return false;

      this.showLoader();
      var url=process.env.API_API_URL+"ProcessReportData?periodProfileId="+FilterPayPeriod+"&employeementType="+paraEmploymentType;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson ProcessReportData Batch payroll");
          console.log(data);
          if (data.responseType === "1") {

              SystemHelpers.ToastSuccess("success");
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        console.log('ExportReportData error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }
  // FT PT report Main grid "Export Excel" API

  // FT PT report Main grid row data
  rowData(ListGrid) {
    console.log('rowData Bacth Payroll');
    console.log(ListGrid);

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    /* Role Management */
    
    var ListGrid_length = ListGrid.length;
    let dataArray = [];
    
    for (var x = 0; x < ListGrid_length; x++)
    {
      if(ListGrid != null)
      {
        // PT false, FT true
          var tempdataArray = [];

          if(this.state.FilterEmploymentType == "true")
          {
            tempdataArray.push(<tr>
              <td>{ListGrid[x].staffNumberId}</td>
              <td>{ListGrid[x].employeeName}</td>
              <td>{ListGrid[x].locationCode}</td>
              <td>{ListGrid[x].locationName}</td>
              <td>{ListGrid[x].regHours}</td>
              <td>{ListGrid[x].sleepHours}</td>
              <td>{ListGrid[x].ftotHours}</td>
              
              <td>{ListGrid[x].straightTime}</td>
              <td>{ListGrid[x].overTime}</td>
              <td>{ListGrid[x].trip}</td>
              <td>{ListGrid[x].ftv}</td>
              <td>{ListGrid[x].mbtu}</td>
              <td>{ListGrid[x].btt}</td>
              <td>{ListGrid[x].sck}</td>
              <td>{ListGrid[x].covid19Hours}</td>
              <td>{ListGrid[x].esp}</td>
              <td>{ListGrid[x].unp}</td>
              <td>{ListGrid[x].brv}</td>
              
              <td>{ListGrid[x].onCall}</td>
              {/*<td>{ListGrid[x].onCallPayRef}</td> statBankHours*/}
              <td>{ListGrid[x].onCallStat}</td>
              <td>{ListGrid[x].onCallBankHoursPayPref}</td>
              <td>{ListGrid[x].onCallPayOutPayPref}</td>
              <td>{ListGrid[x].onCallRRSPPayPref}</td>


              <td>{ListGrid[x].statDayoff}</td> 
              <td>{ListGrid[x].stat25}</td>
              <td>{ListGrid[x].stat15}</td>
              <td>{ListGrid[x].stat1}</td>
              <td>{ListGrid[x].statBankHoursPayPref}</td>
              <td>{ListGrid[x].statPayOutPayPref}</td>
              <td>{ListGrid[x].statRRSPPayPref}</td>


              {/*<td>{ListGrid[x].statPayRef}</td>
              <td>{ListGrid[x].otherEarning}</td>
              <td>{ListGrid[x].otherEarningValue}</td>*/}

              <td>{ListGrid[x].recordType}</td>

              <td>{this.Edit_Update_Btn_Func_Pay(ListGrid[x])}</td>
            </tr>);

            dataArray.push(tempdataArray);
          }
          else if(this.state.FilterEmploymentType == "false")
          {
            tempdataArray.push(<tr>
              <td>{ListGrid[x].staffNumberId}</td>
              <td>{ListGrid[x].employeeName}</td>
              <td>{ListGrid[x].locationCode}</td>
              <td>{ListGrid[x].locationName}</td>
              <td>{ListGrid[x].regHours}</td>
              <td>{ListGrid[x].sleepHours}</td>
              <td>{ListGrid[x].critIllPay}</td>
              <td>{ListGrid[x].sck}</td>
              <td>{ListGrid[x].pptDate}</td>
              <td>{ListGrid[x].brv}</td>
              <td>{ListGrid[x].ftv}</td>
              <td>{ListGrid[x].mbtu}</td>
              <td>{ListGrid[x].ptot}</td>
              <td>{ListGrid[x].otHours}</td>
              <td>{ListGrid[x].covid19Hours}</td>
              <td>{ListGrid[x].stat25}</td>
              {/*<td>{ListGrid[x].statRRSPPayPref}</td>
              <td>{ListGrid[x].statBankHoursPayPref}</td>
              <td>{ListGrid[x].statPayOutPayPref}</td>*/}
              
              <td>{ListGrid[x].publicHolidayPay}</td>
              {/*<td>{ListGrid[x].otherEarning}</td>
              <td>{ListGrid[x].otherEarningValue}</td>*/}
              <td>{ListGrid[x].recordType}</td>
              <td>{ListGrid[x].total}</td>

              <td>{this.Edit_Update_Btn_Func_Pay(ListGrid[x])}</td>
            </tr>);
            

            dataArray.push(tempdataArray);
          }

          
        
      }
    
    }
    //console.log('Return rowdata array');
    //console.log(dataArray);
    return dataArray;
  }
  rowData1(ListGrid) {
    console.log('rowData Bacth Payroll');
    console.log(ListGrid);

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    /* Role Management */
    
    var ListGrid_length = ListGrid.length;
    let dataArray = [];
    
    for (var x = 0; x < ListGrid_length; x++)
    {
      if(ListGrid != null)
      {
        // PT false, FT true
          var tempdataArray = [];

          if(this.state.FilterEmploymentType == "true")
          {
            tempdataArray.idEmployee = ListGrid[x].staffNumberId;
            tempdataArray.nameEmployee = ListGrid[x].employeeName;
            tempdataArray.idLocation = ListGrid[x].locationCode;
            tempdataArray.nameLocation = ListGrid[x].locationName;
            tempdataArray.regHours = ListGrid[x].regHours;
            tempdataArray.sleepHours = ListGrid[x].sleepHours;
            tempdataArray.FTOTShifts = ListGrid[x].ftotHours;
            
            tempdataArray.straightTime = ListGrid[x].straightTime;
            tempdataArray.overtime = ListGrid[x].overTime;
            tempdataArray.trip = ListGrid[x].trip;
            tempdataArray.ftv = ListGrid[x].ftv;
            tempdataArray.miscBankUsed = ListGrid[x].mbtu;
            tempdataArray.btt = ListGrid[x].btt;
            tempdataArray.sck = ListGrid[x].sck;
            tempdataArray.covid19 = ListGrid[x].covid19Hours;
            tempdataArray.esp = ListGrid[x].esp;
            tempdataArray.unp = ListGrid[x].unp;
            tempdataArray.brv = ListGrid[x].brv;
            
            tempdataArray.onCall = ListGrid[x].onCall;
            //tempdataArray.onCallPayPref = ListGrid[x].onCallPayRef; statBankHours
            tempdataArray.onCallstat = ListGrid[x].onCallStat;
            tempdataArray.onCallBankHours = ListGrid[x].onCallBankHoursPayPref;
            tempdataArray.onCallPayout = ListGrid[x].onCallPayOutPayPref;
            tempdataArray.onCallRRSP = ListGrid[x].onCallRRSPPayPref;


            tempdataArray.statDayoff = ListGrid[x].statDayoff; 
            tempdataArray.stat25 = ListGrid[x].stat25;
            tempdataArray.stat15 = ListGrid[x].stat15;
            tempdataArray.stat10 = ListGrid[x].stat1;
            tempdataArray.statBankHours = ListGrid[x].statBankHoursPayPref;
            tempdataArray.statPayout = ListGrid[x].statPayOutPayPref;
            tempdataArray.statRRSP = ListGrid[x].statRRSPPayPref;


            /*tempdataArray.statPayPref = ListGrid[x].statPayRef;
            tempdataArray.otherEarnings = ListGrid[x].otherEarning;
            tempdataArray.otherEarningsValue = ListGrid[x].otherEarningValue;*/
            tempdataArray.recType=ListGrid[x].recordType;

            tempdataArray.action = this.Edit_Update_Btn_Func_Pay(ListGrid[x]);

            dataArray.push(tempdataArray);
          }
          else if(this.state.FilterEmploymentType == "false")
          {
            tempdataArray.idEmployee = ListGrid[x].staffNumberId;
            tempdataArray.nameEmployee = ListGrid[x].employeeName;
            tempdataArray.idLocation = ListGrid[x].locationCode;
            tempdataArray.nameLocation = ListGrid[x].locationName;
            tempdataArray.regHours = ListGrid[x].regHours;
            tempdataArray.sleepHours = ListGrid[x].sleepHours;
            tempdataArray.CritIllPay = ListGrid[x].critIllPay;
            tempdataArray.sck = ListGrid[x].sck;
            tempdataArray.PPTDate = ListGrid[x].pptDate;
            tempdataArray.brv = ListGrid[x].brv;
            tempdataArray.ftv = ListGrid[x].ftv;
            tempdataArray.miscBank = ListGrid[x].mbtu;
            tempdataArray.PTOT = ListGrid[x].ptot;
            tempdataArray.OTHours = ListGrid[x].otHours;
            tempdataArray.covid19 = ListGrid[x].covid19Hours;
            tempdataArray.stat25 = ListGrid[x].stat25;
            /*tempdataArray.statRRSP = ListGrid[x].statRRSPPayPref;
            tempdataArray.statBankHours = ListGrid[x].statBankHoursPayPref;
            tempdataArray.statPayout = ListGrid[x].statPayOutPayPref;*/
            
            tempdataArray.publicHoliday = ListGrid[x].publicHolidayPay;
            //tempdataArray.otherEarnings = ListGrid[x].otherEarning;
            //tempdataArray.otherEarningsValue = ListGrid[x].otherEarningValue;
            tempdataArray.recType= ListGrid[x].recordType;
            tempdataArray.total = ListGrid[x].total;

            tempdataArray.action = this.Edit_Update_Btn_Func_Pay(ListGrid[x]);
            
            

            dataArray.push(tempdataArray);
          }

          
        
      }
    
    }
    //console.log('Return rowdata array');
    //console.log(dataArray);
    return dataArray;
  }
  // FT PT report Main grid row data

  // FT PT report Main grid "EDIT button"
  Edit_Update_Btn_Func_Pay(record){
    //console.log("Edit_Update_Btn_Func_Pay");
    //console.log(record);
    let return_push = [];

    var timeSheetPeriodId = this.state.FilterPayPeriod;
    var isTimeSheetApproved = true;
    var isTimeSheetSubmittedToPayroll = true;
    var isSpecifcTimeSheetApprover = true;
    var isPayrollAdmin = true;
    var LocationGUID = '';
    var IsEditBtn = true;
    //var payroll_adjustment_menu_can_view = true;

    let role_permission = {};
    role_permission["isCordinator"] = record.isCordinator;
    role_permission["isPayrollAdmin"] = record.isPayrollAdmin;
    role_permission["isSeniorCordinator"] = record.isSeniorCordinator;
    role_permission["isServiceCoordinatorLead"] = record.isServiceCoordinatorLead;
    role_permission["isCoordinatorMultipleLocations"] = record.isCoordinatorMultipleLocations;

    if(this.state.role_payroll_adjustment_menu_can.payroll_adjustment_menu_can_view == true){
        let Edit_push = [];
        if(this.state.role_payroll_adjustment_menu_can.payroll_adjustment_menu_can_view == true){
          Edit_push.push(
          <a href="#" onClick={this.PastDetails(role_permission,timeSheetPeriodId,record.employeeId,record.employeeName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,record.locationId,isPayrollAdmin,LocationGUID,IsEditBtn)}  className="dropdown-item" data-toggle="modal" data-target="#View_PayrollAdjustments"><i className="fa fa-pencil m-r-5"></i>Edit</a>
          //<a href="#" onClick={this.EditRecord(record)}  className="dropdown-item" data-toggle="modal" data-target="#PayrollAdjustments_Add_modal"><i className="fa fa-pencil m-r-5"></i>Edit</a>
          );
        }
        let Delete_push = [];
        
        
        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {Edit_push}
              {Delete_push}
            </div>
          </div>
        );
      }
      return return_push;
  }
  // FT PT report Main grid "EDIT button"

  // FT PT report Main grid "EDIT button API"
  PastDetails = (role,timesheetPeriodId,contactId,tempfullName,isTimeSheetApproved,isTimeSheetSubmittedToPayroll,isSpecifcTimeSheetApprover,locationId,isPayrollAdmin,LocationGUID,IsEditBtn) => e => {
    e.preventDefault();
    this.showLoader();
    console.log("contactId => "+contactId+" isPayrollAdmin => "+isPayrollAdmin);
    console.log("locationId => "+locationId);
    console.log(this.state.locationViews);
    //console.log('role');
    //console.log(role);

    // PayRoll Adjustment
    var EmploymentType= this.state.FilterEmploymentType;
    // this.GetEmployeePayrollAdjustment(contactId);
    // this.GetPayrollAdjustmentSummaryViews(timesheetPeriodId,locationId,EmploymentType,contactId,role);
    // PayRoll Adjustment

    //this.GetUserLocationWise(contactId,isPayrollAdmin);

    //this.GetTimseheetEntitlementSummarySummationLocationWsie(contactId,LocationGUID,timesheetPeriodId);

    this.setState({ role_permission : role });

    this.setState({ past_display_amendment_due_date: '' });
    this.setState({ past_display_dueDateAmendment: '' });
    //  console.log('timesheetPeriodId =>'+ timesheetPeriodId);
    //  console.log('contactId =>'+ contactId);
    //  console.log('tempfullName =>'+ tempfullName);
    //  console.log('isTimeSheetApproved =>'+ isTimeSheetApproved);
    //  console.log('isTimeSheetSubmittedToPayroll =>'+ isTimeSheetSubmittedToPayroll);
    //  console.log('isSpecifcTimeSheetApprover =>'+ isSpecifcTimeSheetApprover);

    //  console.log("isPayrollAdminSession =>"+this.state.isPayrollAdminSession);
    //  console.log("isCordinatorSession  =>"+this.state.isCordinatorSession);
    //  console.log("isSeniorCordinatorSession  =>"+this.state.isSeniorCordinatorSession);
    //  console.log("isServiceCoordinatorLeadSession  =>"+this.state.isServiceCoordinatorLeadSession);
    
    this.setState({ isTimeSheetSubmitted: false });
    this.setState({ ListGridPastDetails: [] });
    this.setState({ ListGridPastDetailsCount: 0 });
    this.setState({ GetChatListLoad: false });
    
    this.setState({ EdittimeSheetContactId: contactId });
    this.setState({ EdittimesheetPeriodId: timesheetPeriodId });
    this.setState({ EdittimeSheetContactMasterId: ''});
    //alert(isTimeSheetApproved);
    //alert(isTimeSheetSubmittedToPayroll);
    //alert(IsEditBtn);
    this.Default_TimeSheet2(timesheetPeriodId);

    this.setState({ IsEditBtn: IsEditBtn });
    this.setState({ tempfullName: tempfullName });

    this.setState({ isAmendmendsSubmitted : false });
    this.setState({ isTimeSheetSubmitedToPayroll : false });

    var locationViews = this.state.locationViews;
    var LocationGUID ='';
    for (var zz = 0; zz < locationViews.length; zz++) {
      if(locationId == locationViews[zz].locationId){
        this.setState({ Addlocation: locationViews[zz].locationGuid});
        LocationGUID = locationViews[zz].locationGuid;
      }
    }

    
   
    $('#Addlocation').prop('disabled', true);
    $('#Addlocation').css("background-color", "lightgray");
    

    this.setState({ isTimeSheetApproved: false });

    if(!isTimeSheetSubmittedToPayroll){
      this.setState({ isTimeSheetSubmittedToPayroll: false });
    }else{
      this.setState({ isTimeSheetSubmittedToPayroll: isTimeSheetSubmittedToPayroll });
    }
    
    // let locationid = "";
    // if(localStorage.getItem("isPayrollAdmin") == 'false')
    // {
    //   locationid = localStorage.getItem("primaryLocationId");
    // } 

    this.setState({ templocationId: '' });   
    
    //alert(localStorage.getItem("isPayrollAdmin"));
    //alert(locationid);
    this.setState({ EditworkedHour: '00:00:00 h' });

    this.ClearRecord();

    var url=process.env.API_API_URL+'GetUserPastTimeSheetDataById?contactIdFillFor='+contactId+"&timesheetPeriodId="+timesheetPeriodId+"&locationId="+locationId+"&dedicatedApprover="+isSpecifcTimeSheetApprover;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetUserPastTimeSheetDataById");
      console.log(data);
      //console.log(data.data.timeSheetConatctSheetViews);
      // debugger;
      
      this.GetEmployeePayrollAdjustment(contactId);
      //this.GetPayrollAdjustmentSummaryViews(timesheetPeriodId,locationId,EmploymentType,contactId,role);
      this.GetTimseheetEntitlementSummarySummationLocationWsie(contactId,LocationGUID,timesheetPeriodId);

      if (data.responseType === "1") {
        // Profile & Contact
        this.GetProfile_contactId(contactId);
        if(data != null){

          if(data.data.timesheetContactView != null){
              var vartimeSheetConatctSheetViews = data.data.timesheetContactView.timeSheetConatctSheetViews;
              this.setState({ ListGridPastDetailsCount: vartimeSheetConatctSheetViews.length });
              //alert(vartimeSheetConatctSheetViews.length);
              this.setState({ ListGridPastDetails: data.data.timesheetContactView.timeSheetConatctSheetViews });  
              
              

              this.setState({ past_display_amendment_due_date: data.data.timesheetContactView.dueDate });
              this.setState({ past_display_dueDateAmendment: data.data.timesheetContactView.dueDateAmendment });

              this.setState({ isTimeSheetApproved: data.data.timesheetContactView.isTimeSheetApproved });
              this.setState({ IsAmendmendsEnabled : data.data.timesheetContactView.isAmendmendsEnabled });

              this.setState({ isAmendmendsSubmitted : data.data.timesheetContactView.isAmendmendsSubmitted });
              
              this.setState({ isTimeSheetSubmitedToPayroll : data.data.timesheetContactView.isTimeSheetSubmitedToPayroll });

              this.setState({ EdittimeSheetContactMasterId: data.data.timesheetContactView.timeSheetContactMasterId });

              if(vartimeSheetConatctSheetViews.length > 0){
                this.GetTimeSheetNotes(data.data.timesheetContactView.timeSheetContactMasterId,LocationGUID);  
              }
              
              
              this.setState({ isTimeSheetSubmitted: data.data.timesheetContactView.isTimeSheetSubmitted });

              
              this.setState({ EdittimeSheetStartDate: data.data.timesheetContactView.timeSheetStartDate });
              this.setState({ EdittimeSheetEndDate: data.data.timesheetContactView.timeSheetEndDate });
              this.setState({ EditworkedHour: data.data.timesheetContactView.workedHour });
              //this.setState({ timeSheetPeriodViews: data.data.timeSheetPeriodViews });

              this.setState({ templocationId: LocationGUID });
          }

          if(data.data.timeSheetPeriodViews != null){
          //  console.log('timeSheetPeriodViews update');
            this.setState({ timeSheetPeriodViews: data.data.timeSheetPeriodViews });
          }
        }
        
        //console.log(data.data.userSkillInfo);
        //this.setState({ ListGrid: this.rowData(data.data.userSkillInfo) })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserPastTimeSheetDataById error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }
  // FT PT report Main grid "EDIT button API"

  Default_TimeSheet2(timesheetPeriodId){
      var timeSheetPeriod = this.state.payPeriodListFilter;
      var length = timeSheetPeriod.length;
      var MinDate = moment(new Date()).format("YYYY-MM-DD");
      var MaxDate = moment(new Date()).format("YYYY-MM-DD");
      //alert(this.state.timeSheetPeriodId);
      console.log(timeSheetPeriod);
      if (length > 0) {
        var i = 1;
         for (var zz = 0; zz < length; zz++) {

          if(timeSheetPeriod[zz].isCurrentTimeSheet == true && this.state.timeSheetPeriodId == ""){
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});
            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD')
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            this.setState({ past_display_dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });
            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

             this.setState({ timeSheetPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});
            this.setState({ FilterPayPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
            //this.GetUserTimeSheetDataByLocation();
          }else if(timesheetPeriodId == timeSheetPeriod[zz].timeSheetPeriodId){
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            //alert(timeSheetPeriod[zz].isCurrentTimeSheet);
            var AddDate_Max =  moment(timeSheetPeriod[zz].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
            this.setState({ AddTimeIn_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTimeIn_Max: AddDate_Max+"T00:01"});

            this.setState({ AddTempTimeOut_Max: AddDate_Max+"T00:01"});
            this.setState({ EditTempTimeOut_Max: AddDate_Max+"T00:01"});
            
            var AddDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, "days").format('YYYY-MM-DD')
            this.setState({ AddTimeIn_Min: AddDate_Min+"T00:01"});
            var EditDate_Min = moment(timeSheetPeriod[zz].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(2, "days").format('YYYY-MM-DD');
            this.setState({ EditTimeIn_Min: EditDate_Min+"T00:01"});

            this.setState({ CurrentDate_Min: AddDate_Min});
            this.setState({ CurrentDate_Max: AddDate_Max});
            this.setState({ isCurrent: timeSheetPeriod[zz].isCurrentTimeSheet});
            this.setState({ past_display_dueDateAmendment: timeSheetPeriod[zz].dueDateAmendment });

            
            $('#timeSheetPeriod').val(timeSheetPeriod[zz].timeSheetPeriodId).trigger('change');

            this.setState({ timeSheetPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
            this.setState({ timeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});

            this.setState({ CurrenttimeSheetPeriodId: timeSheetPeriod[zz].timeSheetPeriodId});
            this.setState({ FilterPayPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
            //this.GetUserTimeSheetDataByLocation();
          }
          i++;
        }
      }
  }

  // User Get Employee (Full-time or Part-time) Profile api
  GetProfile_contactId(contactId){
    //  console.log(localStorage.getItem("token"));
    this.setState({ UserHoursType: ''});
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+contactId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetProfile_contactId");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ UserHoursType: data.data.employmentHours});     
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
    //  console.log('GetUserBasicInfoById error');
      console.log('GetProfile_contactId error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }
  // User Get Employee (Full-time or Part-time) Profile api
  // ============================================== FT PT report Main grid API ============================================== //


  // ============================================== Summary Of Entries ============================================== // 
  /*GetPayrollAdjustmentSummaryViews(periodProfileId,locationId,employeementType,contactId,roleId){
    
    this.showLoader();

    var periodProfile_Id = "periodProfileId="+periodProfileId;
    var location_Id = "&locationId="+locationId;
    var employeement_Type = "&employeementType="+employeementType;
    var contact_Id = "&contactId="+contactId;
    var role_Id = "&roleId="+roleId;
    
    var url=process.env.API_API_URL+'GetPayrollAdjustmentSummaryViews?'+periodProfile_Id+location_Id+employeement_Type+contact_Id+role_Id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetPayrollAdjustmentSummaryViews");
        console.log(data);
        if (data.responseType === "1" && data.data != null) {
          this.setState({ SummaryOvertimeTotalRegHours: data.data.totalRegHours });
          this.setState({ SummaryOvertimeTotalSickHours: data.data.totalSickHours });
          this.setState({ SummaryOvertimeTotalOTHours: data.data.totalOTHours });
          this.setState({ SummaryOvertimeTotalAsleepHours: data.data.totalAsleepHours });
          this.setState({ SummaryOvertimeTotal24HoursTrip: data.data.total24HoursTrip });
          this.setState({ SummaryOvertimeTotalBRVHours: data.data.totalBRVHours });
          this.setState({ SummaryOvertimeTotalLeaveHours: data.data.totalLeaveHours });
          this.setState({ SummaryOvertimeTotalOnCall: data.data.totalOnCall });
          this.setState({ SummaryOvertimeTotalStatsHours: data.data.totalStatsHours });
          this.setState({ SummaryOvertimeBankedTimeUsed: data.data.bankedTimeUsed });
          this.setState({ SummaryOvertimeBankedTimeEarn: data.data.bankedTimeEarn });
          this.setState({ SummaryOvertimeSpecialBankTime: data.data.specialBankTime });
          this.setState({ SummaryOvertimeOtPaid: data.data.otPaid });
          this.setState({ SummaryOvertimeTotalBankedHoursTime: data.data.totalBankedHoursTime });
          this.setState({ SummaryOvertimeOverTimeSummary: data.data.overTimeSummary });
          
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      console.log('GetPayrollAdjustmentSummaryViews error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }*/
  GetTimseheetEntitlementSummarySummationLocationWsie(contactId,LocationGUID,periodProfileId){

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */

      this.setState({ SummaryOfEntriesDisplaytotalRegularHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalAsleepHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalTripHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalVacationHours: '00.00' });

      this.setState({ SummaryOfEntriesDisplaytotalSickHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalCOVID19Hours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalBRVHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalLeaveHours: '00.00' });

      this.setState({ SummaryOfEntriesDisplaytotalOTHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalOTBankedUsedHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalMiscBankedUsedHours: '00.00' });
      
      this.setState({ SummaryOfEntriesDisplaytotalOnCallHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalStatHours: '00.00' });
      this.setState({ SummaryOfEntriesDisplaytotalStatDayOffHours: '00.00' });

      this.showLoader();
      var url=process.env.API_API_URL+'GetTimseheetSummarySummationDataLocationWsie'+'?contactId='+contactId+'&locationId='+LocationGUID+'&periodProfileId='+periodProfileId;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetTimseheetSummarySummationDataLocationWsie");
          console.log(data);
          if (data.responseType === "1") {
              if(data.data != null)
              {
                this.setState({ SummaryOfEntriesDisplaytotalRegularHours: data.data.totalRegularHours });
                this.setState({ SummaryOfEntriesDisplaytotalAsleepHours: data.data.totalAsleepHours });
                this.setState({ SummaryOfEntriesDisplaytotalTripHours: data.data.totalTripHours });
                this.setState({ SummaryOfEntriesDisplaytotalVacationHours: data.data.totalVactionHours });

                this.setState({ SummaryOfEntriesDisplaytotalSickHours: data.data.totalSickHours });
                this.setState({ SummaryOfEntriesDisplaytotalCOVID19Hours: data.data.totalCovid19Hours });
                this.setState({ SummaryOfEntriesDisplaytotalBRVHours: data.data.totalBRVHours });
                this.setState({ SummaryOfEntriesDisplaytotalLeaveHours: data.data.totalLeaveHours });

                this.setState({ SummaryOfEntriesDisplaytotalOTHours: data.data.totalOTHours });
                this.setState({ SummaryOfEntriesDisplaytotalOTBankedUsedHours: data.data.totalOTBankUsedHours });
                this.setState({ SummaryOfEntriesDisplaytotalMiscBankedUsedHours: data.data.totalMisBankUsedHours });
                
                this.setState({ SummaryOfEntriesDisplaytotalOnCallHours: data.data.totalOnCallHours });
                this.setState({ SummaryOfEntriesDisplaytotalStatHours: data.data.totalStatHours });
                this.setState({ SummaryOfEntriesDisplaytotalStatDayOffHours: data.data.totalStatDayOff });
              }
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        console.log('GetTimseheetSummarySummationDataLocationWsie error');
        this.props.history.push("/error-500");
      });
  }
  // ============================================== Summary Of Entries ============================================== //

  
  // ============================================== Time Sheet ============================================== //
  Preview_TimeSheet(timeSheetPeriod){
    var length = timeSheetPeriod.length;

    console.log('Preview_TimeSheet 123');
    console.log(timeSheetPeriod);

    if (length > 0) {
      let return_push = [];
      for (var zz = 0; zz < length; zz++) {
        if(timeSheetPeriod[zz].isCurrentTimeSheet == true){
          var isCurrentTimeSheet = timeSheetPeriod[zz];
          var PreviewTimesheetDate = moment(isCurrentTimeSheet.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).subtract(1, 'day').format('MM-DD-YYYY');
        //  console.log(timeSheetPeriod);
        //  console.log("PreviewTimesheetDate => "+PreviewTimesheetDate);
          this.setState({ PreviewTimesheetDate: PreviewTimesheetDate});

          for (var jj = 0; jj < length; jj++) {
            var LoopTimesheetDate = moment(timeSheetPeriod[jj].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('MM-DD-YYYY');
            var LoopTimesheetDateX =LoopTimesheetDate;
            //var diffDate = LoopTimesheetDate.diff(PreviewTimesheetDate, 'days');
            //console.log('diffDate 1 ' + LoopTimesheetDate + " " + PreviewTimesheetDate);
            //console.log(diffDate);
            var TempLoopTimesheetDate=moment(LoopTimesheetDate,'MM-DD-YYYY').format('YYYY-MM-DD');
            var TempPreviewTimesheetDate=moment(PreviewTimesheetDate,'MM-DD-YYYY').format('YYYY-MM-DD');


            let x = moment(TempLoopTimesheetDate,'YYYY-MM-DD');
            let y = moment(TempPreviewTimesheetDate,'YYYY-MM-DD');

            var CheckDays = x.diff(y, 'days');
            //console.log('diffDate '+CheckDays+" "+ + LoopTimesheetDateX + " " + TempPreviewTimesheetDate);

            var timeSheetPeriodEndDate = moment(timeSheetPeriod[jj].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('MM-DD-YYYY');
            if(timeSheetPeriodEndDate == PreviewTimesheetDate){
              this.setState({ PreviewtimeSheetPeriodId:  timeSheetPeriod[jj].timeSheetPeriodId});
            }
            
            if(timeSheetPeriod[jj].isCurrentTimeSheet == true || CheckDays > 0   || timeSheetPeriodEndDate == PreviewTimesheetDate ){
              
              console.log('timeSheetPeriod[jj] => '+ jj);
              console.log(timeSheetPeriod[jj].isCurrentTimeSheet);
              console.log(CheckDays);
              console.log(timeSheetPeriodEndDate);
              console.log(PreviewTimesheetDate);

              return_push.push(timeSheetPeriod[jj]); 
            }
          }
        }
      }

    //  console.log('return_push');
    //  console.log(return_push);

      return return_push;
    }
  }

  ClearRecord = () => e => {
    e.preventDefault();

    this.setState({ AddTimeIn : '' });
    this.setState({ AddTimeOut : '' });
    //this.setState({ Addlocation : '' });
    this.setState({ Addlocation: this.state.locationIdGuid}); 
    this.setState({ AddNotes : '' });
    this.setState({ AddNoOfHours : '' });
    this.setState({ AddService : '' });

    this.setState({ filePreviewsFinalEdit: [] });
    this.setState({ filePreviewsFinalEditActive: 0 });
    this.setState({ allow_emp: false });
    this.setState({ EditNoteId: '' });
    this.setState({ AddMsg: '' });

    this.setState({ AddPreference : [] });

    this.setState({ errormsg: '' });

    $('.readonly-cls-AddTimeOut').css("background-color", "#fff");
  }

  AddRecord = () => e => {
    //debugger;

    e.preventDefault();

    let step1Errors = {};

    var timeSheetStartDate = '';
    var timeSheetEndDate = '';
    var timeSheetContactMasterId = '';
    var timeSheetPeriodId = '';
    var dueDate ='';
    
    var TimeSheet_PeriodViews = this.state.timeSheetPeriodViews;

    //  console.log('timeSheetPeriodViews');
    //console.log(TimeSheet_PeriodViews);
    var IsEntitlemntBasedOnStartDate = true;
    if(this.state["AddTimeIn"] != "" && this.state["AddTimeOut"] != "")
    {
      

      for (var i = 0; i < TimeSheet_PeriodViews.length; i++) {
        // this.state["AddTimeOut"]

        var checkStartDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD');
        var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');


        if(checkStartDate != checkEndDate){
          

          var FirstDay1 = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD HH:mm');
          var FirstDay2 = checkStartDate+" 24:00";

          var SecondDay1 = checkEndDate+" 00:00";
          var SecondDay2 = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD HH:mm');

          console.log("FirstDay1 => "+FirstDay1);
          console.log("FirstDay2 => "+FirstDay2);

          //console.log("SecondDay1 => "+SecondDay1);
          //console.log("SecondDay2 => "+SecondDay2);

          var duration1 = moment.duration(moment(FirstDay2).diff(moment(FirstDay1)));
          var FirstDayMinutes = duration1.asMinutes();

          var duration2 = moment.duration(moment(SecondDay2).diff(moment(SecondDay1)));
          var SecondDayMinutes = duration2.asMinutes();

          //console.log(FirstDayMinutes);
          //console.log(SecondDayMinutes);

          if(FirstDayMinutes > SecondDayMinutes && FirstDayMinutes != SecondDayMinutes){
            var checkEndDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = true;
          }else{
            var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = false;
          }

          if(this.state.AddService == process.env.API_ON_CALL){
            var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = false;
          }

        }else{
          var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
          IsEntitlemntBasedOnStartDate = false;
        }

        console.log(checkEndDate);
        //return false;
        //var checkEndDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD')
        var start_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        var end_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        //var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date);
        var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date, null, '[]');
      //  console.log('start_date => '+ start_date + ' end_date => '+ end_date + ' checkEndDate => '+ checkEndDate);
      //  console.log('time sheet check end date');
      //  console.log(check_is_exists);
        if(check_is_exists == true || end_date == checkEndDate)
        {

          timeSheetStartDate = start_date;
          timeSheetEndDate = end_date;
          timeSheetContactMasterId = TimeSheet_PeriodViews[i].timeSheetContactMasterId;
          timeSheetPeriodId = TimeSheet_PeriodViews[i].timeSheetPeriodId;
          dueDate = TimeSheet_PeriodViews[i].dueDate;
        }
      }
    }
    //return false;

    // if (this.state["AddDate"] =='') {
    //   step1Errors["AddDate"] = "Date is mandatory";
    // }
    

    var AddTimeInDate = moment(this.state["AddTimeIn"]).format("MM-DD-YYYY");
    // var sheetinfo = this.GetTimeSheetId(AddTimeInDate);
    // console.log('GetTimeSheetId');
    // console.log(sheetinfo);

    // if(sheetinfo  == false){
    //   step1Errors["AddTimeIn"] = "Please Check Your Timesheet Date.";
    // }

    if (this.state["AddTimeIn"] == '') {
      step1Errors["AddTimeIn"] = "TimeIn is mandatory";
    }

    if (this.state["AddTimeOut"] == '') {
      step1Errors["AddTimeOut"] = "TimeOut is mandatory";
    }

    var AddTimeInDateTemp = moment(this.state["AddTimeIn"]).format("YYYY-MM-DD");
    var AddTimeOutDateTemp = moment(this.state["AddTimeOut"]).format("YYYY-MM-DD");
    //console.log("AddTimeInDateTemp =>"+AddTimeInDateTemp+" CurrentDate_Min =>"+this.state.CurrentDate_Min);
    if(AddTimeInDateTemp == this.state.CurrentDate_Min && AddTimeOutDateTemp == this.state.CurrentDate_Min){
      step1Errors["AddTimeOut"] = "Time IN and Time OUT values should not belong to Previous pay period dates.";
    }

    if (this.state["Addlocation"] == '') {
      step1Errors["Addlocation"] = "Location is mandatory";
    }

    if (this.state["AddService"] == '') {
      step1Errors["AddService"] = "Service is mandatory";
    }

    if(this.state.IsNoOfHoursvalid == false && this.state.AddService != process.env.API_ON_CALL){
      step1Errors["AddNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
    }

    if(this.state.AddPreference.length > 0 && this.state.AddPreferenceService == ''){
      step1Errors["AddPreferenceService"] = "Payout Preference is mandatory.";
    }

    




    //  console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    if(timeSheetPeriodId == "")
    {
      SystemHelpers.ToastError("Please contact system administrator");
      return false;
    }

    //return false;

    // find CategoryId, SubCategoryId, OperationType
    var CategoryId = '';
    var SubCategoryId = '';
    var OperationType = '';
    var isStatHolidayRuleApplicable = '';
    var HolidayType = null;
    var statHolidayCategoryId = null;
    var statHolidaySubCategoryReasonId = null;
    var statHoursToConsider = null;
    var hoursToConsider = null;
    var isStatBankHous = null;
    var isStatRRSP = null;
    var isStatPayout = null;
    var statEntitlementHours = null;

    var IsAddInDateHoliday = this.state.IsAddInDateHoliday;
    var IsAddOutDateHoliday = this.state.IsAddOutDateHoliday;
    console.log("IsAddInDateHoliday => "+IsAddInDateHoliday);
    console.log("IsAddOutDateHoliday => "+IsAddOutDateHoliday);
    var HolidayType = SystemHelpers.GetHolidayType(IsAddInDateHoliday,IsAddOutDateHoliday,this.state.AddService);

    var serviceView = this.state.serviceView;
    
    // console.log("All Service View Drop Down");
    // console.log(this.state.serviceView);

    for (var z = 0; z < serviceView.length; z++)
    {
      // console.log("For if 1");
      // console.log(serviceView[z].serviceId);
      // console.log(this.state["AddService"]);
      if(serviceView[z].serviceId == this.state["AddService"])
      {
        // console.log("For if 2");
        // console.log(this.state.AddPreferenceService);
        // console.log(process.env.API_BANK_HOURS);
        //if(this.state.AddPreferenceService == process.env.API_BANK_HOURS){
        //if(serviceView[z].operationType != "Bank Hours"){
          //console.log("For if 3");
          //console.log(serviceView[z].operationType);
          OperationType = serviceView[z].operationType;
        //}
        // HR
        /*else if(serviceView[z].operationType !=""){
          console.log("For if 4");
          console.log(serviceView[z].operationType);
          OperationType = serviceView[z].operationType;
        }*/

        // HR

        CategoryId = serviceView[z].categoryId;
        SubCategoryId = serviceView[z].subCategoryId;
        isStatHolidayRuleApplicable = serviceView[z].isStatHolidayRuleApplicable;
        statHolidayCategoryId = serviceView[z].statHolidayCategoryId;
        statHolidaySubCategoryReasonId = serviceView[z].statHolidaySubCategoryReasonId;
        statHoursToConsider = serviceView[z].statHoursToConsider;
        hoursToConsider = serviceView[z].hoursToConsider;
        isStatBankHous = serviceView[z].isStatBankHous;
        isStatRRSP = serviceView[z].isStatRRSP;
        isStatPayout = serviceView[z].isStatPayout;
      }
      
    }

    // find CategoryId, SubCategoryId, OperationType

    // console.log("AddPreferenceService");
    // console.log(this.state.AddPreferenceService);
    // console.log("API_BANK_HOURS");
    // console.log(process.env.API_BANK_HOURS);

    this.showLoader();
    var AddTimeIn=moment(this.state["AddTimeIn"]).format('YYYY-MM-DD hh:mm A');
    var AddTimeOut=moment(this.state["AddTimeOut"]).format('YYYY-MM-DD hh:mm A');
    
    //return false;

    var isAmendment = false;
    if(this.state.isTimeSheetSubmitted == true || this.state.isTimeSheetApproved == true || this.state.isTimeSheetSubmitedToPayroll == true){
      isAmendment = true;
    }else{
      isAmendment = false;
    }

    let currentDate = moment();
    //let currentDate = moment('2021-08-15');
    let DueDate = moment(dueDate);

    var CheckisAmendment = DueDate.diff(currentDate, 'days');

    if(CheckisAmendment < 0){
      isAmendment = true;
    }
    
    //console.log(DueDate.diff(currentDate, 'days'));// => 1
    //console.log(moment());
    //return false;

    let ArrayJson = [];

    if(this.state.AddService == process.env.API_ON_CALL){

      var today = moment();
      let datesCollection = [];
      //console.log(this.state["AddTimeIn"]);
      //console.log(this.state["AddTimeOut"]);
      var startdate = moment(this.state["AddTimeIn"], "DD/MM/YYYY");

      if(this.state["AddTimeOut"] != ''){
        var enddate = moment(this.state["AddTimeOut"], "DD/MM/YYYY");
        var diff_day_count = enddate.diff(startdate, 'days') // 1
      }else{
        var diff_day_count = 1;
      }
      

      //console.log(diff_day_count);
      var z = 0;
      var zx = 1;

      var TempTimeIn=moment(this.state["AddTimeIn"]).format('hh:mm A');
      var TempTimeOut=moment(this.state["AddTimeOut"]).format('hh:mm A');
      var checkEndDateOnCall = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
      ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : checkEndDateOnCall,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn,
            timeOUT: AddTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: '00:00',
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : false,
            isStatHolidayRuleApplicable : isStatHolidayRuleApplicable,
            HolidayType : HolidayType,
            statsHoursType : HolidayType,
            statHours : "00:00",
            normalHours : "00:00",
            statHolidayCategoryId : statHolidayCategoryId,
            statHolidaySubCategoryReasonId : statHolidaySubCategoryReasonId,
            statHoursToConsider : statHoursToConsider,
            hoursToConsider : hoursToConsider,
            isStatBankHous : isStatBankHous,
            isStatRRSP : isStatRRSP,
            isStatPayout : isStatPayout,
            holidayType : isStatPayout
      });

      for (var i = 0; i <= diff_day_count; i++) {
        var AddTimeIn = moment(this.state["AddTimeIn"]).add(z, 'days').format('YYYY-MM-DD');
        var AddTimeOut = moment(this.state["AddTimeIn"]).add(zx, 'days').format('YYYY-MM-DD');

        ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : AddTimeIn,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn+" "+TempTimeIn,
            timeOUT: AddTimeIn+" "+TempTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: '00:00',
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:false,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate,
            isStatHolidayRuleApplicable : isStatHolidayRuleApplicable,
            HolidayType : HolidayType,
            statsHoursType : HolidayType,
            statHours : "00:00",
            normalHours : "00:00",
            statHolidayCategoryId : statHolidayCategoryId,
            statHolidaySubCategoryReasonId : statHolidaySubCategoryReasonId,
            statHoursToConsider : statHoursToConsider,
            hoursToConsider : hoursToConsider,
            isStatBankHous : isStatBankHous,
            isStatRRSP : isStatRRSP,
            isStatPayout : isStatPayout,
            holidayType : isStatPayout
        });

        z++;
        zx++;
      }

    }else{
      var AddTimeInDate = moment(checkEndDate,'YYYY-MM-DD').format("MM-DD-YYYY");

    // *************************************** Hours Count ****************************//
    
    if(  (this.state.IsAddInDateHoliday == true || this.state.IsAddOutDateHoliday == true || istest == true)){
      
      var istest = false;
      var checkStartDate = moment(this.state["AddTimeIn"]).format('YYYY-MM-DD');
      var checkEndDate = moment(this.state["AddTimeOut"]).format('YYYY-MM-DD');
      var IsAddInDateHoliday = this.state.IsAddInDateHoliday;
      var IsAddOutDateHoliday = this.state.IsAddOutDateHoliday;
      var startdatetime = this.state["AddTimeIn"];
      var enddatetime = this.state["AddTimeOut"];

      var GetHolidayHours = SystemHelpers.GetHolidayHours(IsAddInDateHoliday,IsAddOutDateHoliday,startdatetime,enddatetime);
      console.log(GetHolidayHours); 

      var statHours = '';
      var normalHours = '';

      if(isStatHolidayRuleApplicable == false){
        var statHours = "00:00";
        var normalHours = this.state["AddNoOfHours"];
      }else{
        var statHours = GetHolidayHours.statHours;
        var normalHours = GetHolidayHours.normalHours;
      }
       

      ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : AddTimeInDate,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn,
            timeOUT: AddTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: this.state["AddNoOfHours"],
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate,
            isStatHolidayRuleApplicable : isStatHolidayRuleApplicable,
            HolidayType : HolidayType,
            statsHoursType : HolidayType,
            statHours : statHours,
            normalHours : normalHours,
            statHolidayCategoryId : statHolidayCategoryId,
            statHolidaySubCategoryReasonId : statHolidaySubCategoryReasonId,
            statHoursToConsider : statHoursToConsider,
            hoursToConsider : hoursToConsider,
            isStatBankHous : isStatBankHous,
            isStatRRSP : isStatRRSP,
            isStatPayout : isStatPayout,
            holidayType : isStatPayout
      });
    }else{
      ArrayJson.push({
            serviceId: this.state["AddService"],
            timeSheetDate : AddTimeInDate,
            PayTimeOffId: this.state["AddPreferenceService"],
            timeIN: AddTimeIn,
            timeOUT: AddTimeOut,
            timeLocationId: this.state["Addlocation"],
            note: this.state["AddNotes"],
            noOfHours: this.state["AddNoOfHours"],
            timeSheetTransactionId:'',
            IsAmendmends:isAmendment,
            RecordType:'',
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate,
            isStatHolidayRuleApplicable : isStatHolidayRuleApplicable,
            HolidayType : HolidayType,
            statsHoursType : HolidayType,
            statHours : "00:00",
            normalHours : this.state["AddNoOfHours"],
            statHolidayCategoryId : statHolidayCategoryId,
            statHolidaySubCategoryReasonId : statHolidaySubCategoryReasonId,
            statHoursToConsider : statHoursToConsider,
            hoursToConsider : hoursToConsider,
            isStatBankHous : isStatBankHous,
            isStatRRSP : isStatRRSP,
            isStatPayout : isStatPayout,
            holidayType : isStatPayout
      });
    }
    
    //return false;

    // *************************************** Hours Count ****************************//

      
    }


    //console.log(ArrayJson);
    //console.log("=======================Timesheet Entry=======================");
    //return false;
    

    //var timeSheetStartDate = moment(sheetinfo.timeSheetPeriodStartDate).format("YYYY-MM-DD");
    //var timeSheetEndDate = moment(sheetinfo.timeSheetPeriodEndDate).format("YYYY-MM-DD");

    //var timeSheetStartDate = moment(this.state.CurrentDate_Min).format("YYYY-MM-DD");
    //var timeSheetEndDate = moment(this.state.CurrentDate_Max).format("YYYY-MM-DD");
    
    //var timeSheetPeriodId = this.state.timeSheetPeriodId;
    //var timeSheetContactMasterId=this.state.timeSheetContactMasterId;

    var IsOnCall = false;
    if(this.state.AddService == process.env.API_ON_CALL){
      var IsOnCall = true;
    }
    

    let bodyarray = {};
    bodyarray["timeSheetContactMasterId"] = timeSheetContactMasterId;
    bodyarray["isTimeSheetSubmitted"] = false;
    bodyarray["TimeSheetPeriodId"] = timeSheetPeriodId;
    bodyarray["timeSheetStartDate"] = timeSheetStartDate;
    bodyarray["timeSheetEndDate"] = timeSheetEndDate;
    bodyarray["timeSheetContactId"] = this.state.EdittimeSheetContactId;
    bodyarray["timeSheetCreateBy"] = this.state.staffContactID;
    bodyarray["IsOnCall"] = IsOnCall;
    bodyarray["UpdateByName"] = this.state.fullName;
    bodyarray["timeSheetConatctSheetViews"] = ArrayJson;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'InsertUpdateUserTimeSheetData';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson userSkillInfo");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            if(this.state.timeSheetPeriod != timeSheetPeriodId && this.state.AddService == process.env.API_ON_CALL){
              var msg_res = "Please note your on-call entry falls between two pay periods. The timesheet entry will reflect on the 2nd pay period.";
            }else{
              var msg_res = data.responseMessge;
            }

            SystemHelpers.ToastSuccess(msg_res);


            this.setState({ AddTimeIn : '' });
            this.setState({ AddTimeOut : '' });
            //this.setState({ Addlocation : '' });
            this.setState({ Addlocation: this.state.locationIdGuid}); 
            this.setState({ AddNotes : '' });
            this.setState({ AddNoOfHours : '' });
            this.setState({ AddService : '' });
            this.setState({ AddPreferenceService : '' });
            this.setState({ AddPreference: [] });
            $('.readonly-cls-AddTimeOut').css("background-color", "#fff"); 
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories

            

            //SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            //this.setState({ timeSheetPeriodViews: data.data});
            this.setState({ timeSheetPeriodViews: this.Preview_TimeSheet(data.data)});
            this.GetReportListGrid(this.state.currentPage,this.state.pageSize,this.state.searchText);
            //this.GetReportListGridReload();
            //this.GetUserWiseTimeSheetData();
        }
        else if(data.responseType === "2"){
           SystemHelpers.ToastError(data.responseMessge);
          // $( ".close" ).trigger( "click" );
        }
        else{
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log("AddRecord Error");
      console.log(error);
      this.props.history.push("/error-500");
    });
    return false;
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};


    var timeSheetStartDate = '';
    var timeSheetEndDate = '';
    var timeSheetContactMasterId = '';
    var timeSheetPeriodId = '';
    var dueDate = '';
    var TimeSheet_PeriodViews = this.state.timeSheetPeriodViews;

    //  console.log('timeSheetPeriodViews');
    //  console.log(TimeSheet_PeriodViews);
    var IsEntitlemntBasedOnStartDate = true;
    var timeSheetContactMasterId = this.state.EdittimeSheetContactMasterId;
    if(this.state["EditTimeIn"] != "")
    {
      

      for (var i = 0; i < TimeSheet_PeriodViews.length; i++) {

        var checkStartDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD');
        var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');

        if(checkStartDate != checkEndDate){
          

          var FirstDay1 = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD HH:mm');
          var FirstDay2 = checkStartDate+" 24:00";

          var SecondDay1 = checkEndDate+" 00:00";
          var SecondDay2 = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD HH:mm');

          console.log("FirstDay1 => "+FirstDay1);
          console.log("FirstDay2 => "+FirstDay2);

          console.log("SecondDay1 => "+SecondDay1);
          console.log("SecondDay2 => "+SecondDay2);

          var duration1 = moment.duration(moment(FirstDay2).diff(moment(FirstDay1)));
          var FirstDayMinutes = duration1.asMinutes();

          var duration2 = moment.duration(moment(SecondDay2).diff(moment(SecondDay1)));
          var SecondDayMinutes = duration2.asMinutes();

          console.log(FirstDayMinutes);
          console.log(SecondDayMinutes);

          if(FirstDayMinutes > SecondDayMinutes && FirstDayMinutes != SecondDayMinutes){
            var checkEndDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = true;
          }else{
            var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
            IsEntitlemntBasedOnStartDate = false;
          }

        }else{
          var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
          IsEntitlemntBasedOnStartDate = false;
        }

        console.log(checkEndDate);

        // this.state["AddTimeOut"]
        //var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
        //var checkEndDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD')
        var start_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        var end_date = moment(TimeSheet_PeriodViews[i].timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format('YYYY-MM-DD');
        //var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date);
        var check_is_exists = moment(checkEndDate).isBetween(start_date, end_date, null, '[]');
       console.log('start_date => '+ start_date + ' end_date => '+ end_date + ' checkEndDate => '+ checkEndDate);
       console.log('time sheet check end date');
       console.log(check_is_exists);
        if(check_is_exists == true || end_date == checkEndDate)
        {
          timeSheetStartDate = start_date;
          timeSheetEndDate = end_date;
          timeSheetContactMasterId = TimeSheet_PeriodViews[i].timeSheetContactMasterId;
          timeSheetPeriodId = TimeSheet_PeriodViews[i].timeSheetPeriodId;
          dueDate = TimeSheet_PeriodViews[i].dueDate;
        }
      }
    }

    console.log('timeSheetPeriodId =>'+timeSheetPeriodId);

    //return false;
    
    // if (this.state["EditDate"] =='') {
    //   step1Errors["EditDate"] = "Date is mandatory";
    // }

 
    if (this.state["EditTimeIn"] == '') {
      step1Errors["EditTimeIn"] = "TimeIn is mandatory";
    }

    if (this.state["EditTimeOut"] == '') {
      step1Errors["EditTimeOut"] = "TimeOut is mandatory";
    }

    var EditTimeInDateTemp = moment(this.state["EditTimeIn"]).format("YYYY-MM-DD");
    var EditTimeOutDateTemp = moment(this.state["EditTimeOut"]).format("YYYY-MM-DD");
    if(EditTimeInDateTemp == this.state.CurrentDate_Min && EditTimeOutDateTemp == this.state.CurrentDate_Min){
      step1Errors["EditTimeOut"] = "Time IN and Time OUT values should not belong to Previous pay period dates.";
    }

    if (this.state["Editlocation"] == '') {
      step1Errors["Editlocation"] = "Location is mandatory";
    }

    if (this.state["EditService"] == '') {
      step1Errors["EditService"] = "Service is mandatory";
    }

    if(this.state.IsNoOfHoursvalidEdit == false){
      step1Errors["EditNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
    }

    if(timeSheetPeriodId == "")
    {
      SystemHelpers.ToastError("Please contact system administrator");
      return false;
    }


    //  console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    // find CategoryId, SubCategoryId, OperationType
    var CategoryId = '';
    var SubCategoryId = '';
    var OperationType = '';
    var isStatHolidayRuleApplicable = '';
    var HolidayType = null;
    var statHolidayCategoryId = null;
    var statHolidaySubCategoryReasonId = null;
    var statHoursToConsider = null;
    var hoursToConsider = null;
    var isStatBankHous = null;
    var isStatRRSP = null;
    var isStatPayout = null;
    var statEntitlementHours = null;


    var IsEditInDateHoliday = this.state.IsEditInDateHoliday;
    var IsEditOutDateHoliday = this.state.IsEditOutDateHoliday;
    var HolidayType = SystemHelpers.GetHolidayType(IsEditInDateHoliday,IsEditOutDateHoliday,this.state.EditService);


    var serviceView= this.state.serviceView;
    for (var z = 0; z < serviceView.length; z++)
    {
      if(serviceView[z].serviceId == this.state["EditService"])
      {
        //if(this.state.EditPreferenceService == process.env.API_BANK_HOURS){
          OperationType = serviceView[z].operationType;
        //}

        CategoryId = serviceView[z].categoryId;
        SubCategoryId = serviceView[z].subCategoryId;
        isStatHolidayRuleApplicable = serviceView[z].isStatHolidayRuleApplicable;
        statHolidayCategoryId = serviceView[z].statHolidayCategoryId;
        statHolidaySubCategoryReasonId = serviceView[z].statHolidaySubCategoryReasonId;
        statHoursToConsider = serviceView[z].statHoursToConsider;
        hoursToConsider = serviceView[z].hoursToConsider;
        isStatBankHous = serviceView[z].isStatBankHous;
        isStatRRSP = serviceView[z].isStatRRSP;
        isStatPayout = serviceView[z].isStatPayout;
        //OperationType = serviceView[z].operationType;
      }
      
    }


    // find CategoryId, SubCategoryId, OperationType

    

    this.showLoader();
    // var EditTimeIn=moment(this.state["EditDate"]+"T"+this.state["EditTimeIn"]).format('hh:mm A');
    // var EditTimeOut=moment(this.state["EditDate"]+"T"+this.state["EditTimeOut"]).format('hh:mm A');
    
    var EditTimeIn=moment(this.state["EditTimeIn"]).format('YYYY-MM-DD hh:mm A');
    var EditTimeOut=moment(this.state["EditTimeOut"]).format('YYYY-MM-DD hh:mm A');

    var isAmendment = false;
    if(this.state.isTimeSheetSubmitted == true || this.state.isTimeSheetApproved == true || this.state.isTimeSheetSubmitedToPayroll == true){
      isAmendment = true;
    }else{
      isAmendment = false;
    }

    let currentDate = moment();
    //let currentDate = moment('2021-08-15');
    let DueDate = moment(dueDate);

    var CheckisAmendment = DueDate.diff(currentDate, 'days');

    if(CheckisAmendment < 0){
      isAmendment = true;
    }


    var ArrayJson = [];
    var EditTimeInDate = moment(checkEndDate,'YYYY-MM-DD').format("MM-DD-YYYY");

    if(  (this.state.IsEditInDateHoliday == true || this.state.IsEditOutDateHoliday == true)){

      var istest = false;
      var checkStartDate = moment(this.state["EditTimeIn"]).format('YYYY-MM-DD');
      var checkEndDate = moment(this.state["EditTimeOut"]).format('YYYY-MM-DD');
      var IsEditInDateHoliday = this.state.IsEditInDateHoliday;
      var IsEditOutDateHoliday = this.state.IsEditOutDateHoliday;
      var startdatetime = this.state["EditTimeIn"];
      var enddatetime = this.state["EditTimeOut"];

      var GetHolidayHours = SystemHelpers.GetHolidayHours(IsEditInDateHoliday,IsEditOutDateHoliday,startdatetime,enddatetime);
      console.log(GetHolidayHours);  

      var statHours = '';
      var normalHours = '';

      if(isStatHolidayRuleApplicable == false){
        var statHours = "00:00";
        var normalHours = this.state["EditNoOfHours"];
      }else{
        var statHours = GetHolidayHours.statHours;
        var normalHours = GetHolidayHours.normalHours;
      } 

      console.log('isStatHolidayRuleApplicable 1 => '+isStatHolidayRuleApplicable); 
      console.log('statHours => '+statHours); 
      console.log('normalHours => '+normalHours);

      var ArrayJson = [{
            serviceId: this.state["EditService"],
            timeSheetDate: EditTimeInDate,
            PayTimeOffId: this.state["EditPreferenceService"],
            timeIN: EditTimeIn,
            timeOUT: EditTimeOut,
            timeLocationId: this.state["Editlocation"],
            note: this.state["EditNotes"],
            noOfHours: this.state["EditNoOfHours"],
            timeSheetTransactionId:this.state.timeSheetTransactionId,
            isAmendmends:isAmendment,
            recordType:this.state.recordType,
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate,
            isStatHolidayRuleApplicable : isStatHolidayRuleApplicable,
            HolidayType : HolidayType,
            statsHoursType : HolidayType,
            statHours : statHours,
            normalHours : normalHours,
            statHolidayCategoryId : statHolidayCategoryId,
            statHolidaySubCategoryReasonId : statHolidaySubCategoryReasonId,
            statHoursToConsider : statHoursToConsider,
            hoursToConsider : hoursToConsider,
            isStatBankHous : isStatBankHous,
            isStatRRSP : isStatRRSP,
            isStatPayout : isStatPayout,
            holidayType : isStatPayout
      }];
    }else{

      console.log('statHours => 00:00'); 
      console.log('normalHours => '+this.state["EditNoOfHours"]);
      var ArrayJson = [{
            serviceId: this.state["EditService"],
            timeSheetDate: EditTimeInDate,
            PayTimeOffId: this.state["EditPreferenceService"],
            timeIN: EditTimeIn,
            timeOUT: EditTimeOut,
            timeLocationId: this.state["Editlocation"],
            note: this.state["EditNotes"],
            noOfHours: this.state["EditNoOfHours"],
            timeSheetTransactionId:this.state.timeSheetTransactionId,
            isAmendmends:isAmendment,
            recordType:this.state.recordType,
            WantToDisplayInTimesheet:true,
            categoryId:CategoryId,
            subCategoryId:SubCategoryId,
            operationType:OperationType,
            IsEntitlemntBasedOnStartDate : IsEntitlemntBasedOnStartDate,
            isStatHolidayRuleApplicable : isStatHolidayRuleApplicable,
            HolidayType : HolidayType,
            statsHoursType : HolidayType,
            statHours : "00:00",
            normalHours : this.state["EditNoOfHours"],
            statHolidayCategoryId : statHolidayCategoryId,
            statHolidaySubCategoryReasonId : statHolidaySubCategoryReasonId,
            statHoursToConsider : statHoursToConsider,
            hoursToConsider : hoursToConsider,
            isStatBankHous : isStatBankHous,
            isStatRRSP : isStatRRSP,
            isStatPayout : isStatPayout,
            holidayType : isStatPayout
      }];
    }

    


    var IsOnCall = false;
    if(this.state.EditService == process.env.API_ON_CALL){
      var IsOnCall = true;
    }
     
    let bodyarray = {};
    
    bodyarray["timeSheetContactMasterId"] = timeSheetContactMasterId;
    bodyarray["isTimeSheetSubmitted"] = false;
    bodyarray["TimeSheetPeriodId"] = timeSheetPeriodId;
    bodyarray["timeSheetStartDate"] = timeSheetStartDate;
    bodyarray["timeSheetEndDate"] = timeSheetEndDate;
    bodyarray["timeSheetContactId"] = this.state.staffContactID;
    bodyarray["timeSheetCreateBy"] = this.state.staffContactID;
    bodyarray["IsOnCall"] = IsOnCall;
    bodyarray["UpdateByName"] = this.state.fullName;
    bodyarray["timeSheetConatctSheetViews"] = ArrayJson;

    //  console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'InsertUpdateUserTimeSheetData';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson InsertUpdateUserTimeSheetData");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');

            
            //this.setState({ AllSubCategory : [] });
            //AllSubCategory userSkillCategories
            
            this.setState({ EditNotes : '' });

            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            //this.GetUserWiseTimeSheetData();  
            //this.ChangePeriod();  
            //this.GetReportListGridReload();
            this.GetReportListGrid(this.state.currentPage,this.state.pageSize,this.state.searchText);
        }
        else{
            if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.responseMessge);
              }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('UpdateRecord error');
      console.log(error);
      this.props.history.push("/error-500");
    });
    return false;
  }

  EditRecord = (record) => e => {

    console.log(record);
    this.setState({ timeSheetTransactionId: record.timeSheetTransactionId });
    this.setState({ EditPreference: [] });
    this.setState({ AddPreference: [] });

    this.setState({ EditDate: moment(record.timeSheetDate).format('YYYY-MM-DD') });
    this.setState({ EditTimeIn: moment(record.timeIN) });
    this.setState({ EditTimeOut:  moment(record.timeOUT) });

    this.setState({ recordType:  record.recordType });
    this.setState({ isAmendmends:  record.isAmendmends });
    
    //this.setState({ EditTimeOut_Min: moment(moment(record.timeIN).subtract(1, 'day').format('YYYY/MM/DD HH:mm A'))});
    //this.setState({ EditTimeOut_Max: moment(moment(record.timeIN).add(1, 'day').format('YYYY/MM/DD HH:mm A'))});

    this.setState({ EditTimeOut_Min: moment(record.timeIN).subtract(1, 'day')});
    this.setState({ EditTimeOut_Max: moment(record.timeIN).add(2, 'day')});

    if(record.serviceId ==  process.env.API_24HOURS_SERVICE ){
      this.setState({ EditTimeOutDisabled: true});
    }else{
      this.setState({ EditTimeOutDisabled: false});
    }

    this.setState({ Editlocation: record.timeLocationId });
    this.setState({ EditService: record.serviceId });
    this.setState({ EditNotes: record.note });
    this.setState({ EditNoOfHours: record.noOfHours });

    //alert(record.payTimeOffId);
    
    this.setState({ EditPreferenceService: record.payTimeOffId });

    if(record.payTimeOffId !='' && record.payTimeOffId != null){
      this.GetPayOffDetailsForUser(record.serviceId,record.timeIN,true,record.timeLocationId,'Edit','In');
      this.GetPayOffDetailsForUser(record.serviceId,record.timeOUT,true,record.timeLocationId,'Edit','Out');
    }

    if(record.serviceId == process.env.API_ON_CALL){
      $('#EditService').prop('disabled', true);
      $('#Editlocation').prop('disabled', true);
      this.setState({ EditTimeInDisabled: true });
      this.setState({ EditTimeOutDisabled: true });
    }else{
      $('#EditService').prop('disabled', false);
      $('#Editlocation').prop('disabled', false);
      this.setState({ EditTimeInDisabled: false });
      this.setState({ EditTimeOutDisabled: false });
    }
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    //if(this.props.human_resource_can_update == true || this.props.human_resource_can_delete == true){
      let Edit_push = [];
      //if(record.wantToDisplayInTimeSheet == true && record.payTimeOffId == '' ){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#CurrentTimeStaff_Edit_modal"><i className="fa fa-pencil m-r-5" /> Edit</a>
        );
      //}
      
      
      // 
      // onClick={this.DeleteInfo(record)}
      let Delete_push = [];
      
      Delete_push.push(
        <a href="#" onClick={this.DeleteInfo(record)}  className="dropdown-item" data-toggle="modal" data-target="#timesheet_Delete_modal"><i className="fa fa-trash-o m-r-5" /> Delete</a>
      );
      
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    //}
    return return_push;
  }

  DeleteRecord = () => e => {
    e.preventDefault();
    this.showLoader();
    console.log("DeletetimesheetTransactionId");
    console.log(this.state.DeletetimesheetTransactionId);
    //return false;
    var url=process.env.API_API_URL+'DeleteUserTimeSheetByTransactionId?timesheetTransactionId='+this.state.DeletetimesheetTransactionId+'&userName='+this.state.fullName;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson DeleteUserTimeSheetByTransactionId");
      //  console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            //this.SearchGridDataFunc();
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
    //  console.log('DeleteUserTimeSheetByTransactionId error');
      console.log('DeleteRecord error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  DeleteInfo = (record) => e =>{
    e.preventDefault();
    console.log(record);

    var isAmendment = false;
    if(this.state.isTimeSheetSubmitted == true){
      isAmendment = true;
    }else{
      isAmendment = false;
    }
    this.setState({ isAmedmends: isAmendment });
    
    var temparray = '';

    var ListGrid = this.state.ListGridPastDetails;

    for (var z = 0; z < ListGrid.length; z++)
    {
      if(ListGrid[z].tempGuid == record.tempGuid)
      {
        temparray=temparray+ListGrid[z].timeSheetTransactionId+",";
      }  
    }
    temparray=temparray.substring(0, temparray.length - 1);
    //this.setState({ DeletetimesheetTransactionId: record.timeSheetTransactionId });
    this.setState({ DeletetimesheetTransactionId: temparray });

    //console.log('temparray');
    //console.log(temparray);
  }

  GetPayOffDetailsForUser(AddService,date,baseservice,Addlocation,calltype,datetype){

    // if(baseservice == false){

       this.setState({ AddPreference: [] });
    //   this.setState({ EditPreference: [] });
    //   return false;
    // }

    //this.setState({ AddPreferenceService : '' });
    //this.setState({ EditPreferenceService : '' });

    if(calltype == 'Add'){
      this.setState({ AddPreference: [] });

      if(datetype == 'In'){
        this.setState({ IsAddInDateHoliday: false });
      }else{
        this.setState({ IsAddOutDateHoliday: false });
      }

    }else{
      this.setState({ EditPreference: [] });

      if(datetype == 'In'){
        this.setState({ IsEditInDateHoliday: false });
      }else{
        this.setState({ IsEditOutDateHoliday: false });
      }

    }
    
    var Servicedate=moment(date).format('MM/DD/YYYY');

    var isPartTimeEmployee = false;
    if(this.state.UserHoursType != "Full-Time"){
      var isPartTimeEmployee = true;
    }
    //console.log(Servicedate);

    this.showLoader2();
    var url=process.env.API_API_URL+'GetPayOffDetailsForUser?serviceCodeId='+AddService+"&inTimeDate="+Servicedate+"&basedOnServiceCode="+baseservice+"&locationId="+Addlocation+"&isPartTimeEmployee="+isPartTimeEmployee;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetPayOffDetailsForUser");
      console.log(data);
      
      // debugger;
      
      if (data.responseType === "1") {
        // Profile & Contact

        if(data != null){
          if(calltype == 'Add' && data.data != null){

            //API_Pay_Out
            var isStatHolidayRuleApplicable = false;
            var serviceView = this.state.serviceView;
            for (var z = 0; z < serviceView.length; z++)
            {
              if(serviceView[z].serviceId == AddService)
              {
                isStatHolidayRuleApplicable = serviceView[z].isStatHolidayRuleApplicable;
              }
            }
            console.log("isStatHolidayRuleApplicable =>"+isStatHolidayRuleApplicable);
            if(isPartTimeEmployee == true && isStatHolidayRuleApplicable == true){
              this.setState({ AddPreferenceService: process.env.API_Pay_Out });
              console.log("AddPreferenceService True");
              console.log(process.env.API_Pay_Out);
            }else if(isPartTimeEmployee == false){
              if(isStatHolidayRuleApplicable == true){
                this.setState({ AddPreference: data.data });
              }

              if(process.env.API_ON_CALL == AddService){
                this.setState({ AddPreference: data.data });
              }
              //this.setState({ AddPreference: data.data });
            }
           
            if(AddService == process.env.API_Bank_Emergency_Shift || AddService == process.env.API_Bank_Staff_Meeting_Only )
            {
              this.setState({ AddPreferenceService: process.env.API_BANK_HOURS });
              $("#AddPreferenceService").prop( "disabled", true );
              $("#AddPreferenceService").css("background","darkgrey");
            }else if(isPartTimeEmployee == false){
              this.setState({ AddPreferenceService: "" });
              $("#AddPreferenceService").prop( "disabled", false );
              $("#AddPreferenceService").css("background","none");
            }

            if(datetype == 'In'){
              this.setState({ IsAddInDateHoliday: true });
            }else{
              this.setState({ IsAddOutDateHoliday: true });
            }
          }else{

            var isStatHolidayRuleApplicable = false;
            var serviceView = this.state.serviceView;
            for (var z = 0; z < serviceView.length; z++)
            {
              if(serviceView[z].serviceId == AddService)
              {
                isStatHolidayRuleApplicable = serviceView[z].isStatHolidayRuleApplicable;
              }
            }

            console.log("isPartTimeEmployee => "+isPartTimeEmployee);
            console.log("isStatHolidayRuleApplicable => "+isStatHolidayRuleApplicable);

            if(isPartTimeEmployee == true && isStatHolidayRuleApplicable == true){
              this.setState({ EditPreferenceService: process.env.API_Pay_Out });
              console.log("EditPreferenceService True");
              console.log(process.env.API_Pay_Out);
            }else if(isPartTimeEmployee == false){
              if(isStatHolidayRuleApplicable == true){
                this.setState({ EditPreference: data.data });
              }

              if(process.env.API_ON_CALL == AddService){
                this.setState({ EditPreference: data.data });
              }
              //this.setState({ EditPreference: data.data });
            }

            

            if(AddService == process.env.API_Bank_Emergency_Shift || AddService == process.env.API_Bank_Staff_Meeting_Only )
            {
              this.setState({ EditPreferenceService: process.env.API_BANK_HOURS });
              $("#EditPreferenceService").prop( "disabled", true );
              $("#EditPreferenceService").css("background","darkgrey");
            }else if(isPartTimeEmployee == false){
              //this.setState({ EditPreferenceService: "" });
              $("#EditPreferenceService").prop( "disabled", false );
              $("#EditPreferenceService").css("background","none");
            }

            if(datetype == 'In'){
                this.setState({ IsEditInDateHoliday: true });
            }else{
                this.setState({ IsEditOutDateHoliday: true });
            }
          }
        }

        
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader2();
    })
    .catch(error => {
      console.log('GetPayOffDetailsForUser error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  Get_Hour(StarTime,EndTime){
    //  console.log('StarTime =>'+StarTime);
    //  console.log('EndTime =>'+EndTime);
    var StarTime =moment(StarTime).format("DD/MM/YYYY HH:mm:ss");
    var EndTime =moment(EndTime).format("DD/MM/YYYY HH:mm:ss");
    //  console.log(StarTime);
    //  console.log(EndTime);

    var ms = moment(EndTime,"DD/MM/YYYY HH:mm:ss").diff(moment(StarTime,"DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var totalHours = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");
    //alert(d.asHours());

      console.log(this.state.AddService);
      console.log(process.env.API_ZERO_HOURS);

    let step1Errors = {};
    if ((d.asHours() < 0 || totalHours == '0:00') && this.state.AddService != process.env.API_ON_CALL ) {
      step1Errors["AddNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
      this.setState({ IsNoOfHoursvalid: false });
    }else if(d.asHours() > 24 && this.state.AddService != process.env.API_ZERO_HOURS && this.state.AddPreference.length == 0){
      step1Errors["AddNoOfHours"] = "you can add max 24 hours.";
      this.setState({ IsNoOfHoursvalid: false });
    }
    else{
      delete this.state.errormsg['AddNoOfHours'];
      this.setState({ IsNoOfHoursvalid: true });
    }

    //console.log(this.state.AddService);
    
    if(this.state.AddService == process.env.API_ZERO_HOURS){
      delete this.state.errormsg['AddNoOfHours'];
      this.setState({ IsNoOfHoursvalid: true });
      totalHours = '00:00';
      let step1Errors = {};
    }

    this.setState({ errormsg: step1Errors });

    //var totalHours = moment.utc(moment(StarTime,"DD/MM/YYYY HH:mm:ss").diff(moment(EndTime,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
    return totalHours;
  }

  Get_Hour_Service(StarTime,EndTime,AddService){
    //  console.log('StarTime =>'+StarTime);
    //  console.log('EndTime =>'+EndTime);
    var StarTime =moment(StarTime).format("DD/MM/YYYY HH:mm:ss");
    var EndTime =moment(EndTime).format("DD/MM/YYYY HH:mm:ss");

    var ms = moment(EndTime,"DD/MM/YYYY HH:mm:ss").diff(moment(StarTime,"DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var totalHours = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");
    //alert(d.asHours());
    console.log("AddService => "+AddService);
    console.log("API_ZERO_HOURS => "+process.env.API_ZERO_HOURS);
    let step1Errors = {};
    if ((d.asHours() < 0 || totalHours == '0:00') && AddService != process.env.API_ZERO_HOURS) {
      step1Errors["AddNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
      this.setState({ IsNoOfHoursvalid: false });
    }else if(d.asHours() > 24 && AddService != process.env.API_ZERO_HOURS){
      step1Errors["AddNoOfHours"] = "you can add max 24 hours.";
      this.setState({ IsNoOfHoursvalid: false });
    }
    else{
      delete this.state.errormsg['AddNoOfHours'];
      this.setState({ IsNoOfHoursvalid: true });
    }

    
    
    if(AddService == process.env.API_ZERO_HOURS){
      delete this.state.errormsg['AddNoOfHours'];
      this.setState({ IsNoOfHoursvalid: true });
      totalHours = '00:00';
      let step1Errors = {};
    }

    this.setState({ errormsg: step1Errors });

    //var totalHours = moment.utc(moment(StarTime,"DD/MM/YYYY HH:mm:ss").diff(moment(EndTime,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
    return totalHours;
  }

  Get_Hour_Edit(StarTime,EndTime){
    //  console.log(StarTime);
    //  console.log(EndTime);
    var StarTime =moment(StarTime).format("DD/MM/YYYY HH:mm:ss");
    var EndTime =moment(EndTime).format("DD/MM/YYYY HH:mm:ss");
    //  console.log(StarTime);
    //  console.log(EndTime);

    var ms = moment(EndTime,"DD/MM/YYYY HH:mm:ss").diff(moment(StarTime,"DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var totalHours = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");
    //alert(d.asHours());

    let step1Errors = {};
    if (d.asHours() < 0 && (this.state.EditService != process.env.API_ON_CALL || this.state.EditService != process.env.API_ZERO_HOURS)) {
      step1Errors["EditNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
      this.setState({ IsNoOfHoursvalidEdit: false });
    }else if(d.asHours() > 24 && (this.state.EditService != process.env.API_ON_CALL || this.state.EditService != process.env.API_ZERO_HOURS)){
      step1Errors["EditNoOfHours"] = "you can add max 24 hours.";
      this.setState({ IsNoOfHoursvalidEdit: false });
    }
    else{
      delete this.state.errormsg['EditNoOfHours'];
      this.setState({ IsNoOfHoursvalidEdit: true });
    }

    if(this.state.EditService == process.env.API_ZERO_HOURS){
      delete this.state.errormsg['EditNoOfHours'];
      this.setState({ IsNoOfHoursvalidEdit: true });
      totalHours = '00:00';
      let step1Errors = {};
    }

    this.setState({ errormsg: step1Errors });

    //var totalHours = moment.utc(moment(StarTime,"DD/MM/YYYY HH:mm:ss").diff(moment(EndTime,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
    return totalHours;
  }

  Get_Hour_Edit_Service(StarTime,EndTime,EditService){
    //  console.log(StarTime);
    //  console.log(EndTime);
    var StarTime =moment(StarTime).format("DD/MM/YYYY HH:mm:ss");
    var EndTime =moment(EndTime).format("DD/MM/YYYY HH:mm:ss");
    //  console.log(StarTime);
    //  console.log(EndTime);

    var ms = moment(EndTime,"DD/MM/YYYY HH:mm:ss").diff(moment(StarTime,"DD/MM/YYYY HH:mm:ss"));
    var d = moment.duration(ms);
    var totalHours = Math.floor(d.asHours()) + moment.utc(ms).format(":mm");
    //alert(d.asHours());

    let step1Errors = {};
    if (d.asHours() < 0 && EditService != process.env.API_ON_CALL && EditService != process.env.API_ZERO_HOURS) {
      step1Errors["EditNoOfHours"] = "Enter Valid TimeIn and TimeOut.";
      this.setState({ IsNoOfHoursvalidEdit: false });
    }else if(d.asHours() > 24 && (EditService != process.env.API_ON_CALL || EditService != process.env.API_ZERO_HOURS)){
      step1Errors["EditNoOfHours"] = "you can add max 24 hours.";
      this.setState({ IsNoOfHoursvalidEdit: false });
    }
    else{
      delete this.state.errormsg['EditNoOfHours'];
      this.setState({ IsNoOfHoursvalidEdit: true });
    }

    if(EditService == process.env.API_ZERO_HOURS){
      delete this.state.errormsg['EditNoOfHours'];
      this.setState({ IsNoOfHoursvalidEdit: true });
      totalHours = '00:00';
      let step1Errors = {};
    }

    this.setState({ errormsg: step1Errors });

    //var totalHours = moment.utc(moment(StarTime,"DD/MM/YYYY HH:mm:ss").diff(moment(EndTime,"DD/MM/YYYY HH:mm:ss"))).format("HH:mm:ss");
    return totalHours;
  }
  // ============================================== Time Sheet ============================================== //

  // ============================================== Notes ============================================== //
  AddNotes_Msg = () => e => {
    //debugger;
    
    e.preventDefault();

    let step1Errors = {};
    
    if(this.state.filePreviewsFinal.length == 0 && this.state["AddMsg"] == ''){

      if (this.state["AddMsg"] == '')
      {
        step1Errors["AddMsg"] = "Notes is mandatory";
      }

      if (this.state.filePreviewsFinal.length == 0)
      {
        step1Errors["Addattachment"] = "Please Select Attachment File.";
      }
        
    }

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
      return false;
    }

    var isFileAttached = false;
    let File_Push = [];
    if(this.state.filePreviewsFinal.length > 0)
    {
      // var file = this.state.filePreviewsFinal[0].FileData;
      // var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
      // var base64result = file.split(',')[1];
      var isFileAttached = true;
      
      var FileUpload = this.state.filePreviewsFinal;
      for (var z = 0; z < FileUpload.length; z++)
      {
        var file = FileUpload[z].FileData;
        var fileType = file.match(/[^:]\w+\/[\w-+\d.]+(?=;|,)/)[0];
        var base64result = file.split(',')[1];

        let File = {};
        File.FileType = fileType;
        File.Base64String = base64result;
        File.DocumentType = 'Timesheet Notes';
        File.documentName = FileUpload[z].FileName;
        File.documentId = null;
        File.status = true;
        File_Push.push(File);
      }
    }

    if(this.state.filePreviewsFinalEdit.length > 0){
      var isFileAttached = true;
      
      var FileUploadEdit = this.state.filePreviewsFinalEdit;

      for (var j = 0; j < FileUploadEdit.length; j++)
      {
        if(FileUploadEdit[j].status == false){

          let File = {};
          File.base64String = FileUploadEdit[j].base64String;
          File.documentId = FileUploadEdit[j].documentId;
          File.documentName = FileUploadEdit[j].documentName;
          File.documentType = FileUploadEdit[j].documentType;
          File.fileType = FileUploadEdit[j].fileType;
          File.status = FileUploadEdit[j].status;
          File_Push.push(File);

        }
        
      }

    }

    console.log(File_Push);

    //return false;
    this.showLoader();
    console.log('templocationId => '+ this.state.templocationId);
    
    
    let bodyarray = {
      noteId: this.state["EditNoteId"],
      note: this.state["AddMsg"],
      locationId: this.state.templocationId,
      isFileAttached: isFileAttached,
      allowUserToSeeNotes: this.state.allow_emp,
      //fileType: fileType,
      timesheetcontactmasterId: this.state.EdittimeSheetContactMasterId,
      contactId: this.state.staffContactID,
      TimeSheetNotesFile: File_Push
      //base64String: base64result
    };
     
    // let bodyarray = {};
    // bodyarray["contactId"] = this.state.staffContactID;
    // bodyarray["documentView"] = ArrayJson;

    //  console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUpdateTimesheetNoteDetails';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson CreateUpdateTimesheetNoteDetails");
      //  console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            this.setState({ AddMsg: '' });
            //alert();
            this.setState({ errormsg: '' });
            this.setState({ allow_emp : false });
            this.setState({ EditNoteId: '' });

            this.setState({ filePreviewsFinalEdit: [] });
            this.setState({ filePreviewsFinalEditActive: 0 });

            $( ".removeDocumentFunc" ).trigger( "click" );
            //this.setState({ AddNotesResetflag : true });
            //setTimeout(this.setState({ AddNotesResetflag : false }), 1000);
            this.GetTimeSheetNotesLoad(this.state.EdittimeSheetContactMasterId,this.state.templocationId,);
            //SystemHelpers.ToastSuccess(data.responseMessge);
            //$( ".close" ).trigger( "click" );
            //this.GetUserDocuments();    
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('CreateUpdateTimesheetNoteDetails error');
      console.log(error);
      this.props.history.push("/error-500");
    });
    return false;
  }

  GetImageAllNew(baseurl){
      var imgbase=baseurl;
      var gettype=this.base64MimeType(baseurl);

      let Image_return = [];
      let Image_return_Final = [];

      if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
          var image_var = <img src={baseurl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/pdf'){
          var image_var = <img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.ms-excel'){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
          var image_var = <img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } 

      Image_return_Final.push(
          <div className="col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
              <div className="card card-file">
                {/*<div className="dropdown-file">
                  <a href="" className="dropdown-link" data-toggle="dropdown"><i className="fa fa-ellipsis-v" /></a>
                  <div className="dropdown-menu dropdown-menu-right">
                    <a href="#" onClick={this.NewTabOpen(baseurl)} className="dropdown-item">Download</a>
                    <a href="#" className="dropdown-item">Delete</a>
                  </div>
                </div>*/}
                <div className="card-file-thumb">
                  {image_var}
                </div>
              </div>
          </div>
      );



      return Image_return_Final;
  }

  GetImageAllNewImg(ImgDetails){
      var FileNm = ImgDetails.documentName;
      var FileUrl = ImgDetails.base64String;
      var gettype = FileNm.substr( (FileNm.lastIndexOf('.') +1));

      console.log(gettype);
      // var imgbase=baseurl;
      // var gettype=this.base64MimeType(baseurl);

      let Image_return = [];
      let Image_return_Final = [];

      if(gettype == 'png' || gettype == 'jpeg' || gettype == 'jpg' ){
          var image_var = <img src={FileUrl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'pdf'){
          var image_var = <img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } else if(gettype == 'csv' || gettype == 'xlsx' || gettype == 'xls' ){
          var image_var = <img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      }  else if(gettype == 'docx' || gettype == 'doc'){
          var image_var = <img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />;
      } 

      if(ImgDetails.status == true){
        Image_return_Final.push(
            <div className="col-6 col-sm-4 col-md-3 col-lg-4 col-xl-3">
                <div className="card card-file">
                  <div className="dropdown-file">
                    <a href="" className="dropdown-link" data-toggle="dropdown"><i className="fa fa-ellipsis-v" /></a>
                    <div className="dropdown-menu dropdown-menu-right">
                      <a href={FileUrl} target="_blank" className="dropdown-item">Download</a>
                      <a href="#" onClick={this.DeleteImg(ImgDetails.documentId)} className="dropdown-item">Delete</a>
                    </div>
                  </div>
                  <div className="card-file-thumb">
                    {image_var}
                  </div>
                </div>
            </div>
        );
      }



      return Image_return_Final;
  }

  EditMsg = (MsgDetails) => e => {
    e.preventDefault();
    this.setState({ filePreviewsFinalEdit: [] });
    this.setState({ filePreviewsFinalEditActive: 0 });
    console.log(MsgDetails);
    this.setState({ allow_emp: MsgDetails.allowUserToSeeNotes });
    this.setState({ EditNoteId: MsgDetails.noteId });
    this.setState({ AddMsg: MsgDetails.note });

    if(MsgDetails.timeSheetNotesFile != null){
      console.log('timeSheetNotesFile');

      let temp = [];
      var FileList = MsgDetails.timeSheetNotesFile;
      for (var z = 0; z < FileList.length; z++)
      {
        temp.push({
          base64String: FileList[z].base64String,
          documentId: FileList[z].documentId,
          documentName: FileList[z].documentName,
          documentType: FileList[z].documentType,
          fileType: FileList[z].fileType,
          status: true
        });
      }

      this.setState({ filePreviewsFinalEditActive: FileList.length });
      this.setState({ filePreviewsFinalEdit: temp });
    }
  }

  DeleteImg = (documentId) => e => {
      e.preventDefault();
      console.log(documentId);
      if(confirm("Are you sure you want to delete this?"))
      {
        let temp = [];
        var FileList = this.state.filePreviewsFinalEdit;
        var active = 0;
        for (var z = 0; z < FileList.length; z++)
        {
          if(FileList[z].documentId == documentId){
            temp.push({
              base64String: FileList[z].base64String,
              documentId: FileList[z].documentId,
              documentName: FileList[z].documentName,
              documentType: FileList[z].documentType,
              fileType: FileList[z].fileType,
              status: false
            });
          }else{
            temp.push({
              base64String: FileList[z].base64String,
              documentId: FileList[z].documentId,
              documentName: FileList[z].documentName,
              documentType: FileList[z].documentType,
              fileType: FileList[z].fileType,
              status: FileList[z].status
            });

            if(FileList[z].status){
              active++;
            }
          }

        }

        this.setState({ filePreviewsFinalEditActive: active });
        this.setState({ filePreviewsFinalEdit: temp });
        return false;

      }else{
        return false;
      }
  }

  NewTabOpen = (baseurl) => e => {
      e.preventDefault();
      console.log(baseurl);
      
      const linkSource = baseurl;
      const downloadLink = document.createElement("a");
  

      downloadLink.href = linkSource;
      downloadLink.download = this.GetFileName(baseurl);
      downloadLink.click();
      return false;
  }

  GetImageAll(baseurl){
      var imgbase=baseurl;
      var gettype=this.base64MimeType(baseurl);

      let Image_return = [];

      if(gettype == 'image/png' || gettype == 'image/jpeg' || gettype == 'image/jpg' ){
          Image_return.push(<img src={baseurl} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/pdf'){
          Image_return.push(<img src={PdfImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.ms-excel'){
          Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
          Image_return.push(<img src={ExcelImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      } else if(gettype == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || gettype == 'application/msword'){
          Image_return.push(<img src={DocxImg} alt={'image preview'} style={{ width: 50, height:50, margin: 10 }} />);
      }

      return Image_return;
  }

  base64MimeType(encoded) {
    var result = null;

    if (typeof encoded !== 'string') {
      return result;
    }

    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
      result = mime[1];
    }

    return result;
  }

  GetTimeSheetNotesLoad(timesheetcontactmasterId,locationGuid){
    //this.setState({ ListNotes: [] });
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //let canDelete = getrole.documents_can.documents_can_delete;
    let canDelete = false;
    /* Role Management */

    this.showLoader();
    var url=process.env.API_API_URL+'GetTimesheetNotesDetails?timesheetcontactmasterId='+timesheetcontactmasterId+"&locationId="+locationGuid;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson GetTimesheetNotesDetails");
      //  console.log(data);
        //console.log(data.data.userRole);
        
        if (data.responseType === "1") {
            // Profile & Contact
            this.setState({ ListNotes: data.data });
            $(".chat-wrap-inner").stop().animate({ scrollTop: $(".chat-wrap-inner")[0].scrollHeight}, 1000);
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetTimesheetNotesDetails error 2');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  GetTimeSheetNotes(timesheetcontactmasterId,locationGuid){
    this.setState({ ListNotes: [] });
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //let canDelete = getrole.documents_can.documents_can_delete;
    let canDelete = false;
    /* Role Management */
    this.setState({ GetChatListLoad: false });

    this.showLoader();
    var url=process.env.API_API_URL+'GetTimesheetNotesDetails?timesheetcontactmasterId='+timesheetcontactmasterId+"&locationId="+locationGuid;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson GetTimesheetNotesDetails");
      //  console.log(data);
        //console.log(data.data.userRole);
        this.setState({ GetChatListLoad: true });
        if (data.responseType === "1") {
            // Profile & Contact
            this.setState({ ListNotes: data.data });

             $(".chat-wrap-inner").stop().animate({ scrollTop: $(".chat-wrap-inner")[0].scrollHeight}, 1000);
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetTimesheetNotesDetails error 1');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  GetChatList(){

    //console.log('ListGridPastDetailsCount => '+this.state.ListGridPastDetailsCount);


    let final_list_push =[];
    let list_push =[];
    var list = this.state.ListNotes;
    for (var i = 0; i<list.length; i++) {

      let file_type_push =[];

      var isFileAttached = list[i].isFileAttached;
      var timeSheetNotesFile = list[i].timeSheetNotesFile;

      let att_img_push =[];
      let att_doc_push =[];

      if(isFileAttached == true){
        for (var j = 0; j<timeSheetNotesFile.length; j++) {
            var filetype=this.openFile(timeSheetNotesFile[j].base64String);

            if(filetype == 'jpg'){
              att_img_push.push(
                <a className="chat-img-attach" href={timeSheetNotesFile[j].base64String} target="_blank">
                  <img width={182} height={137} alt="" src={timeSheetNotesFile[j].base64String} />
                  <div className="chat-placeholder">
                    {/*<div className="chat-img-name">placeholder.jpg</div>
                    <div className="chat-file-desc">Download</div>*/}
                  </div>
                </a>
              );
            }else if(filetype == 'xls' || filetype == 'doc' || filetype == 'pdf' ){
                var filenm="Notes."+filetype;
                att_doc_push.push(
                  <ul class="attach-list">
                    <li><i class="fa fa-file"></i> <a href={timeSheetNotesFile[j].base64String} target="_blank" download="Notes" >{this.GetFileName(timeSheetNotesFile[j].base64String)}</a></li>
                  </ul>
                );
            }
            
        }

        file_type_push.push(
          <div className="chat-img-group clearfix">
            <p>Uploaded Files</p>
            {att_img_push}
          </div>
        );

      }

      

      
      

      let is_delete_push =[];
      let close_btn = [];
      let edit_btn = [];
      if(list[i].contactId == this.state.staffContactID){
        if(this.state.EditNoteId != '' && this.state.EditNoteId == list[i].noteId){
          close_btn.push(
            <li><a href="#" className="del-msg" onClick={this.ClearRecord()}><i className="fa fa-close" /></a></li>
          );
        }else{
          edit_btn.push(
            <li><a href="#" className="edit-msg" onClick={this.EditMsg(list[i])}><i className="fa fa-pencil" /></a></li>
          );
        }
        is_delete_push.push(
          <ul>
            {edit_btn}
            <li><a href="#" className="del-msg" onClick={this.DeleteMsg(list[i])}><i className="fa fa-trash-o" /></a></li>
            {close_btn}
          </ul>
        );
      }
      
      let msg_push =[];
      if((list[i].note != '' && list[i].note != null) && (isFileAttached == true && isFileAttached != null)){
        msg_push.push(
          <div className="chat-bubble 1">
            <div className="chat-content img-content">
              <span class="task-chat-user">{list[i].contactName}</span>
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              <p>{list[i].note}</p>
              <br/>
              {file_type_push}
              {att_doc_push}
            </div>
            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }else if((list[i].note != '' && list[i].note != null) && (isFileAttached == '' || isFileAttached == null)){
        msg_push.push(
          <div className="chat-bubble 2">
            <div class="chat-content">
              <span class="task-chat-user">{list[i].contactName}</span> 
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              <p>{list[i].note}</p>
            </div>

            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }else if((isFileAttached == true && isFileAttached != null) && (list[i].note == '' || list[i].note == null) ){
        msg_push.push(
          <div className="chat-bubble 3">
            <div className="chat-content img-content">
              <span class="task-chat-user">{list[i].contactName}</span>
              <span class="chat-time chat-time-pk">{moment(list[i].createdOn).format(process.env.DATETIME_FORMAT)}</span>
              
              {file_type_push}
            </div>
            <div className="chat-action-btns">
              {is_delete_push}
            </div>
          </div>
        );
      }

      list_push.push(
        <div className="chat chat-left">
          <div className="chat-avatar">
            <a href="/blue/app/profile/employee-profile" className="avatar">
              <img alt="" src={list[i].profileLink} />
            </a>
          </div>
          <div className="chat-body">
            {msg_push}
          </div>
        </div>
      );
    }

    final_list_push.push(
      <div className="chats">
      {list_push}
      </div>
    );
    

    return final_list_push;
  }

  DeleteMsg = (MsgDetails) => e => {
      e.preventDefault();

      var isdelete = '';
      if(MsgDetails.isDelete== true)
      {
        isdelete = false;
      }
      else
      {
        isdelete = true;
      }

      if(confirm("Are you sure you want to delete this?"))
      {
          this.showLoader();
          var url=process.env.API_API_URL+'DeleteTimesheetNotes?noteId='+MsgDetails.noteId+'&isDelete='+isdelete;
          fetch(url, {
            method: 'PUT',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'bearer '+localStorage.getItem("token")
            },
            //body: JSON.stringify(bodyarray)
          }).then((response) => response.json())
          .then(data => {
            //  console.log("responseJson DeleteLocation");
            //  console.log(data);
              //console.log(data.data.userRole);
              // debugger;
              if (data.responseType === "1") {
                  // Profile & Contact
                  //SystemHelpers.ToastSuccess(data.responseMessge);
                  //$( ".cancel-btn" ).trigger( "click" );
                  this.GetTimeSheetNotesLoad(this.state.EdittimeSheetContactMasterId,this.state.templocationId);
                  this.hideLoader();
              }else if (data.responseType == "2" || data.responseType == "3") {
                  SystemHelpers.ToastError(data.responseMessge);
                  $( ".cancel-btn" ).trigger( "click" );
                  this.hideLoader();
              }else{
                    if(data.message == 'Authorization has been denied for this request.'){
                      SystemHelpers.SessionOut();
                      this.props.history.push("/login");
                    }else{
                      SystemHelpers.ToastError(data.message);
                    }
                    this.hideLoader();
                    $( ".cancel-btn" ).trigger( "click" );
              }    
          })
          .catch(error => {
            console.log('DeleteTimesheetNotes error');
            console.log(error);
            this.props.history.push("/error-500");
          });
      }
      else{
          return false;
      } 
  }

  GetFileName(file) {
   return file.substr( (file.lastIndexOf('/') +1) );
  }

  openFile(file) {
    var extension = file.substr( (file.lastIndexOf('.') +1) );
    switch(extension) {
      case 'jpg':
      case 'png':
      case 'jpeg':
          return 'jpg';  // There's was a typo in the example where
      break;                         // the alert ended with pdf instead of gif.
      case 'xls':
      case 'xlsx':
      case 'csv':
          return 'xls';
      break;
      case 'docx':
      case 'doc':
          return 'doc';
      break;
      case 'pdf':
          return 'pdf';
      break;
      default:
        return '';
    }
  };

  GetTimeSheetNotes(timesheetcontactmasterId,locationGuid){
    this.setState({ ListNotes: [] });
    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    //let canDelete = getrole.documents_can.documents_can_delete;
    let canDelete = false;
    /* Role Management */
    this.setState({ GetChatListLoad: false });

    this.showLoader();
    var url=process.env.API_API_URL+'GetTimesheetNotesDetails?timesheetcontactmasterId='+timesheetcontactmasterId+"&locationId="+locationGuid;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //  console.log("responseJson GetTimesheetNotesDetails");
      //  console.log(data);
        //console.log(data.data.userRole);
        this.setState({ GetChatListLoad: true });
        if (data.responseType === "1") {
            // Profile & Contact
            this.setState({ ListNotes: data.data });

             $(".chat-wrap-inner").stop().animate({ scrollTop: $(".chat-wrap-inner")[0].scrollHeight}, 1000);
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetTimesheetNotesDetails error 1');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }
  // ============================================== Notes ============================================== //


  // ============================================== PayRoll Adjustmenr ============================================== //
  GetPayrollAdjustmentCodeMaster(){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */

    this.showLoader();
    var url=process.env.API_API_URL+'GetPayrollAdjustmentCodeMaster';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetPayrollAdjustmentCodeMaster");
      console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        this.setState({ PayrollADJAdjustmentCodeList: data.data });
        
      

      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetPayrollAdjustmentCodeMaster error');
      this.props.history.push("/error-500");
    });
  }

  GetUserLocationWise(contactId){
    
    var isPayrollAdmin = false;
    this.setState({ PayrollADJLocationList: [] });
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserLocationWise?contactId='+contactId+"&payRoll="+isPayrollAdmin;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserLocationWise");
        console.log(data);
        if (data.responseType === "1") {
            this.setState({ PayrollADJLocationList: data.data.locationList });
        }else{
          if(data.message == 'Authorization has been denied for this request.'){
            SystemHelpers.SessionOut();
            this.props.history.push("/login");
          }else{
            SystemHelpers.ToastError(data.message);
          }
        }
        this.hideLoader();
        
    })
    .catch(error => {
      console.log('GetUserLocationWise error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }
  
  GetPayrollAdjustmentEntitlementReason(id){
    this.showLoader();
    
    var url=process.env.API_API_URL+'GetPayrollAdjustmentReason?payrollAdjustmentCodeId='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetPayrollAdjustmentReason");
        console.log(data);
        if (data.responseType === "1") {
          this.setState({ PayrollADJEntitlementSubCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetPayrollAdjustmentEntitlementCategory(id){
    this.showLoader();
    
    var url=process.env.API_API_URL+'GetPayrollAdjustmentCategory?payrollAdjustmentReasonId='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetPayrollAdjustmentCategory");
        console.log(data);
        if (data.responseType === "1") {
          
          if(data.data.length > 0){
            this.setState({ PayrollADJEntitlementCategoryList: data.data});
            this.setState({ AddPayrollADJEntCategory: data.data[0].id});
          }
          
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  /*GetEntitlementCategoryView(){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementCategoryView';
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ PayrollADJEntitlementCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  GetEntitlementSubCategoryView(id){
    this.showLoader();
    var url=process.env.API_API_URL+'GetEntitlementSubCategoryView?categoryId='+id;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        //console.log("responseJson GetEntitlementSubCategoryView");
        //console.log(data);
        if (data.responseType === "1") {
          this.setState({ PayrollADJEntitlementSubCategoryList: data.data});
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }*/

  TableHeaderDesignPayRollADJ(){
    var columnsPayRollADJ = [
      {
        label: 'Payroll Adjustment Code',
        field: 'payrollAdjustmentCode',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Location',
        field: 'location',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Date',
        field: 'date',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Description',
        field: 'description',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Hours',
        field: 'hours',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Rate',
        field: 'rate',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Entitlement Category',
        field: 'entitlementCategory',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Reason',
        field: 'entitlementSubCatrgory',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Hours',
        field: 'entitlementhours',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Action',
        field: 'action',
        sort: 'asc',
        width: 150
      }
    ];  

    return columnsPayRollADJ;
  }

  GetEmployeePayrollAdjustment(UsercontactId){
    this.showLoader();

    this.GetUserLocationWise(UsercontactId);

    this.setState({ ListGrid_PayRollADJ: [] });

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = true;
    /* Role Management */
    var currentPage=1;
    var pageSize=5;
    var searchText="";
    var sort_Column="";
    var Sort_Type=false;
    var IsSortingEnabled=true;

    var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;

    //var url=process.env.API_API_URL+'GetEmployeePayrollAdjustment?contactId='+this.state.EditstaffContactID+'&canDelete='+canDelete+url_paging_para;
    //var url=process.env.API_API_URL+'GetEmployeePayrollAdjustment?contactId='+UsercontactId+'&canDelete='+canDelete+url_paging_para;
    var url=process.env.API_API_URL+'GetEmployeePayrollAdjustment?contactId='+UsercontactId+'&canDelete='+canDelete;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetEmployeePayrollAdjustment");
        console.log(data);
        if (data.responseType === "1") {
          this.setState({ header_data_PayRollADJListGrid: this.TableHeaderDesignPayRollADJ() });
          this.setState({ ListGrid_PayRollADJ: this.rowData_PayRollADJ(data.data.payrollAdjustmentPostModel) });

          this.setState({ PayrollADJAdjustmentCodeList: data.data.payrollView });
        }else{
          SystemHelpers.ToastError(data.message);   
        }
        this.hideLoader();
    })
    .catch(error => {
      console.log('GetEmployeePayrollAdjustment error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  rowData_PayRollADJ(ListGridPayRollADJ) {
    console.log('rowData PayRollADJListGrid');
    console.log(ListGridPayRollADJ);

    var ListGridPayRollADJ_length = ListGridPayRollADJ.length;
    let dataArray = [];
    //console.log(ListGridPayRollADJ_length);
    if(ListGridPayRollADJ_length > 0)
    {
      //console.log(ListGridPayRollADJ_length);
      for (var x = 0; x < ListGridPayRollADJ_length; x++)
      {
        //console.log(ListGridPayRollADJ_length);
        //if(ListGridPayRollADJ != null)
        //{ 
            //console.log(ListGridPayRollADJ);
            var tempdataArray = [];

            
              
            tempdataArray.payrollAdjustmentCode = ListGridPayRollADJ[x].payrollAdjustmentCodeName;
            tempdataArray.location = ListGridPayRollADJ[x].locationName;

            //console.log("Date PayRoll");
            //console.log(ListGridPayRollADJ[x].payrollAdjustmentDate);
            if(ListGridPayRollADJ[x].payrollAdjustmentDate !="" && ListGridPayRollADJ[x].payrollAdjustmentDate!=null){
              tempdataArray.date = moment(ListGridPayRollADJ[x].payrollAdjustmentDate,'DD-MM-YYYY').format(process.env.DATE_FORMAT);
            }else{
              tempdataArray.date = "";
            }
            
            tempdataArray.description = ListGridPayRollADJ[x].description;
            tempdataArray.hours = ListGridPayRollADJ[x].noOfHours;
            tempdataArray.rate = ListGridPayRollADJ[x].rate;
            tempdataArray.entitlementCategory = ListGridPayRollADJ[x].entitlementCatrgoryName;
            tempdataArray.entitlementSubCatrgory = ListGridPayRollADJ[x].entitlementSubCatrgoryName;
            
            // Entitlement NoOf Hours
            var EntnoOfHours = (ListGridPayRollADJ[x].noOfHoursEntitlement).split(".")[0];
            var EntnoOfminutes = (ListGridPayRollADJ[x].noOfHoursEntitlement).split(".")[1];
            
            /*console.log('Ent noOfHours');
            console.log(record.noOfHoursEntitlement);
            console.log(EntnoOfHours);
            console.log(EntnoOfminutes);*/

            var Entminutes = "00";
            if(EntnoOfminutes == "25"){
              Entminutes = "15";
            }else if(EntnoOfminutes == "5"){
              Entminutes = "30";
            }else if(EntnoOfminutes == "75"){
              Entminutes = "45";
            }

            if(ListGridPayRollADJ[x].noOfHoursEntitlement !="" && ListGridPayRollADJ[x].noOfHoursEntitlement !=null){
              tempdataArray.entitlementhours = EntnoOfHours+":"+Entminutes;
            }else{
              tempdataArray.entitlementhours = "";
            }
            
            // Entitlement NoOf Hours


            
            
            tempdataArray.action = this.Edit_Update_Btn_Func_PayrollAdj(ListGridPayRollADJ[x]);

            dataArray.push(tempdataArray);
            
            
            
        //}
    
      }
    }
    console.log('Return rowdata array');
    console.log(dataArray);
    return dataArray;
  }

  AddRecordPayrollAdj = () => e => {
      e.preventDefault();
      
      /*console.log(this.state.AddTimeIn_Max);
      console.log(moment(this.state.AddTimeIn_Max).format('MM-DD-YYYY'));
      return false;*/


      let step1Errors = {};
      
      if (this.state["AddPayrollADJadjustmentCode"] == '' || this.state["AddPayrollADJadjustmentCode"] == null) {
        step1Errors["AddPayrollADJadjustmentCode"] = "Adjustment Code is mandatory";
      }

      if (this.state["AddPayrollADJlocation"] == '' || this.state["AddPayrollADJlocation"] == null) {
        step1Errors["AddPayrollADJlocation"] = "Location is mandatory";
      } 
      
      /*if (this.state["AddPayrollADJDate"] == '' || this.state["AddPayrollADJDate"] == null) {
        step1Errors["AddPayrollADJDate"] = "Date is mandatory";
      }*/

      if (this.state["AddPayrollADJQtyhours"] == '' || this.state["AddPayrollADJQtyhours"] == null) {
        step1Errors["AddPayrollADJQtyhours"] = "Hours is mandatory";
      }
      if (this.state["AddPayrollADJQtyminutes"] == '' || this.state["AddPayrollADJQtyminutes"] == null) {
        step1Errors["AddPayrollADJQtyminutes"] = "Minutes is mandatory";
      }
      
      if(this.state["AddPayrollADJadjustmentCode"]==process.env.API_PAYROLL_ADJUSTMENTS_STAT_HOLIDAY_GUID && this.state.AddPayrollADJRateFieldIsShow == true){
        console.log("123=");
        console.log(this.state["AddPayrollADJrate"]);
        if (this.state["AddPayrollADJrate"] == "" || this.state["AddPayrollADJrate"] == null) {
          step1Errors["AddPayrollADJrate"] = "Rate is mandatory";
        }
      }
      

      if (this.state["AddPayrollADJdescription"] == '' || this.state["AddPayrollADJdescription"] == null) {
        step1Errors["AddPayrollADJdescription"] = "Description is mandatory";
      }

      //alert($('#AddPayrollADJsubstractFrom').is(":checked"));
      //if($('#AddPayrollADJsubstractFrom').is(":checked"))
      if(this.state.AddPayrollADJsubstractFrom == true)
      {
        //console.log("enti cat");
        //console.log(this.state["AddPayrollADJEntCategory"]);

        var AddPayrollADJsubstractFrom = true;
        var AddPayrollADJEntQtyhours = this.state["AddPayrollADJEntQtyhours"];
        var AddPayrollADJEntQtyminutes = this.state["AddPayrollADJEntQtyminutes"];
        var AddPayrollADJEntQtydays = this.state["AddPayrollADJEntQtydays"];
        var AddPayrollADJEntQtyshifts = this.state["AddPayrollADJEntQtyshifts"];

        if (this.state["AddPayrollADJEntCategory"] == '' || this.state["AddPayrollADJEntCategory"] == null) {
          step1Errors["AddPayrollADJEntCategory"] = "Entitlement Category is mandatory";
        }

        if (this.state["AddPayrollADJEntSubCategory"] == '' || this.state["AddPayrollADJEntSubCategory"] == null) {
          step1Errors["AddPayrollADJEntSubCategory"] = "Reason is mandatory";
        }

        /*if (this.state["AddPayrollADJEntQtyhours"] == '' || this.state["AddPayrollADJEntQtyhours"] == null) {
          step1Errors["AddPayrollADJEntQtyhours"] = "Hours is mandatory";
        }

        if (this.state["AddPayrollADJEntQtyminutes"] == '' || this.state["AddPayrollADJEntQtyminutes"] == null) {
          step1Errors["AddPayrollADJEntQtyminutes"] = "Minutes is mandatory";
        }*/

        if(this.state["AddPayrollADJEntSubCategory"] == process.env.API_ENTITLEMENT_IDEL_COVID19)
        {
          if (this.state["AddPayrollADJEntQtydays"] == '' || this.state["AddPayrollADJEntQtydays"] == null) {
            step1Errors["AddPayrollADJEntQtydays"] = "Days is mandatory";
          }
        }
        else if(this.state["AddPayrollADJEntSubCategory"] == process.env.API_ENTITLEMENT_REASON_SICK_TIME_GUID && this.state.UserHoursType == "Part-Time")
        {
          if (this.state["AddPayrollADJEntQtyshifts"] == '' || this.state["AddPayrollADJEntQtyshifts"] == null) {
            step1Errors["AddPayrollADJEntQtyshifts"] = "Shift is mandatory";
          }
        }
        else
        {
          if (this.state["AddPayrollADJEntQtyhours"] == '' || this.state["AddPayrollADJEntQtyhours"] == null) {
            step1Errors["AddPayrollADJEntQtyhours"] = "Hours is mandatory";
          }
          if (this.state["AddPayrollADJEntQtyminutes"] == '' || this.state["AddPayrollADJEntQtyminutes"] == null) {
            step1Errors["AddPayrollADJEntQtyminutes"] = "Minutes is mandatory";
          }
        }
      }
      else
      {
        var AddPayrollADJsubstractFrom = false;
        var AddPayrollADJEntQtyhours = "";
        var AddPayrollADJEntQtyminutes = "";
        var AddPayrollADJEntQtydays = "";
        var AddPayrollADJEntQtyshifts = "";
      }

      /*if(this.state.IsViewHistoryContactApplyAddRecord == false)
      {
        if(this.state.AddEmploymentHoursFieldCheckbox == false && this.state.AddLocationFieldCheckbox == false)
        {
          step1Errors["AddFieldErrorMsg"] = "Atleast one option from Applies to section should be selected.";
        }
      }*/

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();

      var add_PayrollADJDate ='';
      //alert(this.state["AddPayrollADJDate"]);
      if (this.state["AddPayrollADJDate"] != '' && this.state["AddPayrollADJDate"] != null)
      {
        var add_PayrollADJDate=moment(this.state["AddPayrollADJDate"]).format('MM-DD-YYYY');
      }
      else
      {
        var add_PayrollADJDate=moment(this.state.AddTimeIn_Max).format('MM-DD-YYYY');
      }
      
      let bodyarray = {};
      bodyarray["contactId"] = this.state.EdittimeSheetContactId;
      bodyarray["loggedInUserId"] = this.state.staffContactID;
      bodyarray["loggedInUserByName"] = this.state.fullName;

      bodyarray["adjustmentCodeId"] = this.state["AddPayrollADJadjustmentCode"];
      bodyarray["locationIdGuid"] = this.state["AddPayrollADJlocation"];
      bodyarray["payrollAdjustmentDate"] = add_PayrollADJDate;
      bodyarray["noOfHours"] = this.state["AddPayrollADJQtyhours"]+':'+this.state["AddPayrollADJQtyminutes"];
      bodyarray["rate"] = this.state["AddPayrollADJrate"];
      bodyarray["description"] = this.state["AddPayrollADJdescription"];
      bodyarray["substractFrom"] = AddPayrollADJsubstractFrom;
      bodyarray["entitlementCategoryId"] = this.state["AddPayrollADJEntCategory"];
      bodyarray["entitlementSubCategoryId"] = this.state["AddPayrollADJEntSubCategory"];

      if(this.state.AddPayrollADJsubstractFrom == true)
      {
        if(this.state["AddPayrollADJEntSubCategory"] == process.env.API_ENTITLEMENT_IDEL_COVID19)
        {
          //console.log("days");
          bodyarray["noOfHoursEntitlement"] = AddPayrollADJEntQtydays+':';
        }
        else if(this.state["AddPayrollADJEntSubCategory"] == process.env.API_ENTITLEMENT_REASON_SICK_TIME_GUID && this.state.UserHoursType == "Part-Time")
        {
          //console.log("shifts");
          bodyarray["noOfHoursEntitlement"] = AddPayrollADJEntQtyshifts+':';
        }
        else
        {
          //console.log("hours");
          bodyarray["noOfHoursEntitlement"] = AddPayrollADJEntQtyhours+':'+AddPayrollADJEntQtyminutes;
        }
      }
      else
      {
        //console.log("hours");
        bodyarray["noOfHoursEntitlement"] = "";
      }  

      
      
      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUpdatePayrollAdjustmentView';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdatePayrollAdjustmentView");
          console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".closePayrollAdj" ).trigger( "click" ); 
            this.ClearRecordPayrollAdj();

            //this.GetReportListGridReload();
            this.GetReportListGrid(this.state.currentPage,this.state.pageSize,this.state.searchText);

            // From Clear
            this.setState({ AddPayrollADJadjustmentCode: "" });
            this.setState({ AddPayrollADJlocation: "" });
            this.setState({ AddPayrollADJDate: "" });
            this.setState({ AddPayrollADJQtyhours: "" });
            this.setState({ AddPayrollADJQtyminutes: "" });
            this.setState({ AddPayrollADJrate: "" });
            this.setState({ AddPayrollADJdescription: "" });

            //$('#AddPayrollADJsubstractFrom').prop('checked', false);
            this.setState({ AddPayrollADJsubstractFrom: false });

            this.setState({ AddPayrollADJEntCategory: "" });
            this.setState({ AddPayrollADJEntSubCategory: "" });
            this.setState({ AddPayrollADJEntQtyhours: "" });
            this.setState({ AddPayrollADJEntQtyminutes: "" });

            this.setState({ AddPayrollADJsubstractFromIsShow: false });
            this.setState({ AddPayrollADJRateFieldIsShow: false });

            
            this.setState({ AddPayrollADJEntQtyhoursDivIsShow: true });
            this.setState({ AddPayrollADJEntQtyminutesDivIsShow: true });

            this.setState({ AddPayrollADJEntQtydaysDivIsShow: false });
            this.setState({ AddPayrollADJEntQtyshiftsDivIsShow: false });
            // From Clear

            this.hideLoader();
            
            //this.SearchGridDataFunc();
            //this.GetUsersEntitlementBalanceView(this.state.currentPage,this.state.pageSize,this.state.searchText);
            //this.GetEmployeePayrollAdjustment(this.state.EdittimeSheetContactId);
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
            SystemHelpers.ToastError(data.responseMessge);  
            this.hideLoader();
          } else{
            SystemHelpers.ToastError(data.message);
            this.hideLoader();
          }
          //this.hideLoader();
          
      })
      .catch(error => {
        console.log('CreateUpdatePayrollAdjustmentView error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  Edit_Update_Btn_Func_PayrollAdj(record){
    //console.log("Edit_Update_Btn_Func_PayrollAdj");
    //console.log(record);
    let return_push = [];

    if(this.state.role_payroll_adjustment_menu_can.payroll_adjustment_menu_can_view == true){
        let Edit_push = [];
        Edit_push.push(
          <a href="#" onClick={this.EditRecord_PayrollAdj(record)}  className="dropdown-item" data-toggle="modal" data-target="#PayrollAdjustments_Edit_modal"><i className="fa fa-eye m-r-5"></i>View</a>
          );
        
        let Delete_push = [];
        /*if(emgContactId.isDelete == false)
        {*/  
          Delete_push.push(
            <a href="#" onClick={this.EditRecord_PayrollAdj(record)} data-toggle="modal" data-target="#delete_PayrollAdjustments" className="dropdown-item"><i className="fa fa-trash-o m-r-5" ></i> Delete</a>
          );
        /*}
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord_PayrollAdj(record)} data-toggle="modal" data-target="#delete_emergency_contact" className="dropdown-item"><i className="fa fa-trash-o m-r-5" ></i> Active</a>
          );
        }*/
        
        
        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {Edit_push}
              {Delete_push}
            </div>
          </div>
        );
      }
      return return_push;
  }

  EditRecord_PayrollAdj = (record) => e => {
    e.preventDefault();
    console.log('PayRollADJ EditRecord');
    console.log(record);
    console.log(record.payrollAdjustmentDate );

    //this.setState({ EditstaffContactID: record['employeeId'] });
    this.setState({ EditPayrollADJId: record.payrollAdjustMentId });
    this.setState({ EditPayrollADJadjustmentCode: record.adjustmentCodeId });
    this.setState({ EditPayrollADJlocation: record.locationIdGuid });

    if(record.payrollAdjustmentDate != "" && record.payrollAdjustmentDate != null){
      this.setState({ EditPayrollADJDate: moment(record.payrollAdjustmentDate,'DD-MM-YYYY').format(process.env.DATE_FORMAT) });
    }else{
      this.setState({ EditPayrollADJDate: "" });
    }

    // Hours & Minutes
    var noOfHours = (record.noOfHours).split(":");
    var hours=noOfHours[0];
    var minutes=noOfHours[1];
    
    this.setState({ EditPayrollADJQtyhours: hours });
    this.setState({ EditPayrollADJQtyminutes: minutes });
    // Hours & Minutes

    if(record.adjustmentCodeId==process.env.API_PAYROLL_ADJUSTMENTS_STAT_HOLIDAY_GUID){
      this.setState({ EditPayrollADJRateFieldIsShow: true });
      this.setState({ EditPayrollADJrate: record.rate });
    }else{
      this.setState({ EditPayrollADJRateFieldIsShow: false });
      this.setState({ EditPayrollADJrate: "" });
    }

    this.setState({ EditPayrollADJdescription: record.description });

    this.setState({ EditPayrollADJsubstractFromIsShow: record.substractFrom });
    this.setState({ EditPayrollADJsubstractFrom: record.substractFrom });
    
    this.setState({ EditPayrollADJEntCategory: record.entitlementCategoryId });
    this.setState({ EditPayrollADJEntSubCategory: record.entitlementSubCategoryId });
    

    // Hours & Minutes
    //var EntnoOfHours = (record.noOfHoursEntitlement).split(":");
    var EntnoOfHours = (record.noOfHoursEntitlement).split(".")[0];
    var EntnoOfminutes = (record.noOfHoursEntitlement).split(".")[1];
    
    
    var Entminutes = "00";
    if(EntnoOfminutes == "25"){
      Entminutes = "15";
    }else if(EntnoOfminutes == "5"){
      Entminutes = "30";
    }else if(EntnoOfminutes == "75"){
      Entminutes = "45";
    }
    
    if(record.entitlementSubCategoryId == process.env.API_ENTITLEMENT_IDEL_COVID19)
    {
      this.setState({ EditPayrollADJEntQtydays: EntnoOfHours });

      this.setState({ EditPayrollADJEntQtyhours: "" });
      this.setState({ EditPayrollADJEntQtyminutes: "" });
      this.setState({ EditPayrollADJEntQtyshifts: "" });      

      this.setState({ EditPayrollADJEntQtydaysDivIsShow: true });

      this.setState({ EditPayrollADJEntQtyhoursDivIsShow: false });
      this.setState({ EditPayrollADJEntQtyminutesDivIsShow: false });
      this.setState({ EditPayrollADJEntQtyshiftsDivIsShow: false });
    }
    else if(record.entitlementSubCategoryId== process.env.API_ENTITLEMENT_REASON_SICK_TIME_GUID && this.state.UserHoursType == "Part-Time")
    {
      this.setState({ EditPayrollADJEntQtyshifts: EntnoOfHours });

      this.setState({ EditPayrollADJEntQtyhours: "" });
      this.setState({ EditPayrollADJEntQtyminutes: "" });
      this.setState({ EditPayrollADJEntQtydays: "" });

      this.setState({ EditPayrollADJEntQtyshiftsDivIsShow: true });

      this.setState({ EditPayrollADJEntQtyhoursDivIsShow: false });
      this.setState({ EditPayrollADJEntQtyminutesDivIsShow: false });
      this.setState({ EditPayrollADJEntQtydaysDivIsShow: false });

    }
    else if(record.entitlementSubCategoryId != process.env.API_ENTITLEMENT_IDEL_COVID19)
    {
      this.setState({ EditPayrollADJEntQtyhours: EntnoOfHours });
      this.setState({ EditPayrollADJEntQtyminutes: Entminutes });

      this.setState({ EditPayrollADJEntQtydays: "" });
      this.setState({ EditPayrollADJEntQtyshifts: "" });

      this.setState({ EditPayrollADJEntQtyhoursDivIsShow: true });
      this.setState({ EditPayrollADJEntQtyminutesDivIsShow: true });

      this.setState({ EditPayrollADJEntQtydaysDivIsShow: false });
      this.setState({ EditPayrollADJEntQtyshiftsDivIsShow: false });

    }
    
    // Hours & Minutes

    this.GetPayrollAdjustmentEntitlementReason(record.adjustmentCodeId);
    this.GetPayrollAdjustmentEntitlementCategory( record.entitlementSubCategoryId);

    

    setTimeout(
      function() {
        // Entitlement Payroll Adjustment
        $("#EditPayrollADJsubstractFrom").prop('disabled', true);

        $("#EditPayrollADJEntCategory").prop('disabled', true);
        $("#EditPayrollADJEntSubCategory").prop('disabled', true);
        $("#EditPayrollADJEntQtyhours").prop('disabled', true);
        $("#EditPayrollADJEntQtyminutes").prop('disabled', true);
        $("#EditPayrollADJEntQtydays").prop('disabled', true);
        $("#EditPayrollADJEntQtyshifts").prop('disabled', true);

        $('#EditPayrollADJEntCategory').css("background-color", "lightgray");
        $('#EditPayrollADJEntSubCategory').css("background-color", "lightgray");
        $('#EditPayrollADJEntQtyhours').css("background-color", "lightgray");
        $('#EditPayrollADJEntQtyminutes').css("background-color", "lightgray");
        $("#EditPayrollADJEntQtydays").css("background-color", "lightgray");
        $("#EditPayrollADJEntQtyshifts").css("background-color", "lightgray");
        // Entitlement Payroll Adjustment

        // Payroll Adjustment
        $("#EditPayrollADJadjustmentCode").prop('disabled', true);
        $("#EditPayrollADJlocation").prop('disabled', true);
        $("#EditPayrollADJDate").prop('disabled', true);
        $("#EditPayrollADJQtyhours").prop('disabled', true);
        $("#EditPayrollADJQtyminutes").prop('disabled', true);
        $("#EditPayrollADJrate").prop('disabled', true);
        $("#EditPayrollADJdescription").prop('disabled', true);

        $('#EditPayrollADJadjustmentCode').css("background-color", "lightgray");
        $('#EditPayrollADJlocation').css("background-color", "lightgray");
        $('#EditPayrollADJDate').css("background-color", "lightgray");
        $('#EditPayrollADJQtyhours').css("background-color", "lightgray");
        $('#EditPayrollADJQtyminutes').css("background-color", "lightgray");
        $('#EditPayrollADJrate').css("background-color", "lightgray");
        $('#EditPayrollADJdescription').css("background-color", "lightgray");
        // Payroll Adjustment

        
      }
      .bind(this),
      3000
    );
    
    //this.GetEntitlementSubCategoryView(record.entitlementCategoryId);
    //this.GetPayrollAdjustmentEntitlementReason(record.entitlementCategoryId);
  }

  UpdateRecordPayrollAdj = () => e => {
      e.preventDefault();

      let step1Errors = {};
      
      if (this.state["EditPayrollADJadjustmentCode"] == '' || this.state["EditPayrollADJadjustmentCode"] == null) {
        step1Errors["EditPayrollADJadjustmentCode"] = "Adjustment Code is mandatory";
      }

      if (this.state["EditPayrollADJlocation"] == '' || this.state["EditPayrollADJlocation"] == null) {
        step1Errors["EditPayrollADJlocation"] = "Location is mandatory";
      } 
      
      if (this.state["EditPayrollADJDate"] == '' || this.state["EditPayrollADJDate"] == null) {
        step1Errors["EditPayrollADJDate"] = "Date is mandatory";
      }

      if (this.state["EditPayrollADJQtyhours"] == '' || this.state["EditPayrollADJQtyhours"] == null) {
        step1Errors["EditPayrollADJQtyhours"] = "Hours is mandatory";
      }
      if (this.state["EditPayrollADJQtyminutes"] == '' || this.state["EditPayrollADJQtyminutes"] == null) {
        step1Errors["EditPayrollADJQtyminutes"] = "Minutes is mandatory";
      }
      
      if (this.state["EditPayrollADJrate"] == '' || this.state["EditPayrollADJrate"] == null) {
        step1Errors["EditPayrollADJrate"] = "Amount is mandatory";
      }

      if (this.state["EditPayrollADJdescription"] == '' || this.state["EditPayrollADJdescription"] == null) {
        step1Errors["EditPayrollADJdescription"] = "Description is mandatory";
      }

      if(this.state.EditPayrollADJsubstractFrom == true)
      {
        var EditPayrollADJsubstractFrom = true;
        var EditPayrollADJEntQtyhours = this.state["EditPayrollADJEntQtyhours"];
        var EditPayrollADJEntQtyminutes = this.state["EditPayrollADJEntQtyminutes"];

        if (this.state["EditPayrollADJEntCategory"] == '' || this.state["EditPayrollADJEntCategory"] == null) {
          step1Errors["EditPayrollADJEntCategory"] = "Entitlement Category is mandatory";
        }

        if (this.state["EditPayrollADJEntSubCategory"] == '' || this.state["EditPayrollADJEntSubCategory"] == null) {
          step1Errors["EditPayrollADJEntSubCategory"] = "Reason is mandatory";
        }

        if (this.state["EditPayrollADJEntQtyhours"] == '' || this.state["EditPayrollADJEntQtyhours"] == null) {
          step1Errors["EditPayrollADJEntQtyhours"] = "Hours is mandatory";
        }

        if (this.state["EditPayrollADJEntQtyminutes"] == '' || this.state["EditPayrollADJEntQtyminutes"] == null) {
          step1Errors["EditPayrollADJEntQtyminutes"] = "Minutes is mandatory";
        }
      }
      else
      {
        var EditPayrollADJsubstractFrom = false;
        var EditPayrollADJEntQtyhours = "";
        var EditPayrollADJEntQtyminutes = "";
      }

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();

      var Edit_PayrollADJDate ='';
      //alert(this.state["EditPayrollADJDate"]);
      if (this.state["EditPayrollADJDate"] != '' && this.state["EditPayrollADJDate"] != null)
      {
        //alert(this.state["EditPayrollADJDate"] + " if");
        var Edit_PayrollADJDate=moment(this.state["EditPayrollADJDate"],'DD-MM-YYYY').format('MM-DD-YYYY');
        //alert(Edit_PayrollADJDate);
      }
      
      let bodyarray = {};
      bodyarray["contactId"] = this.state.EdittimeSheetContactId;
      bodyarray["loggedInUserId"] = this.state.staffContactID;
      bodyarray["loggedInUserByName"] = this.state.fullName;

      bodyarray["payrollAdjustMentId"] = this.state["EditPayrollADJId"];
      bodyarray["adjustmentCodeId"] = this.state["EditPayrollADJadjustmentCode"];
      bodyarray["locationIdGuid"] = this.state["EditPayrollADJlocation"];
      bodyarray["payrollAdjustmentDate"] = Edit_PayrollADJDate;
      bodyarray["noOfHours"] = this.state["EditPayrollADJQtyhours"]+':'+this.state["EditPayrollADJQtyminutes"];
      bodyarray["amount"] = this.state["EditPayrollADJrate"];
      bodyarray["description"] = this.state["EditPayrollADJdescription"];
      bodyarray["substractFrom"] = EditPayrollADJsubstractFrom;
      bodyarray["entitlementCategoryId"] = this.state["EditPayrollADJEntCategory"];
      bodyarray["entitlementSubCategoryId"] = this.state["EditPayrollADJEntSubCategory"];
      bodyarray["noOfHoursEntitlement"] = EditPayrollADJEntQtyhours+':'+EditPayrollADJEntQtyminutes;
      
      console.log(bodyarray);
      //return false;
      var url=process.env.API_API_URL+'CreateUpdatePayrollAdjustment';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdatePayrollAdjustment");
          console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".closePayrollAdj" ).trigger( "click" ); 
            this.ClearRecordPayrollAdj();
            //this.GetUsersEntitlementBalanceView(this.state.currentPage,this.state.pageSize,this.state.searchText);
            //this.GetEmployeePayrollAdjustment(this.state.EdittimeSheetContactId);
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
            SystemHelpers.ToastError(data.responseMessge);  
          } else{
            SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
      })
      .catch(error => {
        console.log('UpdateRecordPayrollAdj error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  DeleteRecordPayrollAdj = () => e => {
      e.preventDefault();

      /*var isdelete = '';
      if(this.state.isDelete== true)
      {
        isdelete = false;
      }
      else
      {
        isdelete = true;
      }*/
      
      var isdelete = true;

      this.showLoader();
      console.log(this.state.EditPayrollADJId);
      var url=process.env.API_API_URL+'DeletePayrollAdjustment?payrollAdjustmentId='+this.state.EditPayrollADJId+'&isDelete='+isdelete+'&userName='+this.state.fullName;
      fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson DeletePayrollAdjustment");
          console.log(data);
          if (data.responseType === "1") {
              SystemHelpers.ToastSuccess(data.responseMessge);
              $( ".cancel-btn" ).trigger( "click" );
              //this.GetHolidays();
              this.hideLoader();
          }else if (data.responseType == "2" || data.responseType == "3") {
              SystemHelpers.ToastError(data.responseMessge);
              $( ".cancel-btn" ).trigger( "click" );
              this.hideLoader();
          }else{
                if(data.message == 'Authorization has been denied for this request.'){
                  SystemHelpers.SessionOut();
                  this.props.history.push("/login");
                }else{
                  SystemHelpers.ToastError(data.message);
                }
                this.hideLoader();
                $( ".cancel-btn" ).trigger( "click" );
          }
          
          
      })
      .catch(error => {
        console.log('DeleteRecordPayrollAdj error');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  ClearRecordPayrollAdj = ()=> e => {
    console.log('ClearRecordPayrollAdj');
    e.preventDefault();

    this.setState({ errormsg: '' });

    this.setState({ AddPayrollADJadjustmentCode: "" });
    this.setState({ AddPayrollADJlocation: "" });
    this.setState({ AddPayrollADJDate: "" });
    this.setState({ AddPayrollADJQtyhours: "" });
    this.setState({ AddPayrollADJQtyminutes: "" });
    this.setState({ AddPayrollADJrate: "" });
    this.setState({ AddPayrollADJdescription: "" });

    //$('#AddPayrollADJsubstractFrom').prop('checked', false);
    this.setState({ AddPayrollADJsubstractFrom: false });

    this.setState({ AddPayrollADJEntCategory: "" });
    this.setState({ AddPayrollADJEntSubCategory: "" });
    this.setState({ AddPayrollADJEntQtyhours: "" });
    this.setState({ AddPayrollADJEntQtyminutes: "" });

    this.setState({ AddPayrollADJsubstractFromIsShow: false });
    this.setState({ AddPayrollADJRateFieldIsShow: false });

    this.setState({ AddPayrollADJEntQtyhoursDivIsShow: true });
    this.setState({ AddPayrollADJEntQtyminutesDivIsShow: true });
    
    this.setState({ AddPayrollADJEntQtydaysDivIsShow: false });
    this.setState({ AddPayrollADJEntQtyshiftsDivIsShow: false });
  }

  ClearaddPayrollAdjDateClearBtn = ()=> e => {
    //console.log('ClearaddPayrollAdjDateClearBtn');
    e.preventDefault();

    this.setState({ AddPayrollADJDate: "" });
  }
  
  DesignRate(){
    console.log("DesignRate");
    let rateOption = [];
    if(this.state.UserHoursType == "Full-Time")
    {
      console.log("if"+this.state.UserHoursType);
      rateOption.push(<option value="">-</option>);
      rateOption.push(<option value="1.5">1.5</option>);
      rateOption.push(<option value="2.5">2.5</option>);
    }
    else
    {
      console.log("else"+this.state.UserHoursType);
      rateOption.push(<option value="">-</option>);
      rateOption.push(<option value="2.5">2.5</option>);
    }
    console.log(rateOption);
    return rateOption;
  }

  // Pagination Design
  PaginationDesign ()
  {
    let PageOutput = [];
    //console.log('pagination');
    //console.log(this.state.pagingData);
    
    if(this.state.pagingData !="" && this.state.pagingData !="undefined")
    {
      var Page_Count = this.state.pagingData.totalPages;
      //alert(this.state.pagingData.currentPage);
      //console.log('page count = ' + Page_Count);
      /* pagination count */


        var Page_Start=1;
        var Page_End=1;

        if(this.state.pagingData.currentPage == 1){
            Page_Start=1;

            if(Page_Count <= 10){
                Page_End=Page_Count;
            }else{
                Page_End=10;
            }
            
        }else{

            if(this.state.pagingData.currentPage < 5){
                Page_Start=1;
                Page_End=Page_Count;
                //Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 1 "+ Page_End);
            }else{
                Page_Start=parseInt(this.state.pagingData.currentPage) - parseInt(4);
                Page_End=parseInt(this.state.pagingData.currentPage) + parseInt(6);
                //console.log("Page_End 2 "+ Page_End);
                if(Page_End > Page_Count){
                    Page_End=Page_Count;
                    //console.log("Page_End 3 "+ Page_End);
                }
            }

        }
      let Page = [];
      var i = 1;
      for (var z=Page_Start; z <= Page_End ; z++)
      {
        if(z==this.state.pagingData.currentPage)
        {
          Page.push(<li className="page-item active pk-active">
            <a className="page-link pk-active" id={z} href="#" onClick={this.PageGetGridData}>{z}<span className="sr-only">(current)</span></a>
          </li>);
        }
        else
        {
          Page.push(<li className="page-item"><a className="page-link" id={z} href="#" onClick={this.PageGetGridData} >{z}</a></li>);
        }
        i++;
      }

      let PagePrev = [];

      if(this.state.pagingData.currentPage == 1){
        PagePrev.push(<li className="page-item disabled">
          <a className="page-link" href="#">Previous</a>
        </li>);
      }else{
        PagePrev.push(<li className="page-item">
          <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)-parseInt(1)} tabIndex={-1} onClick={this.PageGetGridData}>Previous</a>
        </li>);
      }

      let PageNext = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageNext.push(<li className="page-item disabled">
          <a className="page-link" href="#">Next</a>
        </li>);
      }else{
        PageNext.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(this.state.pagingData.currentPage)+parseInt(1)} onClick={this.PageGetGridData}>Next</a>
          </li>
        );
      }

      let PageLast = [];

      if(this.state.pagingData.currentPage == Page_Count){
        PageLast.push(<li className="page-item disabled">
          <a className="page-link" href="#">Last</a>
        </li>);
      }else{
        PageLast.push(
          <li className="page-item">
            <a className="page-link" href="#" id={parseInt(Page_Count)} onClick={this.PageGetGridData}>Last</a>
          </li>
        );
      }


      if(Page_Count > 0){
        PageOutput.push(
          <section className="comp-section" id="comp_pagination">
            <div className="pagination-box">
              <div>
                <ul className="pagination">
                  
                  {PagePrev}
                  {Page}
                  {PageNext}
                  {PageLast}
                  
                </ul>
              </div>
            </div>
          </section>);
      }
      
    }
    
    return PageOutput;
  }

  PageGetGridData = e => {

    e.preventDefault();
    let current_page= e.target.id;
    this.GetReportListGrid(current_page,this.state.pageSize,this.state.searchText)
  }

  SearchGridData = e => {
    this.setState({ pageSize: this.state.TempsearchText });
    this.GetReportListGrid(1,this.state.pageSize,this.state.TempsearchText);
  }
  // Pagination Design
  // ============================================== PayRoll Adjustmenr ============================================== //

  render() {

    const data = {
      columns: this.state.header_data,
      rows: this.state.ListGrid
    };

    const data_PayRollADJ = {
      columns: this.state.header_data_PayRollADJListGrid,
      rows: this.state.ListGrid_PayRollADJ
    };

    const { selectedOptionLocation,selectedOptionEmployees  } = this.state;

    return ( 
      <div className="main-wrapper">
     
        {/* Toast & Loder method use */}
          
        {(this.state.loading) ? <Loader /> : null} 
        {(this.state.loading2) ? <LoaderSecond /> : null} 
        {/* Toast & Loder method use */}
        <Header/>

        <div className="page-wrapper">
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Reports</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">Batch Payroll Reports</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">
                      
                    </div>
                  </div>
                </div>

                {/* Search Filter */}
                  <div className="row filter-row">

                    {/* ==================== search Row 1 ==========================*/}
                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterEmploymentType" value={this.state.FilterEmploymentType} onChange={this.handleChange('FilterEmploymentType')}> 
                          <option value="">-</option>
                          <option value="false">PT</option>
                          <option value="true">FT</option>
                        </select>
                        <label className="focus-label">Report Type</label>
                        <span className="form-text error-font-color">{this.state.errormsg["FilterEmploymentType"]}</span>
                      </div>
                    </div>
                    
                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterPayPeriod" value={this.state.FilterPayPeriod} onChange={this.handleChange('FilterPayPeriod')}> 
                          <option value="" data-payperiodname="" >-</option>
                          {this.state.payPeriodListFilter.map(( listValue, index ) => {
                              var payPeriodName = moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)+ " to " +moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
                              return (
                                <option key={index} value={listValue.timeSheetPeriodId} data-payperiodname={payPeriodName} >{moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)} to {moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Pay period</label>
                      </div>
                    </div>
                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        {/*<select className="form-control floating" id="FilterLocation" value={this.state.FilterLocation} onChange={this.handleChange('FilterLocation')}> 
                          <option value="" data-locationname="">All</option>
                          {this.state.locationListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.locationId} data-locationname={listValue.locationName} >{listValue.locationName}</option>
                              );
                           
                          })}
                        </select>*/}
                        <Select
                          value={selectedOptionLocation}
                          onChange={this.handleChangeSelectLocation}
                          options={this.state.LocationListFilter}
                          defaultValue={""}
                          isSearchable={true}
                          placeholder="Search Location"
                        />
                        {/*<label className="focus-label">Location</label>*/}
                        <span className="form-text error-font-color">{this.state.errormsg["FilterLocation"]}</span>
                      </div>
                    </div>

                    
                    
                    {/*<div className="col-sm-6 col-md-2"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterEmploymentType" value={this.state.FilterEmploymentType} onChange={this.handleChange('FilterEmploymentType')}> 
                          <option value="">All</option>
                          {this.state.employmentTypeListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.name} >{listValue.name}</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Employment Type</label>
                      </div>
                    </div>*/}

                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        {/*<input className="form-control" type="text" id="FilterEmployeeName" value={this.state.FilterEmployeeName}  onChange={this.handleChange('FilterEmployeeName')} />*/}
                        {/*<select className="form-control" id="FilterEmployee" value={this.state.FilterEmployee}  onChange={this.handleChange('FilterEmployee')} >
                          <option value='' data-employeename="" >-</option>
                          {this.state.EmployeeNameListFilter.map(( listValue, index ) => {
                            return (
                              <option key={index}  value={listValue.contactId} data-employeename={listValue.employeeName} >{listValue.employeeName}</option>
                            );
                          })}
                        </select>*/}
                        <Select
                          value={selectedOptionEmployees}
                          onChange={this.handleChangeSelectEmployees}
                          options={this.state.EmployeeNameListFilter}
                          defaultValue={""}
                          isSearchable={true}
                          placeholder="Search Employee"
                        />
                        {/*<label className="focus-label">Employee name</label>*/}
                      </div>
                    </div>
                    <section style={{"padding-top":"10px"}}></section>
                    {/* ==================== search Row 1 ==========================*/}

                    {/* ==================== search Row 2 ==========================*/}
                    
                      
                      <div className="col-sm-6 col-md-3">
                        <div className="form-group form-focus">
                          <label className="focus-label">Sorting</label>
                          <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                            <option value="StaffNumberId">Employee-ID</option>
                            <option value="EmployeeName">Employee Name</option>
                            <option value="LocationCode">Location-ID</option>
                            <option value="LocationName">Location Name</option>
                          </select>
                        </div>
                      </div> 

                      <div className="col-sm-6 col-md-3">
                        <div className="form-group form-focus">
                          <label className="focus-label">Sorting Order</label>
                          <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                            <option value="">-</option>
                            <option value="false">Ascending</option>
                            <option value="true">Descending</option>
                          </select>
                        </div>
                      </div> 

                      <div className="col-sm-6 col-md-3">  
                        <div className="form-group form-focus">
                          <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                          <label className="focus-label">Search</label>
                        </div>
                      </div>

                      <div className="col-lg-3 col-sm-4 col-xs-12">  
                        <div className="form-group form-focus pk-emp-padding-top">
                          <div className="checkbox">
                            <label>
                              <input type="checkbox" name="display_Inactive"  value="true" onChange={this.handleChange('display_Inactive')}  checked={this.state.display_Inactive_flage == "true" ? "true" : null} /> Display Inactive Employees
                            </label>
                          </div>
                        </div>
                      </div>
                    
                    {/* ==================== search Row 2 ==========================*/}

                    {/* ==================== search Row 3 ==========================*/}
                    <div className="col-sm-6 col-md-3"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" value={this.state.pageSize}  onChange={this.handleChange('pageSize')}> 
                          <option value="5">5/Page</option>
                          <option value="10">10/Page</option>
                          <option value="50">50/Page</option>
                          {/*<option value="100">100/Page</option>*/}
                        </select>
                        <label className="focus-label">Per Page</label>
                      </div>
                    </div>

                    {/*<div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="BatchRecord" value={this.state.BatchRecord} onChange={this.handleChange('BatchRecord')}> 
                          <option value="">-</option>
                          {this.state.BatchesList.map(( listValue, index ) => {
                            return (
                              <option key={index}  value={listValue.batchRecord} >{listValue.batchName} Emp: {listValue.batchRecord}</option>
                            );
                          })}
                        </select>
                        <label className="focus-label">Employee-Batch List</label>
                        <span className="form-text error-font-color">{this.state.errormsg["FilterEmploymentType"]}</span>
                      </div>
                    </div>*/}
                    
                    <div className="col-lg-4 col-sm-4 col-xs-12"></div>

                    <div className="col-lg-2 col-sm-12 col-xs-12">  
                      <a href="#" id="FTPTsearchBtn" className="col-lg-12 col-sm-12 col-xs-12 float-right btn btn-success btn-block" onClick={this.SearchGridData}> Search </a>  
                    </div> 
                    {/* ==================== search Row 3 ==========================*/}

                  </div>
                {/* /Search Filter */}    
                {/* /Page Header */}
                

                
                  <div className="row">
                      <div className="col-sm-12"> 
                      {/*<a href="#" className="btn btn-purple mr-1 float-right" onClick={this.ProcessReportData()}> Process Report</a>*/}
                      {this.state.ListGrid.length > 0 ? 
                        <a href="#" className="btn btn-danger mr-1 float-right" onClick={this.ExportReportData()}> Export EXCEL </a>
                      : null
                      }
                      </div>
                  </div>
                  

                {/* FT PT Report Main Table */}
                <div className="row">
                  <div className="col-md-12">
                      <div className="table-responsive">
                      
               
                        
                          
                        <table className="table table-striped custom-table mb-0 datatable">
                          <thead>
                            { this.state.ListGrid.length > 0 ? this.state.header_data : null}
                          </thead>
                          <tbody>
                            
                            {this.state.ListGrid.length > 0 ? this.state.ListGrid : null}
                          </tbody>
                        </table> 

                      {this.PaginationDesign()}

                    </div>
                  </div>
                </div>
                {/* FT PT Report Main Table */}
              </div>
              {/* /Page Content */}

              {/* Add Reports Modal */}
              {/* /Add Reports Modal */}

              {/* Edit Reports Modal */}
              {/* /Edit Reports Modal */}

              {/* Delete Today Work Modal */}
              {/* Delete Today Work Modal */}

              {/* FT PT Main Grid Edit Button View Tab Modals */}
                <div id="View_PayrollAdjustments" data-backdrop="static" className="modal custom-modal fade" role="dialog">
                  <div className="modal-dialog modal-dialog-centered modal-xl" role="document">
                    <div className="modal-content">

                      <div className="modal-header">
                        <h3 className="card-title">Time Sheet Entries</h3>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>

                      <div className="modal-body modal-body-past-details">
                 
                        <div className="card">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-lg-6">
                                  <div className="dash-info-list pk-sheet-margin-bottom">
                                    <a href="#" className="dash-card text-danger">
                                      <div className="dash-card-container">
                                        <div className="dash-card-icon">
                                          <i className="fa fa-calendar" />
                                        </div>
                                        <div className="dash-card-content">
                                          <p>Amendment Due Date</p>
                                        </div>
                                        <div className="dash-card-avatars">
                                          {this.state.past_display_amendment_due_date}
                                        </div>
                                      </div>
                                    </a>
                                  </div>
                              </div>
                            </div>

                            {/*Time Sheet Entries Grid*/}
                            <div className="row">
                              <div className="col-lg-12">
                                {( this.state.IsEditBtn == true ||this.state.isTimeSheetApproved == false || (this.state.isTimeSheetSubmittedToPayroll == false && this.state.isPayrollAdmin == true)) ?
                                  <h3 className="card-title"><a href="#" className="edit-icon" data-toggle="modal" data-target="#CurrentTimeStaff_Add_modal"><i className="fa fa-plus" /></a></h3>
                                : null }
                               
                                <div className="table-responsive">
                                  <table className="table table-nowrap">
                                    <thead>
                                     <tr>
                                        <th>Date</th>
                                        <th>Service Name</th>
                                        <th>Time In</th>
                                        <th>Time Out</th>
                                        <th>Location</th>
                                        <th>Note</th>
                                        <th>No of hours</th>
                                        <th>Record Type</th>
                                        <th>CreatedOn</th>
                                        {(this.state.isTimeSheetApproved == false || (this.state.isTimeSheetSubmittedToPayroll == false && this.state.isPayrollAdmin == true)) ?
                                          <th>Action</th>
                                        : null }
                                        
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {this.state.ListGridPastDetails.map(( listValue, index ) => {
                                          if(listValue.isAmendmends == true && listValue.wantToDisplayInTimeSheet == true){
                                            var action_btn = '';
                                            var cls_nm = 'pk-strikethrough';
                                            

                                            return (
                                              <tr key={index}>
                                                <td className={cls_nm}>{moment(listValue.timeSheetDate).format(process.env.DATE_FORMAT)}</td>
                                                <td className={cls_nm}>{listValue.serviceName}</td>
                                                <td className={cls_nm}>{moment(listValue.timeIN).format(process.env.DATETIME_FORMAT)}</td>
                                                <td className={cls_nm}>{moment(listValue.timeOUT).format(process.env.DATETIME_FORMAT)}</td>
                                                <td className={cls_nm}>{listValue.locationName}</td>
                                                <td className={cls_nm}>{listValue.note}</td>
                                                <td className={cls_nm}>{listValue.noOfHours}</td>
                                                <td className={cls_nm}>{listValue.recordType}</td>
                                                <td className={cls_nm}>{SystemHelpers.TimeZone_DateTime(listValue.createdOn)}</td>
                                                <td></td>
                                              </tr>
                                            );
                                          }else{
                                            var action_btn = '';

                                            if(this.state.IsEditBtn == true || this.state.isTimeSheetApproved == false || (this.state.isTimeSheetSubmittedToPayroll == false && this.state.isPayrollAdmin == true)){
                                              action_btn = this.Edit_Update_Btn_Func(listValue);
                                            }

                                            if(listValue.wantToDisplayInTimeSheet == true){
                                              return (
                                                <tr key={index}>
                                                  <td>{moment(listValue.timeSheetDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</td>
                                                  <td>{listValue.serviceName}</td>
                                                  <td>{moment(listValue.timeIN).format(process.env.DATETIME_FORMAT)}</td>
                                                  <td>{moment(listValue.timeOUT).format(process.env.DATETIME_FORMAT)}</td>
                                                  <td>{listValue.locationName}</td>
                                                  <td>{listValue.note}</td>
                                                  <td>{listValue.noOfHours}</td>
                                                  <td>{listValue.recordType}</td>
                                                  <td>{SystemHelpers.TimeZone_DateTime(listValue.createdOn)}</td>
                                                  <td>{action_btn}</td>
                                                </tr>
                                              );
                                            }
                                          }
                                          
                                      })}
                                      <tr><td className="pk-td-border" colSpan="6"> Total hours </td>
                                          <td> {this.state.EditworkedHour}</td>
                                          <td></td>
                                          <td></td>
                                          {(this.state.isPayrollAdminSession == true && this.state.isAmendmendsSubmitted == false ) ||( this.state.isAmendmendsSubmitted == false && this.state.isTimeSheetSubmittedToPayroll == false && (this.state.isCordinatorSession == true  || this.state.isSeniorCordinatorSession == true || this.state.isServiceCoordinatorLeadSession == true)) ?
                                            <td></td>
                                          : null }
                                          
                                      </tr>
                                    </tbody>
                                  </table>
                                  {/*{  this.state.ListGridPastDetails.length > 0 &&   this.state.isAmendmendsSubmitted == false && this.state.IsAmendmendsEnabled == true ?
                                    <div className="row justify-content">
                                      <button className="btn btn-success mr-1"  data-toggle="modal" data-target="#submit_amendmend_from_attSummary">Submit for amendment</button>
                                    </div>
                                    : null
                                  }*/}

                                  {/*{  this.state.ListGridPastDetails.length > 0 && this.state.isTimeSheetSubmittedToPayroll == false && this.state.isTimeSheetSubmitedToPayroll == false && this.state.isAmendmendsSubmitted == false && this.state.isTimeSheetApproved == true && (this.state.isCordinatorSession == true  || this.state.isSeniorCordinatorSession == true || this.state.isServiceCoordinatorLeadSession == true) ?
                                    <div className="row justify-content">
                                      <button className="btn btn-success mr-1"  data-toggle="modal" data-target="#submit_amendmend_from_attSummary">Submit for amendment</button>
                                    </div>
                                    : null
                                  }

                                  { this.state.ListGridPastDetails.length > 0 && this.state.isTimeSheetSubmittedToPayroll == true && this.state.isPayrollAdminSession == true && this.state.isTimeSheetSubmitedToPayroll == false ?
                                    <div className="row justify-content">
                                      <button className="btn btn-success mr-1"  data-toggle="modal" data-target="#submit_amendmend_from_attSummary">Submit for amendment</button>
                                    </div>
                                    : null
                                  }*/}
                                </div>
                              </div>
                            </div>
                            {/*Time Sheet Entries Grid*/}
                            <br/>
                            <br/>

                            {/*Summary Of Entries*/}
                            <div className="row">
                              
                              <div className="col-md-12 col-lg-12 col-xl-12 d-flex">
                                <div className="card flex-fill dash-statistics">
                                  <div className="card-body">
                                    <h5 className="card-title">Summary of Entries</h5>
                                    
                                    <div className="row filter-row"> 

                                      <div className="col-lg-3 col-sm-12 col-xs-12">
                                        <div className="title">Total Regular Hours: {this.state.SummaryOfEntriesDisplaytotalRegularHours}</div>
                                        <div className="title">Total Asleep Hours: {this.state.SummaryOfEntriesDisplaytotalAsleepHours}</div>
                                        <div className="title">Total TRIP Hours: {this.state.SummaryOfEntriesDisplaytotalTripHours}</div>
                                        <div className="title">Total Vacation Hours: {this.state.SummaryOfEntriesDisplaytotalVacationHours}</div>
                                        <div className="title"></div>
                                      </div>
                                      <div className="col-lg-3 col-sm-12 col-xs-12"> 
                                        <div className="title">Total Sick Hours: {this.state.SummaryOfEntriesDisplaytotalSickHours}</div>
                                        <div className="title">Total COVID-19 Hours: {this.state.SummaryOfEntriesDisplaytotalCOVID19Hours}</div>
                                        <div className="title">Total Brv Hours: {this.state.SummaryOfEntriesDisplaytotalBRVHours}</div>
                                        <div className="title">Total Leave Hours: {this.state.SummaryOfEntriesDisplaytotalLeaveHours}</div>
                                        <div className="title"></div>
                                      </div>
                                      <div className="col-lg-3 col-sm-12 col-xs-12"> 
                                        <div className="title">Total OT Hours: {this.state.SummaryOfEntriesDisplaytotalOTHours}</div>
                                        <div className="title">Total OT Banked Used Hours: {this.state.SummaryOfEntriesDisplaytotalOTBankedUsedHours}</div>
                                        <div className="title">Total Misc Banked Used Hours: {this.state.SummaryOfEntriesDisplaytotalMiscBankedUsedHours}</div>
                                        <div className="title"></div>
                                      </div>
                                      <div className="col-lg-3 col-sm-12 col-xs-12"> 
                                        <div className="title">Total On-call: {this.state.SummaryOfEntriesDisplaytotalOnCallHours}</div>
                                        <div className="title">Total STAT Hours: {this.state.SummaryOfEntriesDisplaytotalStatHours}</div>
                                        <div className="title">Total STAT Day-Off Hours: {this.state.SummaryOfEntriesDisplaytotalStatDayOffHours}</div>
                                      </div>
                                    </div>
                                        
                                  </div>
                                </div>
                              </div>

                            </div>
                            {/*Summary Of Entries*/}
                            <br/>
                            <br/>

                            {/*Payroll Adjustments Grid*/}
                              <div className="row">
                                <div className="card col-lg-12">
                                  <div className="card-body">
                                    <h3 className="card-title">Payroll Adjustments</h3>
                                    <h3 className="card-title"><a href="#" className="edit-icon" data-toggle="modal" data-target="#PayrollAdjustments_Add_modal"><i className="fa fa-plus" /></a></h3>
                                    <div className="table-responsive">
                                      {this.state.ListGrid_PayRollADJ.length > 0 ?
                                        <MDBDataTable
                                          striped
                                          bordered
                                          small
                                          data={data_PayRollADJ}
                                          entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                                          className="table table-striped custom-table mb-0 datatable"
                                        />
                                      :null}
                                      
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                            {/*Payroll Adjustments Grid*/}
                            <br/><br/>

                            {/*Notes*/}
                            { this.state.ListGridPastDetailsCount > 0  && this.state.EdittimeSheetContactMasterId != ''  ?
                            <div>
                              <div className="row">
                                 <div className="col-lg-12">
                                 <h3 class="card-title">Notes</h3>
                                  {/* Chat Main Row */}
                                  <div className="chat-main-row">
                                    {/* Chat Main Wrapper */}
                                    <div className="chat-main-wrapper">
                                      {/* Chats View */}
                                      <div className="col-lg-9 message-view task-view">
                                        <div className="chat-window">
                                          
                                          <div className="chat-contents">
                                            <div className="chat-content-wrap">
                                              <div className="chat-wrap-inner">
                                                <div className="chat-box">
                                                  {this.GetChatList()}
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <div className="chat-footer chat-footer-pk">
                                            <div className="message-bar">
                                              <div className="message-inner">
                                                {/*<a className="link attach-icon" href="#" data-toggle="modal" data-target="#drag_files"><img src={Attachment} alt="" /></a>*/}
                                                <div className="message-area">
                                                  <div className="input-group">
                                                    <textarea className="form-control" placeholder="Type Notes..." value={this.state.AddMsg} onChange={this.handleChange('AddMsg')} />
                                                    {/*<span className="input-group-append">
                                                      <button className="btn btn-custom send-btn-pk" type="button" onClick={this.AddNotes_Msg()}  ><i className="fa fa-send" /></button>
                                                    </span>*/}
                                                  </div>
                                                  <span className="form-text error-font-color">{this.state.errormsg["AddMsg"]}</span>
                                                </div>
                                              </div>
                                            </div>
                                            <div className="project-members">
                                              <div className="col-md-12 notes-padding-left">
                                                <div className="form-group">
                                                  <div className="checkbox">
                                                    <label>
                                                      <input type="checkbox" name="allow_emp" value="true" onChange={this.handleChange('allow_emp')}  checked={this.state.allow_emp == true ? "true" : null} /> Allow the employees to see the notes
                                                    </label>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          




                                        </div>
                                      </div>
                                      {/* /Chats View */}

                                      
                                    </div>
                                    {/* /Chat Main Wrapper */}
                                  </div>
                                  {/* /Chat Main Row */}
                                  
                                 </div>
                              </div>
                              <div className="row">
                                <div className="col-md-12 notes-padding-left">
                                  <div className="form-group">
                                    <label>Attachment</label>
                                    <FileUploadPreview
                                      ResetFileMethod={this.state.AddNotesResetflag}
                                      className="form-control" 
                                      setPropState={this.setPropState} />
                                      <input type="hidden" name="filePreviewsFinalEditActive" id="filePreviewsFinalEditActive" value={this.state.filePreviewsFinalEditActive}/>
                                    <span className="form-text success-font-color Guidelines_Doc">{process.env.ATTACHMENT_GUIDELINES}</span>
                                    <span className="form-text error-font-color">{this.state.errormsg["Addattachment"]}</span>
                                  </div>
                                </div>
                              </div>

                      
                              {
                                  this.state.filePreviewsFinal.length > 0 || this.state.filePreviewsFinalEditActive > 0 ?

                                  <div>
                                      <h4>Preview Files</h4>
                                      <br/>
                                      <div className="row row-sm">

                                          {this.state.filePreviewsFinalEdit.map(( listValue2, index ) => {
                                            return (
                                              this.GetImageAllNewImg(listValue2)
                                            );
                                          })}

                                          {this.state.filePreviewsFinal.map(( listValue, index ) => {
                                            return (
                                              this.GetImageAllNew(listValue.FileData)
                                            );
                                          })}
                                          
                                      </div> 
                                  </div>: undefined
                              }  
                            </div>
                            : null }
                            
                            {this.state.GetChatListLoad == true ?
                              <div className="submit-section">
                                <button className="btn btn-primary submit-btn" onClick={this.AddNotes_Msg()}>Save</button>
                              </div>
                              : null
                            }
                            
                            {/*Notes*/}

                          </div>
                        </div>   
                      </div>

                    </div>
                  </div>
                </div>
              {/* FT PT Main Grid Edit Button View Tab Modals */}
              
              {/* Edit Current Time Staff Tab Modals */}
              {/* Edit Current Time Staff Tab Modals */}
              
              {/* ===================================== Modal PK ===================================== */}

              {/* ===================================== Modal Add Edit TimeSheet & Payroll Adjustment ===================================== */}
                {/* /ADD Current Time Staff Tab Modals */}
                  <div id="CurrentTimeStaff_Add_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Time Sheet Entry</h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                         
                            <div className="card">
                              <div className="card-body">
                                <div className="row">
                              {/*    <div className="col-md-4">
                                    <div className="form-group">
                                      <label>Date <span className="text-danger">*</span></label>
                                      <input className="form-control" type="date" min={this.state.AddDate_Min} max={this.state.AddDate_Max} value={this.state.AddDate} onChange={this.handleChange('AddDate')} readonly/>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddDate"]}</span>
                                    </div>
                                  </div> */}

                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Service Name <span className="text-danger">*</span></label>
                                      <select className="form-control" id="AddService" value={this.state.AddService} onChange={this.handleChange('AddService')}>
                                        <option value="">-</option>
                                        {this.state.serviceView.map(( listValue, index ) => {
                                          
                                          if((this.state.role_permission.isCordinator == true || this.state.role_permission.isSeniorCordinator == true || this.state.role_permission.isCoordinatorMultipleLocations == true) && listValue.serviceId == process.env.API_ON_CALL ){
                                            return (
                                              <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                            );
                                          }else if(listValue.serviceId != process.env.API_ON_CALL){
                                            return (
                                              <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                            );
                                          }

                                          {/*return (
                                            <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                          );*/}
                                        })}
                                      </select>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddService"]}</span>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Location <span className="text-danger">*</span></label>
                                      <select className="form-control" id="Addlocation" value={this.state.Addlocation} onChange={this.handleChange('Addlocation')}>
                                        <option value="">-</option>
                                        {this.state.locationViews.map(( listValue, index ) => {
                                          return (
                                            <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                          );
                                        })}
                                      </select>
                                      <span className="form-text error-font-color">{this.state.errormsg["Addlocation"]}</span>
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Time in <span className="text-danger">*</span></label>
                                      <div className="row">
                                        <div className="col-md-12">
                                          <Datetime
                                            
                                            isValidDate={this.validationDateAddIn}
                                            inputProps={{readOnly: true}}
                                            onClose={this.AddTimeInonClose}
                                            closeOnTab={true}
                                            input={true}
                                            value={(this.state.AddTimeIn) ? this.state.AddTimeIn : ''}
                                            onChange={this.handleDateAddIn}
                                            dateFormat={process.env.DATE_FORMAT}
                                            timeFormat={process.env.TIME_FORMAT}
                                            timeConstraints={{
                                              hours: { min: 0, max: 23 },
                                              minutes: { min: 0, max: 59, step: 15 }
                                            }}
                                            renderInput={(props) => {
                                               return <input {...props} value={(this.state.AddTimeIn) ? props.value : ''} />
                                            }}
                                          />
                                        </div>
                                      </div>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddTimeIn"]}</span>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Time out <span className="text-danger">*</span></label>
                                      {/* <input className="form-control" type="datetime-local" step="900" id="AddTimeOut" min={this.state.AddTimeOut_Min} max={this.state.AddTimeOut_Max} value={this.state.AddTimeOut} onChange={this.handleChange('AddTimeOut')} /> */}
                                      <div className="row">
                                        <div className="col-md-12">
                                            <Datetime
                                              inputProps={{readOnly: true,disabled: this.state.AddTimeOutDisabled}}
                                              className="readonly-cls AddTimeOut"
                                              isValidDate={this.validationDateAddOut}
                                              onClose={this.AddTimeOutonClose}
                                              closeOnTab={true}
                                              input={true}
                                              value={(this.state.AddTimeOut) ? this.state.AddTimeOut : ''} 
                                              defaultValue={this.state.AddTimeOut}
                                              onChange={this.handleDateAddOut}
                                              dateFormat={process.env.DATE_FORMAT}
                                              timeFormat={process.env.TIME_FORMAT}
                                              timeConstraints={{
                                                hours: { min: 0, max: 23 },
                                                minutes: { min: 0, max: 59, step: 15 }
                                              }}
                                              renderInput={(props) => {
                                                   return <input {...props} value={(this.state.AddTimeOut) ? props.value : ''} />
                                               }}
                                            /> 
                                        </div>
                                      </div>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddTimeOut"]}</span>
                                    </div>
                                  </div>
                                 

                                  
                                  
                                  <div className="col-md-6">
                                    { this.state.AddService != process.env.API_ON_CALL ?
                                      <div className="form-group">
                                        <label># of hrs</label>
                                        <input className="form-control" type="text"  value={this.state.AddNoOfHours} onChange={this.handleChange('AddNoOfHours')} disabled/>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddNoOfHours"]}</span>
                                      </div>
                                      : null
                                    }
                                    
                                  </div>

                                  { this.state.AddPreference.length > 0 ?
                                    <div className="col-md-6">
                                      <div className="form-group">
                                        <label>Payout Preference <span className="text-danger">*</span></label>
                                        <select className="form-control" id="AddPreferenceService" value={this.state.AddPreferenceService} onChange={this.handleChange('AddPreferenceService')}>
                                          <option value="">-</option>
                                          {this.state.AddPreference.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPreferenceService"]}</span>
                                      </div>
                                    </div>
                                    : null
                                  }
                                  
                                  <div className="col-md-12">
                                    <div className="form-group">
                                      <label>Notes</label>
                                      <textarea className="form-control" type="text" value={this.state.AddNotes} onChange={this.handleChange('AddNotes')} ></textarea>
                                      <span className="form-text error-font-color">{this.state.errormsg["AddNotes"]}</span>
                                    </div>
                                  </div>
                                  
                                </div>
                                
                                
                                <div className="submit-section">
                                  <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Save</button>
                                </div>
                              </div>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                {/* /ADD Current Time Staff Tab Modals */}

                {/* /Edit Current Time Staff Tab Modals */}
                  <div id="CurrentTimeStaff_Edit_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Time Sheet Entry</h5>
                          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                         
                            <div className="card">
                              <div className="card-body">
                                <div className="row">
                                  {/*
                                    <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Date</label>
                                      <input className="form-control" type="text"  value={this.state.EditDate} onChange={this.handleChange('EditDate')} disabled/>
                                    </div>
                                  </div>
                                  */}

                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Service Name <span className="text-danger">*</span></label>
                                      <select className="form-control" id="EditService" value={this.state.EditService} onChange={this.handleChange('EditService')}>
                                        <option value="">-</option>
                                        {this.state.serviceView.map(( listValue, index ) => {
                                          if(this.state.EditService == process.env.API_ON_CALL || listValue.serviceId != process.env.API_ON_CALL){
                                            return (
                                            <option key={index} data-basserivce={listValue.isBasedOnSerivceBase} value={listValue.serviceId}>{listValue.serviceName}</option>
                                          );
                                          }
                                        })}
                                      </select>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Location <span className="text-danger">*</span></label>
                                      <select className="form-control" id="Editlocation" value={this.state.Editlocation} onChange={this.handleChange('Editlocation')}>
                                        <option value="">-</option>
                                        {this.state.locationViews.map(( listValue, index ) => {
                                          return (
                                            <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                          );
                                        })}
                                      </select>
                                    </div>
                                  </div>
                                  
                                  
                                  
                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Time in <span className="text-danger">*</span></label>
                                      {/*<input className="form-control" type="time"  value={this.state.EditTimeIn} onChange={this.handleChange('EditTimeIn')} /> */}
                                      <div className="row">
                                        <div className="col-md-12">
                                            <Datetime
                                              inputProps={{readOnly: true,disabled: this.state.EditTimeInDisabled}}
                                              isValidDate={this.validationDateEditIn}
                                              onClose={this.EditTimeInonClose}
                                              closeOnTab={true}
                                              input={true}
                                              value={(this.state.EditTimeIn) ? this.state.EditTimeIn : ''}
                                              onChange={this.handleDateEditIn}
                                              dateFormat={process.env.DATE_FORMAT}
                                              timeFormat={process.env.TIME_FORMAT}
                                              timeConstraints={{
                                                hours: { min: 0, max: 23 },
                                                minutes: { min: 0, max: 59, step: 15 }
                                              }}
                                              renderInput={(props) => {
                                                   return <input {...props} value={(this.state.EditTimeIn) ? props.value : ''} />
                                              }}
                                            />
                                        </div>
                                      </div>
                                      <span className="form-text error-font-color">{this.state.errormsg["EditTimeIn"]}</span>
                                    </div>
                                  </div>
                                  {/*<div className="col-md-6">
                                    <div className="form-group">
                                      <label>Time out <span className="text-danger">*</span></label>
                                      <input className="form-control" id="EditTimeOut" type="datetime-local" min={this.state.EditTimeOut_Min} max={this.state.EditTimeOut_Max}   value={this.state.EditTimeOut} onChange={this.handleChange('EditTimeOut')} />
                                      <span className="form-text error-font-color">{this.state.errormsg["EditTimeOut"]}</span>
                                    </div>
                                  </div> */}

                                  <div className="col-md-6">
                                    <div className="form-group">
                                      <label>Time out <span className="text-danger">*</span></label>
                                      {/* <input className="form-control" type="datetime-local" step="900" id="AddTimeOut" min={this.state.AddTimeOut_Min} max={this.state.AddTimeOut_Max} value={this.state.AddTimeOut} onChange={this.handleChange('AddTimeOut')} /> */}
                                      <div className="row">
                                        <div className="col-md-12">
                                            <Datetime
                                              inputProps={{readOnly: true,disabled: this.state.EditTimeOutDisabled}}
                                              isValidDate={this.validationDateEditOut}
                                              onClose={this.EditTimeOutonClose}
                                              closeOnTab={true}
                                              input={true}
                                              value={(this.state.EditTimeOut) ? this.state.EditTimeOut : ''}
                                              onChange={this.handleDateEditOut}
                                              dateFormat={process.env.DATE_FORMAT}
                                              timeFormat={process.env.TIME_FORMAT}
                                              timeConstraints={{
                                                hours: { min: 0, max: 23 },
                                                minutes: { min: 0, max: 59, step: 15 }
                                              }}
                                              renderInput={(props) => {
                                                   return <input {...props} value={(this.state.EditTimeOut) ? props.value : ''} />
                                              }}
                                            />
                                        </div>
                                      </div>
                                      <span className="form-text error-font-color">{this.state.errormsg["EditTimeOut"]}</span>
                                    </div>
                                  </div>
                                  


                                  <div className="col-md-6">
                                    { this.state.EditService != process.env.API_ON_CALL ?
                                      <div className="form-group">
                                        <label>No of hours</label>
                                        <input className="form-control" type="text"  value={this.state.EditNoOfHours} onChange={this.handleChange('EditNoOfHours')} disabled/>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditNoOfHours"]}</span>
                                      </div>
                                      : null
                                    }
                                  </div>

                                  { this.state.EditPreference.length > 0 ?
                                    <div className="col-md-6">
                                      <div className="form-group">
                                        <label>Payout Preference <span className="text-danger">*</span></label>
                                        <select className="form-control" id="EditPreferenceService" value={this.state.EditPreferenceService} onChange={this.handleChange('EditPreferenceService')}>
                                          <option value="">-</option>
                                          {this.state.EditPreference.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.guidId}>{listValue.name}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPreferenceService"]}</span>
                                      </div>
                                    </div>
                                    : null
                                  }
                                  
                                  <div className="col-md-12">
                                    <div className="form-group">
                                      <label>Notes</label>
                                      <textarea className="form-control" type="text" value={this.state.EditNotes} onChange={this.handleChange('EditNotes')} ></textarea>
                                    </div>
                                  </div>
                                  
                                </div>
                                  
                                
        
                                <div className="submit-section">
                                  <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Save</button>
                                </div>
                              </div>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                {/* /Edit Current Time Staff Tab Modals */}

                {/* ADD Payroll Adjustments Modal */}
                  <div id="PayrollAdjustments_Add_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Payroll Adjustments</h5>
                          <button type="button" className="close closePayrollAdj" data-dismiss="modal" aria-label="Close"  onClick={this.ClearRecordPayrollAdj()} >
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                         
                            <div className="card">
                              <div className="card-body">

                                <div className="row">
                                  <div className="col-md-12">
                                    <div className="form-group">
                                      <label>Employee Name : {this.state.tempfullName}</label>
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Adjustment Code<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <select className="form-control" id="AddPayrollADJadjustmentCode" value={this.state.AddPayrollADJadjustmentCode} onChange={this.handleChange('AddPayrollADJadjustmentCode')} >
                                          <option value="">-</option>
                                          {this.state.PayrollADJAdjustmentCodeList.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.payrollId} data-addpayrolladjsubstractfromishow={listValue.substractFrom} >{listValue.payrollCode}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJadjustmentCode"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Location<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <select className="form-control" value={this.state.AddPayrollADJlocation} onChange={this.handleChange('AddPayrollADJlocation')} >
                                          <option value="">-</option>
                                          {this.state.PayrollADJLocationList.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJlocation"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-4">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Date</label>
                                      <div className="col-lg-7">
                                        <Datetime
                                          isValidDate={this.validationDateAddIn}
                                          inputProps={{readOnly: true}}
                                          closeOnTab={true}
                                          input={true}
                                          value={(this.state.AddPayrollADJDate) ? this.state.AddPayrollADJDate : ''}
                                          onChange={this.handleAddPayrollADJDate}
                                          dateFormat={process.env.DATE_FORMAT}
                                          timeFormat={false}
                                          renderInput={(props) => {
                                            return <input {...props} value={(this.state.AddPayrollADJDate) ? props.value : ''} />
                                          }}
                                        />
                                        <button type="button" className="close" onClick={this.ClearaddPayrollAdjDateClearBtn()} >
                                          <span aria-hidden="true">×</span>
                                        </button>
                                        {/*<span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJDate"]}</span>*/}
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div className="col-md-4" id="id_hours_div_AddPayrollADJ">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Hours<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <input type="number" className="form-control" onKeyDown={this.handleKeypress} min='0' step='1' value={this.state.AddPayrollADJQtyhours} onChange={this.handleChange('AddPayrollADJQtyhours')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJQtyhours"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-4" id="id_minutes_div_AddPayrollADJ">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Minutes<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">  
                                        <select className="form-control floating" value={this.state.AddPayrollADJQtyminutes} onChange={this.handleChange('AddPayrollADJQtyminutes')}> 
                                          <option value="">-</option>
                                          <option value="00">00</option>
                                          <option value="15">15</option>
                                          <option value="30">30</option>
                                          <option value="45">45</option>
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJQtyminutes"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  {this.state.AddPayrollADJRateFieldIsShow == true ?
                                    <div className="col-md-4">
                                      <div className="form-group row">
                                        <label className="col-lg-5 col-form-label">Rate<span className="text-danger">*</span></label>
                                        <div className="col-lg-7">
                                          <select className="form-control floating" value={this.state.AddPayrollADJrate} onChange={this.handleChange('AddPayrollADJrate')}> 
                                            {this.DesignRate()}
                                          </select>
                                          <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJrate"]}</span>
                                        </div>
                                      </div>
                                    </div> :
                                    null
                                  }

                                  <div className="col-md-12">
                                    <div className="form-group row">
                                      <label className="col-lg-2 col-form-label">Description<span className="text-danger">*</span></label>
                                      <div className="col-lg-10">
                                        <input type="text" className="form-control" value={this.state.AddPayrollADJdescription} onChange={this.handleChange('AddPayrollADJdescription')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJdescription"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  {this.state.AddPayrollADJsubstractFromIsShow == "true" ?
                                    <div className="col-md-12 pk-margin-top">
                                      <div class="checkbox pk-margin-top-summary">
                                        <label>
                                          <input type="checkbox" name="checkbox" id="AddPayrollADJsubstractFrom" onChange={this.handleChange('AddPayrollADJsubstractFrom')} checked={this.state.AddPayrollADJsubstractFrom == true ? "true" : null} /> Subtract from
                                        </label>
                                      </div>
                                    </div>
                                    :null
                                  }

                                </div>

                                {(this.state.AddPayrollADJsubstractFrom == true || this.state.AddPayrollADJsubstractFrom == "true") && (this.state.AddPayrollADJsubstractFromIsShow == "true") ?
                                  <div className="row" id="SubtractfromEntitlement">
                                    
                                    <div className="col-md-6">
                                      <div className="form-group row">
                                        <label className="col-lg-6 col-form-label">Reason<span className="text-danger">*</span></label>
                                        <div className="col-lg-6">
                                          <select className="form-control" id="EditService" value={this.state.AddPayrollADJEntSubCategory}  onChange={this.handleChange('AddPayrollADJEntSubCategory')}  >
                                            <option value="">-</option>
                                            
                                              {this.state.PayrollADJEntitlementSubCategoryList.map(( listValue, index ) => {
                                                return (
                                                  <option key={index} value={listValue.id}>{listValue.name}</option>
                                                );
                                              })}
                                          </select>
                                          <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntSubCategory"]}</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div className="col-md-6">
                                      <div className="form-group row">
                                        <label className="col-lg-6 col-form-label">Entitlement Category<span className="text-danger">*</span></label>
                                        <div className="col-lg-6">
                                          <select className="form-control" value={this.state.AddPayrollADJEntCategory}  onChange={this.handleChange('AddPayrollADJEntCategory')} >
                                            {/*<option value="">-</option>*/}
                                              {this.state.PayrollADJEntitlementCategoryList.map(( listValue, index ) => {
                                                return (
                                                  <option key={index} value={listValue.id}>{listValue.name}</option>
                                                );
                                              })}
                                          </select>
                                          <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntCategory"]}</span>
                                        </div>
                                      </div>
                                    </div>
                                    

                                    {this.state.AddPayrollADJEntQtyshiftsDivIsShow == true ?

                                      <div className="col-md-4">
                                        <div className="form-groupp row">
                                          <label className="col-lg-5 col-form-label">Shift<span className="text-danger">*</span></label>
                                          <div className="col-lg-7">  
                                            <input type="number" className="form-control" onKeyDown={this.handleKeypress} min='0' step='1' value={this.state.AddPayrollADJEntQtyshifts} onChange={this.handleChange('AddPayrollADJEntQtyshifts')} />
                                            <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntQtyshifts"]}</span>
                                          </div>
                                        </div>
                                      </div>
                                      : null
                                    }

                                    {this.state.AddPayrollADJEntQtydaysDivIsShow == true ?
                                      <div className="col-md-4">
                                        <div className="form-groupp row">
                                          <label className="col-lg-5 col-form-label">Days<span className="text-danger">*</span></label>
                                          <div className="col-lg-7">  
                                            <input type="number" className="form-control" onKeyDown={this.handleKeypress} min='0' step='1' value={this.state.AddPayrollADJEntQtydays} onChange={this.handleChange('AddPayrollADJEntQtydays')} />
                                            <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntQtydays"]}</span>
                                          </div>
                                        </div>
                                      </div>
                                      : null
                                    }
                                      
                                    {this.state.AddPayrollADJEntQtyhoursDivIsShow == true ?
                                      <div className="col-md-4">
                                        <div className="form-group row">
                                          <label className="col-lg-5 col-form-label">Hours<span className="text-danger">*</span></label>
                                          <div className="col-lg-7">
                                            <input type="number" className="form-control" onKeyDown={this.handleKeypress} min='0' step='1' value={this.state.AddPayrollADJEntQtyhours} onChange={this.handleChange('AddPayrollADJEntQtyhours')} />
                                            <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntQtyhours"]}</span>
                                          </div>
                                        </div>
                                      </div>
                                      : null
                                    }
                                    {this.state.AddPayrollADJEntQtyminutesDivIsShow == true ?
                                      <div className="col-md-4">
                                        <div className="form-group row">
                                          <label className="col-lg-5 col-form-label">Minutes<span className="text-danger">*</span></label>
                                          <div className="col-lg-7">  
                                            <select className="form-control floating" value={this.state.AddPayrollADJEntQtyminutes} onChange={this.handleChange('AddPayrollADJEntQtyminutes')}> 
                                              <option value="">-</option>
                                              <option value="00">00</option>
                                              <option value="15">15</option>
                                              <option value="30">30</option>
                                              <option value="45">45</option>
                                            </select>
                                            <span className="form-text error-font-color">{this.state.errormsg["AddPayrollADJEntQtyminutes"]}</span>
                                          </div>
                                        </div>
                                      </div>
                                      : null
                                    }
                                    
                                  </div>: null
                                }
                                
                                
                                <div className="submit-section">
                                  <button className="btn btn-primary submit-btn" onClick={this.AddRecordPayrollAdj()} >Save</button>
                                </div>
                              </div>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                {/* /ADD Payroll Adjustments Modal */}

                {/* /Edit Payroll Adjustments Modal */}
                  <div id="PayrollAdjustments_Edit_modal" data-backdrop="static" className="modal custom-modal" role="dialog">
                    <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                      <div className="modal-content">
                        <div className="modal-header">
                          <h5 className="modal-title">Payroll Adjustments</h5>
                          <button type="button" className="close closePayrollAdj" data-dismiss="modal" aria-label="Close" >
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div className="modal-body">
                         
                            <div className="card">
                              <div className="card-body">

                                <div className="row">
                                  <div className="col-md-12">
                                    <div className="form-group">
                                      <label>Employee Name : {this.state.tempfullName}</label>
                                    </div>
                                  </div>

                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Adjustment Code<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <select className="form-control" id="EditPayrollADJadjustmentCode" value={this.state.EditPayrollADJadjustmentCode} onChange={this.handleChange('EditPayrollADJadjustmentCode')} >
                                          <option value="">-</option>
                                          {this.state.PayrollADJAdjustmentCodeList.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.payrollId}>{listValue.payrollCode}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJadjustmentCode"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-6">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Location<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <select className="form-control" id="EditPayrollADJlocation" value={this.state.EditPayrollADJlocation} onChange={this.handleChange('EditPayrollADJlocation')} >
                                          <option value="">-</option>
                                          {this.state.PayrollADJLocationList.map(( listValue, index ) => {
                                            return (
                                              <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                            );
                                          })}
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJlocation"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-md-4">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Date</label>
                                      <div className="col-lg-7">
                                        <Datetime

                                          inputProps={{disabled: true}}
                                          closeOnTab={true}
                                          input={true}
                                          value={(this.state.EditPayrollADJDate) ? this.state.EditPayrollADJDate : ''}
                                          onChange={this.handleEditPayrollADJDate}
                                          dateFormat={process.env.DATE_FORMAT}
                                          timeFormat={false}
                                          renderInput={(props) => {
                                             return <input {...props} value={(this.state.EditPayrollADJDate) ? props.value : ''} style={{"background-color" : "lightgray"}} />
                                          }}
                                        />
                                        {/*<span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJDate"]}</span>*/}
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div className="col-md-4" id="id_hours_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Hours<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">
                                        <input type="number" className="form-control" id="EditPayrollADJQtyhours" value={this.state.EditPayrollADJQtyhours} onChange={this.handleChange('EditPayrollADJQtyhours')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJQtyhours"]}</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-md-4" id="id_minutes_div">
                                    <div className="form-group row">
                                      <label className="col-lg-5 col-form-label">Minutes<span className="text-danger">*</span></label>
                                      <div className="col-lg-7">  
                                        <select className="form-control floating" id="EditPayrollADJQtyminutes" value={this.state.EditPayrollADJQtyminutes} onChange={this.handleChange('EditPayrollADJQtyminutes')}> 
                                          <option value="">-</option>
                                          <option value="00">00</option>
                                          <option value="15">15</option>
                                          <option value="30">30</option>
                                          <option value="45">45</option>
                                        </select>
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJQtyminutes"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  {this.state.EditPayrollADJRateFieldIsShow == true ?
                                    <div className="col-md-4">
                                      <div className="form-group row">
                                        <label className="col-lg-5 col-form-label">Rate<span className="text-danger">*</span></label>
                                        <div className="col-lg-7">
                                          <input type="text" className="form-control" id="EditPayrollADJrate" value={this.state.EditPayrollADJrate} onChange={this.handleChange('EditPayrollADJrate')} />
                                          <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJrate"]}</span>
                                        </div>
                                      </div>
                                    </div>
                                    :null
                                  }

                                  <div className="col-md-12">
                                    <div className="form-group row">
                                      <label className="col-lg-2 col-form-label">Description<span className="text-danger">*</span></label>
                                      <div className="col-lg-10">
                                        <input type="text" className="form-control" id="EditPayrollADJdescription" value={this.state.EditPayrollADJdescription} onChange={this.handleChange('EditPayrollADJdescription')} />
                                        <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJdescription"]}</span>
                                      </div>
                                    </div>
                                  </div>

                                  {this.state.EditPayrollADJsubstractFromIsShow == true ?
                                    <div className="col-md-12 pk-margin-top">
                                      <div className="checkbox pk-margin-top-summary">
                                        <input type="checkbox" className="" id="EditPayrollADJsubstractFrom" onChange={this.handleChange('EditPayrollADJsubstractFrom')} checked={this.state.EditPayrollADJsubstractFrom == true ? "true" : null} />
                                        <label className="col-lg-4 col-form-label">Subtract from</label>
                                      </div>
                                    </div>
                                    :null
                                  }

                                  {this.state.EditPayrollADJsubstractFrom == true || this.state.EditPayrollADJsubstractFrom == "true" ?
                                    <div className="row">  

                                      <div className="col-md-6">
                                        <div className="form-group row">
                                          <label className="col-lg-6 col-form-label">Reason<span className="text-danger">*</span></label>
                                          <div className="col-lg-6">
                                            <select className="form-control" id="EditPayrollADJEntSubCategory" value={this.state.EditPayrollADJEntSubCategory}  onChange={this.handleChange('EditPayrollADJEntSubCategory')}  >
                                              <option value="">-</option>
                                              
                                                {this.state.PayrollADJEntitlementSubCategoryList.map(( listValue, index ) => {
                                                  return (
                                                    <option key={index} value={listValue.id}>{listValue.name}</option>
                                                  );
                                                })}
                                            </select>
                                            <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntSubCategory"]}</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-md-6">
                                        <div className="form-group row">
                                          <label className="col-lg-6 col-form-label">Entitlement Category<span className="text-danger">*</span></label>
                                          <div className="col-lg-6">
                                            <select className="form-control" id="EditPayrollADJEntCategory" value={this.state.EditPayrollADJEntCategory}  onChange={this.handleChange('EditPayrollADJEntCategory')} >
                                              <option value="">-</option>
                                                {this.state.PayrollADJEntitlementCategoryList.map(( listValue, index ) => {
                                                  return (
                                                    <option key={index} value={listValue.id}>{listValue.name}</option>
                                                  );
                                                })}
                                            </select>
                                            <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntCategory"]}</span>
                                          </div>
                                        </div>
                                      </div>
                                      

                                      {this.state.EditPayrollADJEntQtyshiftsDivIsShow == true ?

                                        <div className="col-md-4">
                                          <div className="form-groupp row">
                                            <label className="col-lg-5 col-form-label">Shift<span className="text-danger">*</span></label>
                                            <div className="col-lg-7">  
                                              <input type="number" className="form-control" id="EditPayrollADJEntQtyshifts" value={this.state.EditPayrollADJEntQtyshifts} onChange={this.handleChange('EditPayrollADJEntQtyshifts')} />
                                              <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntQtyshifts"]}</span>
                                            </div>
                                          </div>
                                        </div>
                                        : null
                                      }

                                      {this.state.EditPayrollADJEntQtydaysDivIsShow == true ?
                                        <div className="col-md-4">
                                          <div className="form-groupp row">
                                            <label className="col-lg-5 col-form-label">Days<span className="text-danger">*</span></label>
                                            <div className="col-lg-7">  
                                              <input type="number" className="form-control" id="EditPayrollADJEntQtydays" value={this.state.EditPayrollADJEntQtydays} onChange={this.handleChange('EditPayrollADJEntQtydays')} />
                                              <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntQtydays"]}</span>
                                            </div>
                                          </div>
                                        </div>
                                        : null
                                      }

                                      {this.state.EditPayrollADJEntQtyhoursDivIsShow == true ?
                                        <div className="col-md-4" id="id_hours_div">
                                          <div className="form-group row">
                                            <label className="col-lg-5 col-form-label">Hours<span className="text-danger">*</span></label>
                                            <div className="col-lg-7">
                                              <input type="number" className="form-control" id="EditPayrollADJEntQtyhours" value={this.state.EditPayrollADJEntQtyhours} onChange={this.handleChange('EditPayrollADJEntQtyhours')} />
                                              <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntQtyhours"]}</span>
                                            </div>
                                          </div>
                                        </div>
                                        : null
                                      }
                                      {this.state.EditPayrollADJEntQtyminutesDivIsShow == true ?
                                        <div className="col-md-4" id="id_minutes_div">
                                          <div className="form-group row">
                                            <label className="col-lg-5 col-form-label">Minutes<span className="text-danger">*</span></label>
                                            <div className="col-lg-7">  
                                              <select className="form-control floating" id="EditPayrollADJEntQtyminutes" value={this.state.EditPayrollADJEntQtyminutes} onChange={this.handleChange('EditPayrollADJEntQtyminutes')}> 
                                                <option value="">-</option>
                                                <option value="00">00</option>
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                              </select>
                                              <span className="form-text error-font-color">{this.state.errormsg["EditPayrollADJEntQtyminutes"]}</span>
                                            </div>
                                          </div>
                                        </div>
                                        : null
                                      }

                                    </div>:null
                                  }

                                </div>
                                
                                {/*<div className="submit-section">
                                  <button className="btn btn-primary submit-btn" onClick={this.UpdateRecordPayrollAdj()} >Save</button>
                                </div>*/}
                              </div>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                {/* /Edit Payroll Adjustments Modal */}
              {/* ===================================== Modal Add Edit TimeSheet & Payroll Adjustment ===================================== */}  


              

              {/* ************************************************** Add Reports Modal  ******** ******** */}
              {/* /Add Reports Modal */}

              {/* Edit Reports Modal */}
              {/*<div id="edit_location" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Edit Holiday</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        
                      </form>
                    </div>
                  </div>
                </div>
              </div>*/}
              {/* /Edit Reports Modal */}

              {/* Delete Today Work Modal */}
              {/* Delete Today Work Modal */}

              {/* Delete timesheet  Modal */}
                <div className="modal custom-modal" id="timesheet_Delete_modal" role="dialog">
                  <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-body">
                        <div className="form-header">
                          <h3>Time Sheet</h3>
                          <p>Are you sure want to delete selected record?</p>
                        </div>
                        <div className="modal-btn delete-action">
                          <div className="row">
                            <div className="col-6">
                              <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">Delete</a>
                            </div>
                            <div className="col-6">
                              <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              {/* /Delete Human Resources Modal */}

              {/* Delete Payroll Adjustment  Modal */}
                <div className="modal custom-modal" data-backdrop="static" id="delete_PayrollAdjustments" role="dialog">
                  <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-body">
                        <div className="form-header">
                          <h3>Payroll Adjustment</h3>
                          {/*<p>Are you sure you want to mark Payroll Adjustment as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>*/}
                          <p>Are you sure you want to Delete Payroll Adjustment ?</p>
                        </div>
                        <div className="modal-btn delete-action">
                          <div className="row">
                            <div className="col-6">
                              {/*<a  onClick={this.DeleteRecordPayrollAdj()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>*/}
                              <a  onClick={this.DeleteRecordPayrollAdj()} className="btn btn-primary continue-btn">Delete</a>
                            </div>
                            <div className="col-6">
                              <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              {/* /Delete Payroll Adjustment Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default BacthPayrollReports;
