
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';


import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import Loader from '../Loader';
import LoaderSecond from '../LoaderSecond';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';
import FileUploadHelper from '../Helpers/FileUploadHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import moment from 'moment';

import Datetime from "react-datetime";

import SelectDropdown from 'react-dropdown-select';

//import Select from 'react-select-me';
import Select from 'react-select';

import FileUploadPreview from '../FileUpload/FileUploadPreviewNotes'
import DocxImg from '../../assets/img/doc/docx.png'
import ExcelImg from '../../assets/img/doc/excel.png'
import PdfImg from '../../assets/img/doc/pdf.png'

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import axios from 'axios';

class BacthPayrollReports extends Component {
  constructor(props) {
    super(props);
    this.state = {

        // Pagination
        totalCount : 0,
        pageSize : 5,
        currentPage : 1,
        totalPages : 0,
        previousPage : false,
        nextPage : false,
        searchText : '',
        pagingData : {},
        TempsearchText:'',

        sortColumn : 'StaffNumberId',
        SortType : false,
        IsSortingEnabled : true,
        display_Inactive_flage:"false",
        // Pagination

        locationID:localStorage.getItem("primaryLocationId"),
        staffContactID:localStorage.getItem("contactId"),
        fullName:localStorage.getItem("fullName"),

        role_reports_can : {},
        role_employees_can : {},
        role_hierarchy_can : {},
        role_payroll_adjustment_menu_can : {},

        errormsg :  '',
        isDelete : false,
        ListGrid : [],

        header_data : [],

        // FT PT 
        payPeriodListFilter : [],
        locationListFilter : [],
        
        employmentTypeListFilter :[],
        
        EmployeeNameListFilter : [],

        FilterPayPeriod : '',
        FilterLocation : '',
        
        FilterEmploymentType : '',
        
        FilterEmployee : '',

        ExportFilter : {},
       
        GetCRMUserList:[],
        selectedOptionLocation: null,
        selectedOptionEmployees: null,
        // FT PT
        

        // ============ TimeSheet ============ //
        // Drop down list
        serviceView:[],
        locationViews:[],
        AddPreference:[],
        // Drop down list

        // Add
        AddDate:'',
        AddTimeIn:'',
        AddTimeOut:'',
        Addlocation:'',
        AddService:'',
        AddNotes:'',
        AddNoOfHours:'',
        timeSheetPeriodId:'',
        IsNoOfHoursvalid: false,
        AddTimeOutDisabled : false,
        // Add

        // Edit
        timeSheetTransactionId: '',
        EditService: '',
        Editlocation: '',
        EditTimeIn: '',
        EditTimeOut: '',
        EditNoOfHours: '',
        EditDate: '',
        IsNoOfHoursvalidEdit:true,
        DeletetimesheetTransactionId:'',
        // Edit

        ListGridPastDetails:[],
        role_permission: {},
        // ============ TimeSheet ============ //

        
        

        
        UserHoursType : '',


       
        BatchesList:[],
        BatchRecord:'',

        // ============ FT PT Generate Report ============ //
        header_data_FTPT_GenerateReport_ListGrid : [],
        ListGrid_FTPT_GenerateReport : []
        // ============ FT PT Generate Report ============ //

        /* ************************ Pk Dev Timesheet ************************* */
    };

    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    
   
  }

  onChangeSearch(value) {
    console.log(value);
    this.setState({ value });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }

  hideLoader2 = () => {
    this.setState({ loading2: false });
  }
  showLoader2 = () => {
    this.setState({ loading2: true });
  }
  // Loader hide show method

 
  // Handle TimeSheet

  handleChangeSelectLocation = (selectedOptionLocation) => {
    this.setState({ selectedOptionLocation });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ FilterLocation: selectedOptionLocation.value });
    //LocationFilterID
  };

  handleChangeSelectEmployees = (selectedOptionEmployees) => {
    this.setState({ selectedOptionEmployees });
    //console.log(`Option selected:`, selectedOptionLocation.value);
    this.setState({ FilterEmployee: selectedOptionEmployees.value });
    //LocationFilterID
  };
  

  handleChange = input => e => {

    if( [input] != 'display_Inactive'){
      this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    }
    
    //console.log('handleChange');
    //console.log(input);

    if (this.state[input] != '') {
      delete this.state.errormsg[input];
    }

    // Active - Inactive employee Checkbox Display
    if( [input] == 'display_Inactive'){
      //console.log("**************************************** Start");
      //console.log([input]);
      //console.log(this.state.display_Inactive_flage);
      //console.log("**************************************** End");
      if(this.state.display_Inactive_flage == "true"){
        this.setState({ display_Inactive_flage: "false" });
      }else{
        this.setState({ display_Inactive_flage: "true" });
      }
    }
    // Active - Inactive employee Checkbox Display

    
    
    console.log(input);
    console.log(e.target);

    

    if (e.target.value != '') {
        delete this.state.errormsg[input];
    }

  
  }

  componentDidMount() {
    console.log("Reports");
    this.GetTimeSheetReportView();
    this.GetFTPTWebjobReport_default();
  }


  
  // ============================================== FT PT report Main grid & TimeSheet Pay Period Drop down API ============================================== //
  GetTimeSheetReportView(){

    /* Role Management */
      var getrole = SystemHelpers.GetRole();
      console.log('Location Get role');
      console.log(getrole.locations_can);
      //let canDelete = getrole.locations_can.locations_can_delete;
      //let locationscanViewall = getrole.locations_can.locations_can_viewall;
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */
    this.showLoader();
    var url=process.env.API_API_URL+'GetTimeSheetReportView?loggedInUserId='+this.state.staffContactID+'&rolePriorityId='+hierarchyId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
      //console.log("responseJson Report GetTimeSheetReportView");
      //console.log(data);
      
      // debugger;
      if (data.responseType === "1") {

        this.setState({ payPeriodListFilter: data.data.timeSheetPeriodViews});

        if(this.state.role_hierarchy_can.hierarchy_can_id == "1")
        {
          $('#FilterLocation').prop('disabled', true);

          $('#FilterEmployee').prop('disabled', true);
          this.setState({ FilterEmployee: this.state.staffContactID });

          $('#FilterEmploymentType').prop('disabled', true);
          this.setState({ FilterEmploymentType: localStorage.getItem("employmentHoursName") });
          
          
        }
        
        
        
        this.setState({ employmentTypeListFilter: data.data.employeementTypes });
        
        /* ************************************************************ */
        let locationViews_push = [];
        var locationViews = data.data.locationViews;

        let locationtemp = {};
        locationtemp["value"] = "";
        locationtemp["label"] = "All";
        locationViews_push.push(locationtemp);

        for (var zz = 0; zz < locationViews.length; zz++) {
            let locationtemp = {};
            //locationtemp["value"] = locationViews[zz].locationId;
            locationtemp["value"] = locationViews[zz].locationGuid;
            locationtemp["label"] = locationViews[zz].locationName;
            locationViews_push.push(locationtemp);
        }

        this.setState({ LocationListFilter: locationViews_push });
        //this.setState({ locationListFilter: data.data.locationViews });
        /* ************************************************************ */

        /* ************************************************************ */
        let employeeViews_push = [];
        var employeeViews = data.data.employees;

        let employeetemp = {};
        employeetemp["value"] = "";
        employeetemp["label"] = "All";
        employeeViews_push.push(employeetemp);

        for (var zz = 0; zz < employeeViews.length; zz++) {
            let employeetemp = {};
            employeetemp["value"] = employeeViews[zz].contactId;
            employeetemp["label"] = employeeViews[zz].employeeName;
            employeeViews_push.push(employeetemp);
        }

        this.setState({ EmployeeNameListFilter: employeeViews_push });
        //this.setState({ EmployeeNameListFilter: data.data.employees });
        /* ************************************************************ */
        this.setState({ locationViews: data.data.locationViews });
        this.setState({ serviceView: data.data.serviceViews });
        
        this.Default_TimeSheet(data.data.timeSheetPeriodViews);
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetUserWiseTimeSheetData error');
      console.log(error);
      this.props.history.push("/error-500");
    });
  }

  Default_TimeSheet(timeSheetPeriod){
      var length = timeSheetPeriod.length;
      
      if (length > 0) {
        var i = 1;
        for (var zz = 0; zz < length; zz++) {
          if(timeSheetPeriod[zz].isCurrentTimeSheet == true){
            this.setState({ FilterPayPeriod: timeSheetPeriod[zz].timeSheetPeriodId});
          }
          i++;
        }
      }
  }


  
  // FT PT report Main grid API


  // FT PT report Main grid "Export Excel" API PK DEV
  TableHeaderDesignFTPTGenerateReport(){
    var columnsFTPTGenerateReport = [
      {
        label: 'ReportID',
        field: 'reportID',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Report Type',
        field: 'reportType',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Pay Period',
        field: 'payPeriod',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Download',
        field: 'download',
        sort: 'asc',
        width: 150
      },
      {
        label: 'CreatedOn',
        field: 'createdOn',
        sort: 'asc',
        width: 150
      }
    ];  

    return columnsFTPTGenerateReport;
  }

  GetFTPTWebjobReport_default(){
      
      // this.setState({ ListGrid : [] });
      // this.setState({ ExportFilter: {} });
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */

      let step1Errors = {};
    
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      var para = '?LoggedId='+this.state.staffContactID+'&sortColumn=CreatedOn&sortType=true';

      var ApiName= 'GETParametersByLoggedId'+para;
      
      var pass_url = ApiName;
      
      this.showLoader();

      var url=process.env.API_API_URL+pass_url;
      //axios.get(url + sessionBUID)
      axios.get(url, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      })
      .then(data => {
        console.log("responseJson GETParametersByLoggedId Batch payroll axios");
          console.log(data);
          if (data.data.responseType === "1") {

              if(data.data.data != null)
              {

                this.setState({ header_data_FTPT_GenerateReport_ListGrid: this.TableHeaderDesignFTPTGenerateReport() });
                this.setState({ ListGrid_FTPT_GenerateReport: this.rowData_FTPT_GenerateReport(data.data.data) });

              }
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
          console.log('GETParametersByLoggedId error axios');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  GetFTPTWebjobReport_defaultnew(){
      
      // this.setState({ ListGrid : [] });
      // this.setState({ ExportFilter: {} });
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */

      let step1Errors = {};
    
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      var para = '?LoggedId='+this.state.staffContactID+'&sortColumn=CreatedOn&sortType=true';

      var ApiName= 'GETParametersByLoggedId'+para;
      
      var pass_url = ApiName;
      
      //this.showLoader();

      var url=process.env.API_API_URL+pass_url;
      //axios.get(url + sessionBUID)
      axios.get(url, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      })
      .then(data => {
        console.log("responseJson GETParametersByLoggedId Batch payroll axios");
          console.log(data);
          if (data.data.responseType === "1") {

              if(data.data.data != null)
              {

                this.setState({ header_data_FTPT_GenerateReport_ListGrid: this.TableHeaderDesignFTPTGenerateReport() });
                this.setState({ ListGrid_FTPT_GenerateReport: this.rowData_FTPT_GenerateReport(data.data.data) });

              }
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
          console.log('GETParametersByLoggedId error axios');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  

  ExportReportDataWebJob = () => e => {
    
      //console.log("ExportReportData");
      e.preventDefault();

      var currentPage = 1;
      var pageSize = 5;
      var searchText ="";
      
      //this.setState({ ListGrid : [] });
      //this.setState({ ExportFilter: {} });
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      /* Role Management */

      // Pagination
      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      this.setState({ pagingData : bodyarray });

      this.setState({ currentPage: currentPage });
      this.setState({ pageSize: pageSize });

      var sort_Column = this.state.sortColumn;
      var Sort_Type = this.state.SortType;
      
      var IsSortingEnabled = true;

      var url_paging_para = '&pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled;
      // Pagination


      let step1Errors = {};
    
      if (this.state["FilterEmploymentType"] =='') {
        step1Errors["FilterEmploymentType"] = "Please select Report Type.";
      }

      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      /*var paraPayPeriod = 'periodProfileId='+this.state.FilterPayPeriod;
      var BatchRecord = '&batchRecord='+this.state.BatchRecord;
      var paraLocation = '&locationId='+this.state.FilterLocation;
      var paraEmploymentType = '&employeementType='+this.state.FilterEmploymentType;
      var paraContactId = '&contactId='+this.state.FilterEmployee;
      var paraDisplayOnlyActive = '&displayOnlyActive='+this.state.display_Inactive_flage;
      var paraneedToExport = '&needToExport=true';*/

      let bodyarray1 = {};
      bodyarray1["periodId"] = this.state.FilterPayPeriod;
      bodyarray1["locationId"] = this.state.FilterLocation;
      bodyarray1["reportType"] = this.state.FilterEmploymentType;
      bodyarray1["employeeId"] = this.state.FilterEmployee;
      bodyarray1["base64"] = "";
      bodyarray1["loggedId"] = this.state.staffContactID;
      bodyarray1["sortColumn"] = "CreatedOn";
      //bodyarray1["sortType"] = "true";
      bodyarray1["sortType"] = "false";
      //bodyarray1["display_Inactive_flage"] = this.state.display_Inactive_flage;
      bodyarray1["displayOnlyActive"] = this.state.display_Inactive_flage;
      
      //ExportFilter
      let ExportFilterArray = {
        FilterPayPeriod: this.state.FilterPayPeriod,
        FilterLocation: this.state.FilterLocation,
        FilterEmploymentType: this.state.FilterEmploymentType,
        FilterEmployee: this.state.FilterEmployee,
        FilterDisplayOnlyActive: this.state.display_Inactive_flage,
        FilterPagingPara: url_paging_para,
      };
      this.setState({ ExportFilter: ExportFilterArray });
      //ExportFilter

      var ApiName= 'WebjobSaveptParameters'
      
      var pass_url = ApiName;
      
      this.showLoader();

      var url=process.env.API_API_URL+pass_url;
      //axios.get(url + sessionBUID)
      axios.post(url,bodyarray1, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        //body: JSON.stringify(bodyarray1)
      })
      .then(data => {
        console.log("responseJson WebjobSaveptParameters Batch payroll axios");
          console.log(data);
          
          if (data.data.responseType === "1") {

              if(data.data.data != null)
              {

                //this.setState({ header_data_FTPT_GenerateReport_ListGrid: this.TableHeaderDesignFTPTGenerateReport() });
                //this.setState({ ListGrid_FTPT_GenerateReport: this.rowData_FTPT_GenerateReport(data.data.data) });

                this.GetFTPTWebjobReport_default();

                SystemHelpers.ToastSuccess(data.data.responseMessge);  
                /*//alert();
                var today = new Date();
                var y = today.getFullYear();
                var m = today.getMonth() + 1;
                var d = today.getDate();
                var h = today.getHours();
                var mi = today.getMinutes();
                var s = today.getSeconds();
                var ms = today.getMilliseconds();
                var time = "PHSS000"+y  + m  + d  + h  + mi  + s + ms+FileUploadHelper.randomFun();

                // File name
                var payperiodname = $('#FilterPayPeriod').find(':selected').data('payperiodname');
                
                let firstChar = $.trim(payperiodname.split(" ")[0]);
                let lastChar = $.trim(payperiodname.split(" ").pop());
                
                if(this.state.FilterEmploymentType == "false"){
                  var File_Name= 'PT Payroll Batch '+payperiodname;
                }else{
                  var name_ptfile = moment(firstChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT)+ " to " +moment(lastChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT);
                  var File_Name= 'FT Payroll Batch '+name_ptfile;
                }
                // File name
                
                var createBase64 = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+data.data.data;
                var ext=File_Name+'.xlsx';
                
                const linkSource = createBase64;
                const downloadLink = document.createElement("a");
                const FileName = ext;

                downloadLink.href = linkSource;
                downloadLink.download = FileName;
                downloadLink.click();*/

              }
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastWarning(data.data.responseMessge);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
          console.log('WebjobSaveptParameters error axios');
        console.log(error);
        this.props.history.push("/error-500");
      });
  }

  rowData_FTPT_GenerateReport(ListGridFTPTGenerateReport) {
    console.log('rowData ListGridFTPTGenerateReport');
    console.log(ListGridFTPTGenerateReport);

    var ListGridFTPTGenerateReport_length = ListGridFTPTGenerateReport.length;
    let dataArray = [];
    //console.log(ListGridFTPTGenerateReport_length);
    if(ListGridFTPTGenerateReport_length > 0)
    {
      //console.log(ListGridFTPTGenerateReport_length);
      var recall_method = false;
      var totalcount = ListGridFTPTGenerateReport_length;
      for (var x = 0; x < ListGridFTPTGenerateReport_length; x++)
      {

        if(recall_method == false && ListGridFTPTGenerateReport[x].base64 == "" || ListGridFTPTGenerateReport[x].base64 == null){
          recall_method = true;
        }
        var tempdataArray = [];
        //  PT false, FT true // false =>FT , True =>PT
        var FTPT_type = "PT";
        if(ListGridFTPTGenerateReport[x].reportType == "true" || ListGridFTPTGenerateReport[x].reportType == true){
          FTPT_type = "FT";
        }

        var payPeriodDis= moment(ListGridFTPTGenerateReport[x].startDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT) + ' to ' +moment(ListGridFTPTGenerateReport[x].endDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);

        //tempdataArray.reportID = ListGridFTPTGenerateReport[x].periodId;
        tempdataArray.reportID = "RG00"+(totalcount - x);
        tempdataArray.reportType = FTPT_type;
        tempdataArray.payPeriod = payPeriodDis;

        if(ListGridFTPTGenerateReport[x].base64 != null){
          var link = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+ListGridFTPTGenerateReport[x].base64;
          tempdataArray.download = <a href="#" id="FTPTsearchBtn" class="col-lg-9 col-sm-12 col-xs-12 btn btn-success btn-block" onClick={this.Download_FTPT_GenerateReport(ListGridFTPTGenerateReport[x])}>Download</a>;//ListGridFTPTGenerateReport[x].base64;
        }else{
          tempdataArray.download = <button class="col-lg-9 col-sm-12 col-xs-12 btn btn-danger btn-block" type="button" disabled>
                                  <span class="spinner-border spinner-border-sm marginrightexport" role="status" aria-hidden="true"></span>
                                  Loading...
                                </button>;
        }
        
        tempdataArray.createdOn = SystemHelpers.TimeZone_DateTime(ListGridFTPTGenerateReport[x].createdOn);
        //tempdataArray.createdOn = ListGridFTPTGenerateReport[x].createdOn + '(' + SystemHelpers.TimeZone_DateTime(ListGridFTPTGenerateReport[x].createdOn)+')';
        dataArray.push(tempdataArray);
      }

      if(recall_method == true){

        setTimeout( () => {this.GetFTPTWebjobReport_defaultnew()}, 10000);
        // setTimeout(function () {
        //     console.log("setTimeout GetFTPTWebjobReport call");
        //     this.GetFTPTWebjobReport_default();
        // }, 15000);
      }
    }
    console.log('Return rowdata array ListGridFTPTGenerateReport');
    console.log(dataArray);
    return dataArray;
  }

  Download_FTPT_GenerateReport= (Data) => e => {
    console.log("Download_FTPT_GenerateReport");
    console.log(Data);

    // File name
    var payperiodname = moment(Data.startDate,process.env.API_DATE_FORMAT).format(process.env.FTPAYROLLBACTH_DATEFORMAT) + ' to ' +moment(Data.endDate,process.env.API_DATE_FORMAT).format(process.env.FTPAYROLLBACTH_DATEFORMAT);//$('#FilterPayPeriod').find(':selected').data('payperiodname');
    
    let firstChar = $.trim(payperiodname.split(" ")[0]);
    let lastChar = $.trim(payperiodname.split(" ").pop());
    
    if(Data.reportType == "false" || Data.reportType == false){
      var File_Name= 'PT Payroll Batch '+payperiodname;
    }else{
      var name_ptfile = moment(Data.startDate,process.env.API_DATE_FORMAT).format(process.env.FTPAYROLLBACTH_DATEFORMAT) + ' to ' +moment(Data.endDate,process.env.API_DATE_FORMAT).format(process.env.FTPAYROLLBACTH_DATEFORMAT);;//moment(firstChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT)+ " to " +moment(lastChar,'DD/MM/YYYY').format(process.env.FTPAYROLLBACTH_DATEFORMAT);
      var File_Name= 'FT Payroll Batch '+name_ptfile;
    }
    // File name

    var createBase64 = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+Data.base64;
    var ext=File_Name+'.xlsx';
    
    const linkSource = createBase64;
    const downloadLink = document.createElement("a");
    const FileName = ext;

    downloadLink.href = linkSource;
    downloadLink.download = FileName;
    downloadLink.click();
  }
  // FT PT report Main grid "Export Excel" API PK DEV



  render() {

    

    const data_FTPT_Report = {
      columns: this.state.header_data_FTPT_GenerateReport_ListGrid,
      rows: this.state.ListGrid_FTPT_GenerateReport
    };

    const { selectedOptionLocation,selectedOptionEmployees  } = this.state;

    return ( 
      <div className="main-wrapper">
     
        {/* Toast & Loder method use */}
          
        {(this.state.loading) ? <Loader /> : null} 
        {(this.state.loading2) ? <LoaderSecond /> : null} 
        {/* Toast & Loder method use */}
        <Header/>

        <div className="page-wrapper">
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Generate Report</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">Batch Payroll Reports Export</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">
                      
                    </div>
                  </div>
                </div>

                {/* Search Filter */}
                  <div className="row filter-row">

                    {/* ==================== search Row 1 ==========================*/}
                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterEmploymentType" value={this.state.FilterEmploymentType} onChange={this.handleChange('FilterEmploymentType')}> 
                          <option value="">-</option>
                          <option value="false">PT</option>
                          <option value="true">FT</option>
                        </select>
                        <label className="focus-label">Report Type</label>
                        <span className="form-text error-font-color">{this.state.errormsg["FilterEmploymentType"]}</span>
                      </div>
                    </div>
                    
                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterPayPeriod" value={this.state.FilterPayPeriod} onChange={this.handleChange('FilterPayPeriod')}> 
                          <option value="" data-payperiodname="" >-</option>
                          {this.state.payPeriodListFilter.map(( listValue, index ) => {
                              var payPeriodName = moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)+ " to " +moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
                              return (
                                <option key={index} value={listValue.timeSheetPeriodId} data-payperiodname={payPeriodName} >{moment(listValue.timeSheetPeriodStartDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)} to {moment(listValue.timeSheetPeriodEndDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Pay period</label>
                      </div>
                    </div>
                    <div className="col-lg-3 col-sm-4 col-xs-12">  
                      <div className="form-group form-focus pk-emp-padding-top">
                        <div className="checkbox">
                          <label>
                            <input type="checkbox" name="display_Inactive"  value="true" onChange={this.handleChange('display_Inactive')}  checked={this.state.display_Inactive_flage == "true" ? "true" : null} /> Display Inactive Employees
                          </label>
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-3 col-sm-12 col-xs-12">  
                      <a href="#" id="FTPTsearchBtn" className="col-lg-12 col-sm-12 col-xs-12 float-right btn btn-danger btn-block" onClick={this.ExportReportDataWebJob()}>Generate Report</a>  
                    </div> 
                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        {/*<select className="form-control floating" id="FilterLocation" value={this.state.FilterLocation} onChange={this.handleChange('FilterLocation')}> 
                          <option value="" data-locationname="">All</option>
                          {this.state.locationListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.locationId} data-locationname={listValue.locationName} >{listValue.locationName}</option>
                              );
                           
                          })}
                        </select>*/}
                        {/* New <Select
                          value={selectedOptionLocation}
                          onChange={this.handleChangeSelectLocation}
                          options={this.state.LocationListFilter}
                          defaultValue={""}
                          isSearchable={true}
                          placeholder="Search Location"
                        />*/}
                        {/*<label className="focus-label">Location</label>*/}
                      </div>
                    </div>
                    
                    {/*<div className="col-sm-6 col-md-2"> 
                      <div className="form-group form-focus select-focus">
                        <select className="form-control floating" id="FilterEmploymentType" value={this.state.FilterEmploymentType} onChange={this.handleChange('FilterEmploymentType')}> 
                          <option value="">All</option>
                          {this.state.employmentTypeListFilter.map(( listValue, index ) => {
                           
                              return (
                                <option key={index} value={listValue.name} >{listValue.name}</option>
                              );
                           
                          })}
                        </select>
                        <label className="focus-label">Employment Type</label>
                      </div>
                    </div>*/}

                    <div className="col-lg-3 col-sm-3 col-xs-12"> 
                      <div className="form-group form-focus select-focus">
                        {/*<input className="form-control" type="text" id="FilterEmployeeName" value={this.state.FilterEmployeeName}  onChange={this.handleChange('FilterEmployeeName')} />*/}
                        {/*<select className="form-control" id="FilterEmployee" value={this.state.FilterEmployee}  onChange={this.handleChange('FilterEmployee')} >
                          <option value='' data-employeename="" >-</option>
                          {this.state.EmployeeNameListFilter.map(( listValue, index ) => {
                            return (
                              <option key={index}  value={listValue.contactId} data-employeename={listValue.employeeName} >{listValue.employeeName}</option>
                            );
                          })}
                        </select>*/}
                        {/* New <Select
                          value={selectedOptionEmployees}
                          onChange={this.handleChangeSelectEmployees}
                          options={this.state.EmployeeNameListFilter}
                          defaultValue={""}
                          isSearchable={true}
                          placeholder="Search Employee"
                        />*/}
                        {/*<label className="focus-label">Employee name</label>*/}
                      </div>
                    </div>

                    {/* ==================== search Row 1 ==========================*/}

                    {/* ==================== search Row 2 ==========================*/}
                    
                      
                      <div className="col-sm-6 col-md-2">
                        {/*<div className="form-group form-focus">
                          <label className="focus-label">Sorting</label>
                          <select className="form-control floating" id="sortColumn" value={this.state.sortColumn} onChange={this.handleChange('sortColumn')}> 
                            <option value="StaffNumberId">Employee-ID</option>
                            <option value="EmployeeName">Employee Name</option>
                            <option value="LocationCode">Location-ID</option>
                            <option value="LocationName">Location Name</option>
                          </select>
                        </div>*/}
                      </div> 

                      <div className="col-sm-6 col-md-2">
                        {/*<div className="form-group form-focus">
                          <label className="focus-label">Sorting Order</label>
                          <select className="form-control floating" id="SortTypeId" value={this.state.SortType} onChange={this.handleChange('SortType')}> 
                            <option value="">-</option>
                            <option value="false">Ascending</option>
                            <option value="true">Descending</option>
                          </select>
                        </div>*/}
                      </div> 

                      <div className="col-sm-6 col-md-3">  
                        {/*<div className="form-group form-focus">
                          <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                          <label className="focus-label">Search</label>
                        </div>*/}
                      </div>

                      
                    
                    {/* ==================== search Row 2 ==========================*/}

                    {/* ==================== search Row 3 ==========================*/}
                    

                    
                    
                    

                   
                    {/* ==================== search Row 3 ==========================*/}

                  </div>
                {/* /Search Filter */}    
                {/* /Page Header */}
                

                
                  
                <br/>
                

                <br/>
                {/* FT PT Report Main Table */}
                <div className="row">
                  <div className="col-md-12">
                      <div className="table-responsive">

                      {this.state.ListGrid_FTPT_GenerateReport.length > 0 ?

                        <MDBDataTable
                          striped
                          bordered
                          small
                          data={data_FTPT_Report}
                          entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                          className="table table-striped custom-table mb-0 datatable"
                        />
                        :null
                      }

                      
                        {/*<table className="table table-striped custom-table mb-0 datatable">
                          <thead>
                            <tr>
                              <th>ReportID</th>
                              <th>Report Type</th>
                              <th>Pay Period</th>
                              <th>Download</th>
                              <th>CreatedOn</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><a href="#">#EXL-0001</a></td>
                              <td>PT</td>
                              <td>07/11/2021 to 20/11/2021</td>
                              <td>
                                <a href="#" class="col-lg-9 col-sm-12 col-xs-12 btn btn-success btn-block">Download</a>
                              </td>
                              <td>04/03/2022 05:33 PM</td>
                            </tr>
                            <tr>
                              <td><a href="#">#EXL-0002</a></td>
                              <td>PT</td>
                              <td>07/11/2021 to 20/11/2021</td>
                              <td>
                                <button class="col-lg-9 col-sm-12 col-xs-12 btn btn-danger btn-block" type="button" disabled>
                                  <span class="spinner-border spinner-border-sm marginrightexport" role="status" aria-hidden="true"></span>
                                  Loading...
                                </button>
                              </td>
                              <td>04/03/2022 05:33 PM</td>
                            </tr>
                          </tbody>
                        </table> */}

                      

                    </div>
                  </div>
                </div>
                {/* FT PT Report Main Table */}
              </div>
              {/* /Page Content */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default BacthPayrollReports;
