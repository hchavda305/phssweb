/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Applogo} from '../Entryfile/imagepath.jsx'
import PasswordMask from 'react-password-mask';
import InputMask from 'react-input-mask';

import Loader from './Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
import SystemHelpers from './Helpers/SystemHelper';

class Loginpage extends Component {
    constructor(props) {
      super(props);

      this.state = {
          questions: [],
          password : '',
          errormsg :  '',
          step : 2,
          setuserName : '',
          setMobileNumber : '',
          setMobileNumberCountryCode : '',
          setPassword : '',
          setConfirmPassword : '',
          setQue1  : 0,
          setQue2 : 0,
          setQue3 : 0,
          setAns1 : '',
          setAns2 : '',
          setAns3 : '',
          userEmail : ''

      };
      this.handleChange = this.handleChange.bind(this)
    }
   

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });

    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }
    console.log('1111');
    console.log(this.state[input]);
  }

  componentDidMount(){
      if (location.pathname.includes("account_verification") || location.pathname.includes("login") || location.pathname.includes("register") || location.pathname.includes("forgotpassword")
      || location.pathname.includes("resetpassword") || location.pathname.includes("mobilesubmit") || location.pathname.includes("otp")|| location.pathname.includes("lockscreen") ) {
          $('body').addClass('account-page');
      }else if (location.pathname.includes("error-404") || location.pathname.includes("error-500") ) {
          $('body').addClass('error-page');
      }

      if (localStorage.getItem("token") != null) {
        this.props.history.push("/dashboard");
      }
      this.loginclick();
  }
      
  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method
  
  handleOnChangeMobile(value, data, event, formattedValue)
  {
    //console.log(value);
    var phone = value;
    var str2 = "+";
    var country_code=data.dialCode;
    if(phone.indexOf(str2) != -1){
        console.log(str2 + " found");
    }else{
        value='+'+value;
        country_code="+"+data.dialCode;
    }
    //console.log('value ' + value);
    //console.log('formattedValue ' + formattedValue);
    //console.log('country_code ' + country_code);
    this.setState({ setMobileNumber: value});
    this.setState({ setMobileNumberCountryCode : country_code });   
  }

  Setuser = () => e => {    
      //debugger; 
      e.preventDefault();
      
      /*console.log('phone no');
      console.log(this.state.setMobileNumber);
      return false;*/

      let step1Errors = {};
      // if (this.state["setuserName"] === '') {
      //   step1Errors["setuserName"] = "Please Enter User Name";
      // }else{
      //   var regex_user = /^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/
      //   if (!(regex_user.test(this.state["setuserName"]))) {
      //     step1Errors["setuserName"] = "Username should be 8-20 characters long , no _ or . at the end, no __ or _. or ._ or .. inside, no _ or . at the beginning";
      //   }
      // }

      if (this.state["setQue1"] === 0) {
        step1Errors["setQue1"] = "Please Select Security Question#1";
      }

      var setAns1_strlen = this.state["setAns1"].length;
      if (this.state["setAns1"] === '') {
        step1Errors["setAns1"] = "Please Enter Answer #1";
      }else if(this.state["setAns1"] ===" "){
        step1Errors["setAns1"] = "Space are not allowed Answer #1";
      }
      
      if (this.state["setQue2"] === 0) {
        step1Errors["setQue2"] = "Please Select Security Question#2";
      }

      var setAns2_strlen = this.state["setAns2"].length;
      if (this.state["setAns2"] === '') {
        step1Errors["setAns2"] = "Please Enter Answer #2";
      }else if(this.state["setAns2"] ===" "){
        step1Errors["setAns2"] = "Space are not allowed Answer #2";
      }

      if (this.state["setQue3"] === 0) {
        step1Errors["setQue3"] = "Please Select Security Question#3";
      }

      var setAns3_strlen = this.state["setAns3"].length;
      if (this.state["setAns3"] === '') {
        step1Errors["setAns3"] = "Please Enter Answer #3";
      }else if(this.state["setAns3"] ===" "){
        step1Errors["setAns3"] = "Space are not allowed Answer #3";
      }


      if (this.state["setConfirmPassword"] === '') {
        step1Errors["setConfirmPassword"] = "Please Enter Password";
      }else if (this.state["setPassword"] !== this.state["setConfirmPassword"]) {
          step1Errors["setConfirmPassword"] = "Confirm Password doesn't match"
      }

      if (this.state["setPassword"] == '') {
          step1Errors["setPassword"] = "Password is mandatory"
      }else {
          //var regex = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$%&?@ "])[a-zA-Z0-9!#$%&?@]{8,20}$/
          var regex = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
          if (!(regex.test(this.state["setPassword"]))) {
            step1Errors["setPassword"] = "Password should contain one upper, lower, special char and min 8 char";
          }
      }

      var MobileNumber =this.state["setMobileNumber"];
      var MobileNumberCountryCode =this.state["setMobileNumberCountryCode"];
      if(MobileNumber.length > 0 ) {
        var total = MobileNumber.length - MobileNumberCountryCode.length;
        if(total!=10)
        {
          step1Errors["setMobileNumber"] = "Please Enter a Valid Phone Number.";
        }
      }
      
      if(this.state["setQue1"] == this.state["setQue2"])
      {
        step1Errors["setQue2"] = "You must select different a Security Question.";
      }else if(this.state["setQue2"] == this.state["setQue3"]){
        step1Errors["setQue3"] = "You must select different a Security Question.";
      }else if(this.state["setQue1"] == this.state["setQue3"]){
        step1Errors["setQue3"] = "You must select different a Security Question.";
      }
      //console.log(splitmobile.length);
      //console.log(MobileNumber.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/ ]/gi, ''));
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }
      
      
      let Que_Ans_Json = [
            {
                securityQuestionId: this.state["setQue1"],
                securityQuestion: this.state["setAns1"],
                securityQuestionAns: this.state["setAns1"]
            },
            {
                securityQuestionId: this.state["setQue2"],
                securityQuestion: this.state["setAns2"],
                securityQuestionAns: this.state["setAns2"]
            },
            {
                securityQuestionId: this.state["setQue3"],
                securityQuestion: this.state["setAns3"],
                securityQuestionAns: this.state["setAns3"]
            }
        ];
     
      let bodyarray = {};
      //bodyarray["userName"] = this.state["setuserName"];
      bodyarray["userName"] = this.state["userEmail"];
      bodyarray["password"] = this.state["setPassword"];
      bodyarray["contactId"] = localStorage.getItem("temp_login_id");
      bodyarray["mobileNo"] = this.state["setMobileNumber"];
      bodyarray["securityQuestionView"] = Que_Ans_Json;
      
      //console.log(bodyarray);
      //return false;
      this.showLoader();
      var url=process.env.API_API_URL+'SetUsernamePasswordAndSecurityQuestion';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("temp_login_token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson SetUsernamePasswordAndSecurityQuestion");
          console.log(data);
          
          if (data.responseType === "1") {
              if(data.redirectUrl == '/login'){
                  SystemHelpers.ToastSuccess(data.responseMessge);
                  SystemHelpers.SessionOut();

                  //localStorage.setItem("temp_registration_default_userName", this.state["setuserName"]);
                  localStorage.setItem("temp_registration_default_userName", this.state["userEmail"]);

                  this.props.history.push("/login");
              }
          }else{
            SystemHelpers.ToastError(data.responseMessge);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  loginclick(){  
        
      //debugger;
      let step1Errors = {};

      var staffNumberId=this.props.match.params.id 
      this.showLoader();
     
      let bodyarray = {};
      bodyarray["staffNumberId"] = staffNumberId;
      var url=process.env.API_API_URL+'DoLogin';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson DoLogin");
          console.log(data);
          //console.log(responseJson);
          

          if (data.responseType === "1") {
            
              
              localStorage.setItem("temp_login_token", data.token);
              localStorage.setItem("temp_login_id", data.data.id);
              localStorage.setItem("temp_login_UserName", this.state["userName"]);
              localStorage.setItem("temp_login_Password", this.state["password"]);
              
              this.setState({userEmail: data.data.emailId});

              //this.props.history.push('/dashboard');
              if(data.redirectUrl == '/setusername'){
                this.setState({step: 2});
                this.setState({ questions: data.data.securityQuestionView });
                this.setState({ setuserName: data.data.username });
                if(data.data.mobile != null && data.data.mobile !=''){
                  this.setState({ setMobileNumber: data.data.mobile });
                }
                
              
                //SystemHelpers.ToastSuccess(data.responseMessge);
              }else{
                this.props.history.push(data.redirectUrl);
              }
              
          }else if(data.responseType === "2"){
              SystemHelpers.ToastError("Link is expired, please contact system admin to get active link.");
              SystemHelpers.SessionOut();
          }
          else{
              SystemHelpers.SessionOut();
              SystemHelpers.ToastError("This link is invalid.");
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  step_function(){
    let div_return = [];
    const { step } = this.state;
    const { userName, password, setuserName, setMobileNumber, setPassword, setConfirmPassword, setQue1 , setQue2, setQue3, setAns1, setAns2, setAns3 } = this.props;
    

    if(step == 2){
      div_return.push(
        <div className="account-content account-content-pk">
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box account-box-pk">
              <div className="account-wrapper">
                <h3 className="account-title">Configuration</h3>
                <p className="account-subtitle"></p>
                {/* Account Form */}
                <form action="/app/main/dashboard">
                  <div className="row">
                    {/*<div className="col-sm-6">
                      <div className="form-group">
                        <label>Username <span class="text-danger">*</span></label>
                        <input className="form-control" type="text" value={this.state.setuserName} readonly />
                        
                      </div>
                    </div>*/}
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Email <span class="text-danger"></span></label>
                        <input className="form-control" type="text" value={this.state.userEmail} readonly />
                        
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Phone</label>
                        {/*<InputMask className="form-control" mask="(999) 999-9999" value={setMobileNumber} onChange={this.handleChange('setMobileNumber')} ></InputMask>*/}
                        <PhoneInput
                          inputClass='form-control'
                          inputStyle= {{'width': '100%'}}
                          country={'ca'}
                          onlyCountries={['ca', 'in' ,'us']}
                          value={this.state.setMobileNumber}
                          onChange={this.handleOnChangeMobile.bind(this)}
                        />
                        <span class="form-text error-font-color">{this.state.errormsg["setMobileNumber"]}</span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Password <span class="text-danger">*</span></label>
                        <input className="form-control" type="password" value={setPassword} onChange={this.handleChange('setPassword')}  />
                        <span class="form-text error-font-color">{this.state.errormsg["setPassword"]}</span>
                        <span class="form-text success-font-color Guidelines">Password should contain a minimum 8 characters with a combination of at least 1 upper case, 1 lower case, 1 number and 1 special character.</span>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Confirm Password <span class="text-danger">*</span></label>
                        <input className="form-control" type="password" value={setConfirmPassword} onChange={this.handleChange('setConfirmPassword')}  />
                        <span class="form-text error-font-color">{this.state.errormsg["setConfirmPassword"]}</span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Question #1 <span class="text-danger">*</span></label>
                        <select className="form-control" value={setQue1} onChange={this.handleChange('setQue1')}>
                          <option className="select_otp_pk">Select</option>
                          {this.state.questions.map((question) => <option className="select_otp_pk" value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                        </select>
                        <span class="form-text error-font-color">{this.state.errormsg["setQue1"]}</span>
                        <span class="form-text success-font-color Guidelines">These questions will be used to verify your identity and help to reset your password, if you ever forget it.</span>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Answer #1 <span class="text-danger">*</span></label>
                        <input type="text" className="form-control" value={setAns1} onChange={this.handleChange('setAns1')} />
                        <span class="form-text error-font-color">{this.state.errormsg["setAns1"]}</span>
                      </div>
                    </div>
                  </div>
                  

                  <div className="row">
                    
                    <div className="col-sm-6">
                        <div className="form-group">
                          <label>Question #2 <span class="text-danger">*</span></label>
                          <select className="form-control" value={setQue2} onChange={this.handleChange('setQue2')}>
                            <option>Select</option>
                           {this.state.questions.map((question) => <option value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                          </select>
                          <span class="form-text error-font-color">{this.state.errormsg["setQue2"]}</span>
                          <span class="form-text success-font-color Guidelines">These questions will be used to verify your identity and help to reset your password, if you ever forget it.</span>
                        </div>
                    </div>

                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Answer #2 <span class="text-danger">*</span></label>
                        <input type="text" className="form-control" value={setAns2} onChange={this.handleChange('setAns2')} />
                        <span class="form-text error-font-color">{this.state.errormsg["setAns2"]}</span>
                      </div>
                    </div>
                    
                    
                  </div>

                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Question #3 <span class="text-danger">*</span></label>
                        <select className="form-control" value={setQue3} onChange={this.handleChange('setQue3')}>
                          <option>Select</option>
                          {this.state.questions.map((question) => <option value={question.securityQuestionId}  >{question.securityQuestion}</option>)}
                        </select>
                        <span class="form-text error-font-color">{this.state.errormsg["setQue3"]}</span>
                        <span class="form-text success-font-color Guidelines">These questions will be used to verify your identity and help to reset your password, if you ever forget it.</span>
                      </div>
                    </div>

                    <div className="col-sm-6">
                       <div className="form-group">
                          <label>Answer #3 <span class="text-danger">*</span></label>
                          <input type="text" className="form-control" value={setAns3} onChange={this.handleChange('setAns3')} />
                          <span class="form-text error-font-color">{this.state.errormsg["setAns3"]}</span>
                        </div>
                    </div>
                  </div>
                  
                  <div className="row">
                    
                    <div className="col-sm-12">
                      <div class="text-right">
                        <button type="submit" class="btn btn-primary" onClick={this.Setuser()}>Submit</button>
                      </div>
                    </div>
                    
                    
                  </div>
                  
                  <div className="account-footer">
                    
                  </div>
                </form>
                {/* /Account Form */}
              </div>
            </div>
          </div>
        </div>
      );
    }

    return div_return;
  }


   render() {
      
      return (
        <div>
         <ToastContainer position="top-right"
              autoClose={process.env.API_TOAST_TIMEOUT}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick={false}
              rtl={false}
              pauseOnVisibilityChange={false}
              draggable={false}
              pauseOnHover={false} />
         {(this.state.loading) ? <Loader /> : null} {/* Loder method use */}
         
         <div className="main-wrapper">
           <Helmet>
               <title>Login - PARTICIPATION HOUSE SUPPORT SERVICES</title>
               <meta name="description" content="Login page"/>					
         </Helmet>
        {this.step_function()}
      </div>
      </div>
      );
   }
}

export default Loginpage;
